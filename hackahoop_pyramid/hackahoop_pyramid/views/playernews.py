from hackahoop_models.meta import DBSession
from hackahoop_models.playernews import PlayerTweet, Tweet, FollowRelationship
from pyramid.view import view_config
from datetime import datetime
from utils import (
    getForbiddenError,
    getBadRequestError,
    getUnauthorizedError,
    check_csrf_token)

@view_config(route_name='ajax_playernews', renderer='json')
def playernews_json_view(request):
    """
    /v1/nba/playernews
        - playerId
        - since
        - limit
    """
    GET = request.GET
    playerId = GET.get("playerId")
    since = GET.get("since")
    limit = None

    # validating limit
    try:
        limit = "limit" in GET and int(GET.get("limit")) or 20
    except ValueError:
        return getBadRequestError("limit param should be integer")

    # validating playerId
    if playerId is None:
        request.response.status_code = 400
        return getBadRequestError("playerId param is missing")

    sinceDT = None
    if since is not None:
        try:
            sinceDT = datetime.strptime(since, "%Y-%m-%d %H:%M:%S")
        except ValueError:
            return getBadRequestError("since param is not valid")

    data = PlayerTweet.getByPlayerId(playerId, limit=limit, sinceDT=sinceDT)

    return {"news": [{
            "expandedText": d.tweet.getExpandedText(),
            "authorName": d.tweet.authorName,
            "authorScreenName": d.tweet.authorScreenName,
            "displayCreatedTime": d.tweet.getDisplayCreatedTime(),
            "createdTime": d.tweet.createdAt.strftime('%Y-%m-%d %H:%M:%S'),
            "profileImageUrl": d.tweet.profileImageUrl,
        } for d in data]}


@view_config(route_name="unfollow_player", renderer="json")
def unfollow_player_view(request):
    return _follow_helper(request, FollowRelationship.remove)

@view_config(route_name="follow_player", renderer="json")
def follow_player_view(request):
    return _follow_helper(request, FollowRelationship.add)

def _follow_helper(request, action=FollowRelationship.add):
    if not request.user:
        return getUnauthorizedError("Unauthenticated request")

    userId = request.user["userId"]

    if not check_csrf_token(request):
        return getForbiddenError("Failed CSRF check")

    if "playerId" not in request.POST:
        return getBadRequestError("playerId missing from POST")

    playerId = None
    try:
        playerId = int(request.POST["playerId"])
    except ValueError:  
        return getBadRequestError("Invalid playerId")

    action(playerId, userId) 

    return {
        "relationship": {
            "playerId": playerId,
            "userId": userId,
        }
    }
