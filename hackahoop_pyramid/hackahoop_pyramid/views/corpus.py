from pyramid.view import view_config
from hackahoop_models.corpus import Corpus1
from hackahoop_models.meta import DBSession
import logging
import math
from utils import (getForbiddenError, check_csrf_token, getParam,
    getBadRequestError, getUnauthorizedError)

log = logging.getLogger(__name__)

@view_config(route_name='corpus_list', renderer='../templates/corpus_list.pt')
def corpus_list_view(request):
    if not request.user:
        return getUnauthorizedError("Unauthorized request")

    query = Corpus1.get_query()
    total_count = query.count()

    page_size = 20
    page_count = int(math.ceil(float(total_count)/float(page_size)))

    offset = 0
    if "offset" in request.params:
        offset = int(request.params["offset"])
    
    page = (offset/page_size) + 1

    data = query[offset:offset + page_size]

    log.debug(total_count)
    return {
        "data": data,
        "total_count": total_count,
        "page_count": page_count,
        "page_size": page_size,
        "page": page,
    }

@view_config(route_name='update_corpus_doc', renderer='json')
def update_corpus_doc_view(request):
    userId = request.user["userId"]
    docId = request.matchdict["docId"]
    newsLabel = getParam(request, "newsLabel")

    doc = Corpus1.getById(docId)
    doc.news_label = newsLabel
    doc.news_labeler_id = userId
    DBSession.merge(doc)

    log.debug("{} {}".format(docId, newsLabel))
    log.debug(doc)
    
