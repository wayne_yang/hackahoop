from hackahoop_models.account import AuthId, AuthUser, addNewUser, FB_USER_DENIED_ERROR
from hackahoop_models.facebook import (saveFacebookFriends,
    saveFacebookInterests, saveFacebookLikes)

from pyramid.httpexceptions import HTTPFound
from pyramid.security import forget, remember
from pyramid.view import view_config

import utils
from velruse import login_url

import logging
log = logging.getLogger(__name__)


@view_config(route_name="register", renderer="../templates/register.pt")
def register_view(request):
    if request.user:
        # if already login redirect to an appropriate page
        # TODO:  check if this is an appropriate page.  Maybe redirect to an upsell/ads page
        return HTTPFound(location=request.route_path('index'))

    if "from" in request.GET:
        request.session["from"] = request.GET["from"]
    else:
        request.session["from"] = request.current_route_path()
        
    request.session.save()

    return { "login_url": login_url(request, "facebook") }

@view_config(
    context='velruse.AuthenticationComplete',
    renderer='json',
)
def login_complete_view(request):
    analytics = utils.HackAHoopAnalytics(request)
    log.debug("analytics={0}".format(analytics))

    context = request.context
    profile = context.profile
    account = profile["accounts"][0]
    accessToken = None 

    if "oauthAccessToken" in context.credentials:
        accessToken = context.credentials["oauthAccessToken"]

    log.debug("Received providerName={0} providerType={1}".format(
              context.provider_name, context.provider_type))
    log.debug("Received context.profile={0}".format(profile))
    log.debug("Received context.credentials.oauthAccessToken={0}".format(
              accessToken))
    log.debug("session={0}".format(request.session))

    # Check DB for this provider/login pair
    authUser = AuthUser.getByLogin(context.provider_name, account["userid"])
    log.debug("authUser={0}".format(authUser))
    if not authUser:
        # add user to DB if not already exists
        log.debug("Create a new authUser")
        (authIdObj, authUser) = addNewUser(context.provider_name, profile, accessToken)

        """
        TODO:  Slow.  Move to offline processs
        """
        analytics.trackSignUpEvent(context.provider_name)

        saveFacebookFriends(authIdObj.id, account["userid"], accessToken)
        saveFacebookLikes(authIdObj.id, account["userid"], accessToken)
        saveFacebookInterests(authIdObj.id, account["userid"], accessToken)

    else:
        analytics.trackLoginEvent(context.provider_name)

        log.debug("Found existing authUser: {0}".format(authUser))

    cameFrom = None
    if "from" in request.session:
        cameFrom = request.session["from"]
        del request.session["from"]
    else:
        # TODO configurable, upsell/ad page
        cameFrom = request.route_path("index")

    # update session
    remember(request, authUser.authId)
    request.session.save()

    return HTTPFound(location=cameFrom)


@view_config(
    context='velruse.AuthenticationDenied',
    renderer='json',
)
def login_denied_view(request):
    log.warn("login_denied_view: {0}".format(request.GET))

    # TODO:  revisit this part when other auth providers are added
    errorCode = "" 
    try:
        errorCode = int(request.GET["error_code"]) 
    except ValueError as e:
        log.info("Failed to get error code: {0}".format(e))
    
    # flash message for display on the target redirect page
    errorMsg = None
    if errorCode == FB_USER_DENIED_ERROR:
        errorMsg = "Please grant HackAHoop app accees on Facebook to get better experience."
    else:
        errorMsg = "Failed to login to Facebook ({0})".format(errorCode)
    request.session.flash(errorMsg)

    if "from" in request.session:
        cameFrom = request.session["from"]
        del request.session["from"]
    else:
        # TODO configurable, upsell/ad page
        cameFrom = request.route_path("index")

    # TODO:  need to explain what happened.  need to have a nice beautiful full -screen background
    return HTTPFound(location=cameFrom)


@view_config(route_name="logout")
def logout(request):
    headers = forget(request)
    came_from = utils.getCameFrom(request)
    request.session.invalidate()
    return HTTPFound(location=came_from, headers=headers)

"""
{
    "profile": {
        "preferredUsername": "wyang1106",
        "displayName": "Wayne Yang",
        "name": {
            "givenName": "Wayne",
            "formatted": "Wayne Yang",
            "familyName": "Yang"
        },
        "gender": "male",
        "utcOffset": "-07:00",
        "accounts": [
            {
                "domain": "facebook.com",
                "userid": "584357227"
            }
        ],
        "verifiedEmail": "yang.wayne@gmail.com",
        "emails": [
            {
                "primary": true,
                "value": "yang.wayne@gmail.com"
            }
        ]
    },
    "credentials": {
        "oauthAccessToken": "CAAClzLmSVK8BAEpL5sAM5IZCGYDZAsKNT6rYkktLeUz8zsMGL27anHr6uHIIEC7kBKMSfBDPHJh78JxZA3uBgXI8fKHVM3DKlZBjGPFXZCSwgU7sbN70E2adkjt2PiyGzTqMbeeKlPosBnqurZCfNLJ74qvhh26fN5xPqjAMALbgssKb2JmYEyYyy0lLKZCQp4ZD"
    },
    "provider_name": "facebook",
    "provider_type": "facebook"
}
"""
