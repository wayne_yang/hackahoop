import argparse
import os
import sys
import datetime
from hackahoop_nba import utils

from sqlalchemy import create_engine
from sqlalchemy.sql import text

parser = argparse.ArgumentParser(description="Update Season Average")
parser.add_argument("--config_file", default="boxscore_fetcher.ini", help="config file that contains all the settings")
parser.add_argument("--since_days", type=int, help="Last x days to compute average for")
parser.add_argument("--current_season", action="store_const", const=True, default=False)
args = parser.parse_args()

if args.since_days is not None and args.since_days < 1:
    print "since_days has to be > 0"
    sys.exit(1)

if args.since_days is not None and args.current_season:
    print "Please specify only one of since_days or current_season"
    sys.exit()


settings = { "boxscore_fetcher": {} }
if args.config_file is not None:
    cfgFile = os.path.join(os.path.dirname(__file__), 'boxscore_fetcher.ini')
    settings = utils.getConfigSettings(cfgFile)

engine = create_engine(settings["boxscore_fetcher"]["sqlalchemy.url"], echo=False)
conn = engine.connect()

columns = ["min", "fgm", "fga", "fg_pct", "fg3m", "fg3a", "fg3_pct", "ftm",
           "fta", "ft_pct", "oreb", "dreb", "reb", "ast", "stl", "blk", "tov", "pf" ,"pts"]

# no game start information available
today = datetime.datetime.utcnow().date()
currentSeason = utils.getSeasonFromDate(today)
gameStart = 0
params = {"gameStart": gameStart}
if args.since_days is not None:
    params["gameDate"] = today - datetime.timedelta(days=args.since_days)
    params["seasonId"] = utils.getSeasonIdForLastXDay(currentSeason, args.since_days)

    sql = text(
        "INSERT INTO overall_careerstats SELECT player_id, :seasonId, count(player_id), :gameStart, " +
         ", ".join(["avg({0})".format(i) for i in columns]) + " from gamelog " +
         "WHERE game_date > :gameDate GROUP BY player_id " +
         "ON DUPLICATE KEY UPDATE " + ", ".join(["{0}=values({0})".format(i) for i in columns])
    )
elif args.current_season:
    params["seasonId"] = currentSeason
    sql = text(
        "INSERT INTO overall_careerstats SELECT player_id, :seasonId, count(player_id), :gameStart, " +
         ", ".join(["avg({0})".format(i) for i in columns]) + " from gamelog " +
         "WHERE season_id = :seasonId GROUP BY player_id " +
         "ON DUPLICATE KEY UPDATE " + ", ".join(["{0}=values({0})".format(i) for i in columns])
    )
else:
    raise RuntimeError("Please specify either since_days or current_season")


result = conn.execute(sql, **params)


