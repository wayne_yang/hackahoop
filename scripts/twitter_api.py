from twitter_auth import twitter_app_auth
import urllib, urllib2
import json
import sys


consumerKey = "FI1axT6vyl9UKzl0x9HUA"
consumerSecret = "9AThPIlV0aDo2HOwvxiPa9C135kpZyJN2MSHVgg0"
token = None


def user_timeline(params, token=None):
    if token is None:
        token = twitter_app_auth(consumerKey, consumerSecret)

    headers = {
        "Authorization": "Bearer {0}".format(token)
    }

    queryString = urllib.urlencode(params)
    url = "https://api.twitter.com/1.1/statuses/user_timeline.json?{0}".format(queryString)

    req = urllib2.Request(url, headers=headers)
    resp = urllib2.urlopen(req)
    # convert to unicode
    content = resp.read().decode("utf-8")
    return content

def fetch_all_user_timeline(params):
    lastMaxId = sys.maxint
    data = json.loads(user_timeline(params))
    result = []

    while len(data) > 0 and data[0]["id"] < lastMaxId:
        result = result + data

        # max id is inclusive so substract 1 since that tweet was processed already in last request
        lastMaxId = data[-1]["id"] - 1
        params['max_id'] = lastMaxId
        sys.stderr.write("lasteMaxId={0} count={1}\n".format(lastMaxId, len(data)))
        data = json.loads(user_timeline(params))
        #sys.stderr.write("newData[0][id]={0}\n".format(data[0]["id"]))

    expand_retweets(result)
    return result

def expand_retweets(tweets):
    """
    Modifies in-place
    """
    for tweet in tweets:
        if "retweeted_status" in tweet:
            tweet["text"] = u"RT @{0}: {1}".format(tweet["user"]["screen_name"], tweet["retweeted_status"]["text"])

def search():
    params = {
        "q": "from:DwightHoward",
        "count": 5, 
        "result_type": "mixed",
        "with_twitter_user_id": "true",
    }
    url = "https://api.twitter.com/1.1/search/tweets.json?{0}".format(queryString)
    req = urllib2.Request(url, headers=headers)
    resp = urllib2.urlopen(req)
    content = resp.read()
    print content



