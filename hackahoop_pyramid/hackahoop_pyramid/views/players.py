from pyramid.view import view_config
from ..config import StatSliderConfig, Position

@view_config(route_name='players', renderer='../templates/players.pt')
def players_view(request):
    return {
        'statFilters': StatSliderConfig,
        'positions': Position }


