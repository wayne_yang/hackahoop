import json
import sys
import player_detection

counters = {}

with open("twitter_data/FBasketballNews.timeline.json", "r") as myFile:
    stringData = myFile.read()

data = json.loads(stringData)
for tweet in data:
    compareString = player_detection.get_compare_string(tweet)
    
    detectResult = player_detection.detect_player(compareString)
    if detectResult:
        for result in detectResult:
            fullName = result["fullName"]
            if fullName not in counters:
                counters[fullName] = 0
            counters[fullName] = counters[fullName] + 1
        #print "fullName={0} compareString={1}".format(fullName, compareString.encode("utf-8"))
    else:
        pass
        print compareString.encode("utf-8")


totalPlayersWithNews = 0
newsCoveringPlayers = 0

for key in counters.keys():
    count = counters[key]
    if count > 0:
        totalPlayersWithNews = totalPlayersWithNews + 1
        newsCoveringPlayers = newsCoveringPlayers + count 
        

print "totalNews={0} newsCoveringPlayers={1} totalPlayers={2} totalPlayersWithNews={3}".format(
    len(data), newsCoveringPlayers, len(player_detection.full2LastNames), totalPlayersWithNews)

print "newsCoverage={0} playerCoverage={1}".format(
    float(newsCoveringPlayers)/len(data),
    float(totalPlayersWithNews)/len(player_detection.full2LastNames))
