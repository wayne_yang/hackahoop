from pyramid.view import view_config
from pyramid.httpexceptions import HTTPFound

from .playersearch import  playersearch_json_view
import re
import logging
log = logging.getLogger(__name__)

@view_config(route_name='search_result', renderer='../templates/search_result.pt')
def search_view(request):
    # TODO:  this needs to go to a general-purpose search platform
    # for now assume the keyword is for player names
    GET = request.GET
    result = []
    keyword = ''
    if 'q' in GET:
        keyword = GET.getone('q')
        request.GET['term'] = keyword
        result = playersearch_json_view(request)
    else:
        return { 'data': [], 'keyword': ''}

    #log.debug("result={0}".format(result))
    if len(result) == 1:
        return HTTPFound(request.route_path("single_player", playerName=result[0]["id"]))

    # add highlighting of the keyword
    pattern = re.compile(r"({0})".format(keyword), flags=re.IGNORECASE)
    for i in result:
        i["label"] = re.sub(pattern, r"<strong>\1</strong>".format(keyword),
            i["label"])
    return { 'data': result, 'keyword': keyword}
