from datetime import datetime, timedelta 
from bs4 import BeautifulSoup
import requests
import re
import time
from teams import TeamInfo
import db_utils


espnURL = "http://espn.go.com/nba/schedule/_/date/{0}"
insertSQL = ("INSERT INTO nba_stats.game_schedule (home_team, away_team,"
    "game_date, game_time) VALUES(%s, %s, %s, %s)")


def importGameSchedule(htmlContent=None, startDate=None, dbImport=True):
    if htmlContent is None and startDate is None:
        raise Exception("Please specify either htmlContent or gameDate")

    if startDate is not None:
        dStr = startDate.strftime("%Y%m%d")
        resp = requests.get(espnURL.format(dStr))
        htmlContent = resp.text

    soup = BeautifulSoup(htmlContent)

    # figure out what nba season it is
    scheRange = soup.select("div.mod-content h1.h2")[0].text
    rangeParts = scheRange.split(" - ")
    ts = time.strptime(rangeParts[1], "%b %d, %Y")
    lowYear = None
    highYear = None
    if ts.tm_mon < 10:
        highYear = ts.tm_year
        lowYear = highYear - 1
    else:
        lowYear = ts.tm_year
        highYear = lowYear + 1

    tables = soup.select("div.mod-content table")
    conn = db_utils.connect(db_utils.dbConfig)
    cursor = conn.cursor()

    for table in tables:
        colHead = table.select("tr.colhead")
        if not colHead:
            # no game day
            continue

        dateStruct = time.strptime(table.select("tr.stathead td")[0].text, "%A, %B %d")
        year = highYear if dateStruct.tm_mon < 10 else lowYear
        dateStr = datetime(year, dateStruct.tm_mon, dateStruct.tm_mday).strftime("%Y-%m-%d")

        rows = table.find_all("tr", class_=re.compile(r"team-.*"))    
        for gameRow in rows:
            cols = gameRow.find_all("td")
            #print cols

            # parse teams
            [awayTeam, homeTeam] = [
                re.search(r"/([^/]+)$", a.attrs["href"]).group(1).replace("-", " ")
                for a in cols[0].find_all("a")]
            homeId = TeamInfo[homeTeam][1]
            awayId = TeamInfo[awayTeam][1]

            timeStr = datetime.strptime(cols[1].text, "%I:%M %p").strftime("%H:%M")

            if dbImport:
                cursor.execute(insertSQL, (homeId, awayId, dateStr, timeStr))


    conn.commit()
    cursor.close()
    conn.close()

if __name__ == "__main__":

    start = datetime(2013, 10, 29)
    end = datetime(2014, 4, 21)

    days = 7
    pageSize = timedelta(days)

    """
    fetchGameSchedule(startDate=datetime(2013, 11, 26))

    html = open("/tmp/b").read()
    fetchGameSchedule(htmlContent=html)
    """

    pageDate = start
    while pageDate < end:
        importGameSchedule(startDate=pageDate)
        pageDate = pageDate + pageSize

