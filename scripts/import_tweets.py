import mysql.connector
import json
import sys
import time
import datetime
import db_utils
import player_detection
from mysql.connector.errors import IntegrityError



def format_tweet_time(timeStr):
    return time.strftime('%Y-%m-%d %H:%M:%S', time.strptime(timeStr,'%a %b %d %H:%M:%S +0000 %Y'))


def import_tweets(tweets, conn=None):
    insertTweetSql = ("INSERT INTO nba_stats.tweet (id, author_name, author_screen_name,"
                "text, entities, retweet_count, favorite_count, verified, created_at,"
                " profile_image_url) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)")

    if conn is None:
        conn = db_utils.connect(db_utils.dbConfig)
    cursor = conn.cursor()

    for tweet in tweets:
        idStr = tweet["id_str"]
        entitiesStr = json.dumps(tweet["entities"])
        text = tweet["text"]
        retweetCount = tweet["retweet_count"]
        favoriteCount = tweet["favorite_count"]
        authorName = tweet["user"]["name"]
        authorScreenName = tweet["user"]["screen_name"]
        profileImageUrl = tweet["user"]["profile_image_url"]
        verified = False
        if "verified" in tweet:
            verified = True

        createdAt = format_tweet_time(tweet['created_at'])

        try:
            cursor.execute(insertTweetSql, (idStr, authorName, authorScreenName, text,
                entitiesStr, retweetCount, favoriteCount, verified, createdAt, profileImageUrl))
        except IntegrityError as e:
            sys.stderr.write("IntegrityError: {0}\n".format(e))
    conn.commit()

def import_player_tweet(playerId, tweetId, conn=None):
    insertNewsSql = ("INSERT INTO nba_stats.player_tweets (player_id, tweet_id) VALUES (%s, %s)")
    if conn is None:
        conn = db_utils.connect(db_utils.dbConfig)
    cursor = conn.cursor()
    try:
        cursor.execute(insertNewsSql, (playerId, tweetId))

    except IntegrityError as e:
        sys.stderr.write("IntegrityError: {0}\n".format(e))

if __name__ == "__main__":
    cnx = db_utils.connect(db_utils.dbConfig)
    cursor = cnx.cursor()

    for line in sys.stdin:
        data = json.loads(line)

        for tweet in data:
            compareString = player_detection.get_compare_string(tweet)
            detectResult = player_detection.detect_player(compareString)
            if not detectResult:
                continue

            import_tweets([ tweet ]) 

            for result in detectResult:
                import_player_tweet(result["id"], tweet["id_str"])

    cnx.commit()
    cursor.close()
    cnx.close()
