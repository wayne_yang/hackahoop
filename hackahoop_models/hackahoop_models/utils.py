import datetime
import decimal
from json import JSONEncoder

class ModelJsonEncoder(JSONEncoder):
    def default(self, obj):
        if isinstance(obj, datetime.date):
            return obj.isoformat()

        elif isinstance(obj, decimal.Decimal):
            return float(obj)

        return JSONEncoder.default(self, obj)
