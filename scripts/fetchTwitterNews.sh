#!/bin/bash

# New:
# ESPNNBA, EyeOnBasketball YahooSports, SpearsNBAYahoo, basketballtalk, RotoWireKyleNBA, RotoAnalysis, RotoExperts
#
#
# Existing:
# ESPNSteinLine FBasketballNews HOOPSWORLD NBA Rotoworld_BK
SOURCES="ESPNSteinLine FBasketballNews HOOPSWORLD NBA Rotoworld_BK ESPNNBA EyeOnBasketball YahooSports SpearsNBAYahoo basketballtalk RotoWireKyleNBA RotoAnalysis RotoExperts"

for i in $SOURCES; do
    echo $i;
    python fetch_twitter_news.py $i
done
