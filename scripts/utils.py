import re
from hackahoop_models.playerinfo import PlayerInfo

SuffixPattern = re.compile("(II|III|Jr.)")

def getSEOName(firstName, lastName, suffix):
    components = [firstName, lastName]
    if suffix is not None:
        components.append(suffix)
    return "-".join([i.replace(".", "") for i in components]).lower()



def parseHumanName(fullName):
    """
    Use Simple Heuristic to parse the suffix from the name.  Return name and suffix in a dict
    """
    parts = fullName.split(" ")
    suffix = None
    name = fullName

    if SuffixPattern.match(parts[-1]):
        suffix = parts[-1]
        name = " ".join(parts[:-1])

    return {"name": name, "suffix": suffix}

def findPlayer(firstName, lastName):
    """
    Wrapper over PlayerInfo with additional fallbacks when no result
    TODO:  should probably consider better player identification/matching mechanism
    """
    nameParse = parseHumanName(lastName)
    suffix = nameParse["suffix"]
    if suffix is not None:
        lastName = nameParse["name"]

    # fetch playerinfo to get id
    playerinfo = PlayerInfo.getByName(
        firstName,
        lastName,
        suffix,
        rosterStatus=1)

    if playerinfo is None:
        if firstName == u"Timothy":
            firstName = u"Tim"
            playerinfo = PlayerInfo.getByName(
                firstName, lastName, suffix, rosterStatus=1)
        elif len(firstName) == 2:
            firstName = firstName[0] + "." + firstName[1] + "."
            playerinfo = PlayerInfo.getByName(
                firstName, lastName, suffix, rosterStatus=1)

    return playerinfo 

if __name__ == "__main__":
    print parseHumanName("Jucar Hello III")
