import mysql.connector
import json
import sys
import time
import datetime
import db_utils


encoding = "utf-8"


insertSql = ("INSERT INTO nba_stats.playercareerstats (PLAYER_ID,SEASON_ID,LEAGUE_ID,"
            "TEAM_ID,TEAM_ABBREVIATION,PLAYER_AGE,GP,GS,MIN,FGM,FGA,FG_PCT,"
            "FG3M,FG3A,FG3_PCT,FTM,FTA,FT_PCT,OREB,DREB,REB,AST,STL,BLK,TOV,"
            "PF,PTS) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,"
            "%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)")


cnx = db_utils.connect(db_utils.dbConfig)
cursor = cnx.cursor()

for line in sys.stdin:
    data = json.loads(line)

    seasonAvgData = None
    for res in data["resultSets"]:
        if res["name"] == "SeasonTotalsRegularSeason":
            seasonAvgData = res
            break

    if seasonAvgData is None:
        sys.stderr.write("Failed to find SeasonTotalsRegularSeason\n")
        continue

    sAggregate = {}
    for seasonData in seasonAvgData["rowSet"]:
        playerId = seasonData[0]
        seasonId = seasonData[1].split("-")[0]
        leagueId = int(seasonData[2])
        teamId = int(seasonData[3])

        if seasonData[4] is None:
            teamAbbreviation = ""
        else:
            teamAbbreviation = seasonData[4].encode(encoding)
        playerAge = int(seasonData[5])
        gp = int(seasonData[6])

        if seasonData[7] is None:
            gs = 0
        else:
            gs = int(seasonData[7])
        minutes = seasonData[8]
        fgm = seasonData[9]
        fga = seasonData[10]
        fgPCT = seasonData[11]
        fg3m = seasonData[12]
        fg3a = seasonData[13]
        fg3PCT = seasonData[14]
        ftm = seasonData[15]
        fta = seasonData[16]
        ftPCT = seasonData[17]
        oreb = seasonData[18]
        dreb = seasonData[19]
        reb = seasonData[20]
        ast = seasonData[21]
        stl = seasonData[22]
        blk = seasonData[23]
        tov = seasonData[24]
        pf = seasonData[25]
        pts = seasonData[26]

        if minutes is None:
            minutes = 0
        if fgm is None:
            fgm = 0
        if fga is None:
            fga = 0
        if fgPCT is None:
            fgPCT = 0
        if fg3m is None:
            fg3m = 0
        if fg3a is None:
            fg3a = 0
        if fg3PCT is None:
            fg3PCT = 0
        if ftm is None:
            ftm = 0
        if fta is None:
            fta = 0
        if ftPCT is None:
            ftPCT = 0
        if oreb is None:
            oreb = 0
        if dreb is None:
            dreb = 0
        if reb is None:
            reb = 0
        if ast is None:
            ast = 0
        if stl is None:
            stl = 0
        if blk is None:
            blk = 0
        if tov is None:
            tov = 0
        if pf is None:
            pf = 0
        if pts is None:
            pts = 0


        formatStr = ("playerId={0},seasonId={1},leagueId={2},teamId={3},teamAbbreviation={4},playerAge={5},gp={6},gs={7},minutes={8},fgm={9},fga={10},fgPCT={11},fg3m={12},fg3a={13},fg3PCT={14},ftm={15},fta={16},ftPCT={17},oreb={18},dreb={19},reb={20},ast={21},stl={22},blk={23},tov={24},pf={25},pts={26}")
        #print formatStr.format(playerId, seasonId, leagueId, teamId, teamAbbreviation, playerAge, gp, gs, minutes, fgm, fga, fgPCT, fg3m, fg3a, fg3PCT, ftm, fta, ftPCT, oreb, dreb, reb, ast, stl, blk, tov, pf, pts) 

        record = [playerId, seasonId, leagueId, teamId, teamAbbreviation, playerAge, gp, gs, minutes, fgm, fga, fgPCT, fg3m, fg3a, fg3PCT, ftm, fta, ftPCT, oreb, dreb, reb, ast, stl, blk, tov, pf, pts]

        cursor.execute(insertSql, *record)

        """
        if seasonId not in sAggregate:
            sAggregate[seasonId] = []
        sAggregate[seasonId].append(record)

    for seasonId in sAggregate:
        print seasonId, sAggregate[seasonId]
        if len(sAggregate[seasonId]) == 1:
            pass
            cursor.execute(insertSql, *sAggregate[seasonId][0])
        else:
            totalGP = 0
            totalGS = 0
            for subSeason in sAggregate[seasonId]:
                gp = subSeason[6]
                totalGP = totalGP + gp
                print gp
                print subSeason
        """

cnx.commit()
cursor.close()
cnx.close()

