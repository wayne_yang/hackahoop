from readability.readability import Document
import html2text
import logging
import ConfigParser

# User-Agent
CHROME_UA = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.95 Safari/537.36"

def clean_web_page(html):
    """
    Result is in markdown syntax
    """
    readable_article = Document(html).summary()
    return html2text.html2text(readable_article)

def getConfigSettings(cfgFile):
    parser = ConfigIniParser()
    parser.read(cfgFile)
    return parser.as_dict()

class ConfigIniParser(ConfigParser.ConfigParser):
    def as_dict(self):
        d = dict(self._sections)
        for k in d:
            d[k] = dict(self._defaults, **d[k])
            d[k].pop('__name__', None)
        return d

def getLogger(logLevel=logging.DEBUG):
    logger = logging.getLogger(__name__)
    logger.setLevel(logLevel)
    logger.propagate = False

    if not logger.handlers:
        # create formatter
        formatter = logging.Formatter('%(asctime)s %(module)s %(funcName)s %(levelname)s - %(message)s')
        ch = logging.StreamHandler()
        ch.setFormatter(formatter)
        logger.addHandler(ch)
        
    return logger

def is_media_type(guess_type):
    if guess_type is None:
        return False
    else:
        return (guess_type.startswith("image") 
            or guess_type.startswith("audio") 
            or guess_type.startswith("video"))

