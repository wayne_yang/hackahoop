from hackahoop_nba import nba
from hackahoop_nba import utils
from hackahoop_models.game_schedule import GameSchedule
from hackahoop_models.gamelog import GameLog
from hackahoop_models.playerinfo import PlayerInfo
from hackahoop_models.depthchart import DepthChart
from hackahoop_models.playernews import FollowRelationship, PlayerTweet
from hackahoop_models.careerstats import CareerStats, CombinedCareerStats
from hackahoop_models.rate import Rating
from hackahoop_models.similar_player import SimilarPlayer
from hackahoop_models.youtube_video import YoutubeVideo

import datetime
import logging
import os
from sqlalchemy.exc import IntegrityError
import argparse

from requests.exceptions import RequestException

def updateExistingData(session, oldPlayerId, newPlayerId):
    (session.query(DepthChart)
        .filter(DepthChart.playerId == oldPlayerId)
        .update({DepthChart.playerId: newPlayerId},
        synchronize_session=False))

    (session.query(FollowRelationship)
        .filter(FollowRelationship.playerId == oldPlayerId)
        .update({FollowRelationship.playerId: newPlayerId},
        synchronize_session=False))

    (session.query(CareerStats)
        .filter(CareerStats.playerId == oldPlayerId)
        .update({CareerStats.playerId: newPlayerId},
        synchronize_session=False))

    (session.query(GameLog)
        .filter(GameLog.playerId == oldPlayerId)
        .update({GameLog.playerId: newPlayerId},
        synchronize_session=False))

    (session.query(CombinedCareerStats)
        .filter(CombinedCareerStats.playerId == oldPlayerId)
        .update({CombinedCareerStats.playerId: newPlayerId},
        synchronize_session=False))

    (session.query(PlayerTweet)
        .filter(PlayerTweet.playerId == oldPlayerId)
        .update({PlayerTweet.playerId: newPlayerId},
        synchronize_session=False))

    (session.query(Rating)
        .filter(Rating.playerId == dup.playerId)
        .update({Rating.playerId: playerInfo.playerId},
        synchronize_session=False))

    (session.query(SimilarPlayer)
        .filter(SimilarPlayer.playerId == dup.playerId)
        .update({SimilarPlayer.playerId: playerInfo.playerId},
        synchronize_session=False))

    (session.query(YoutubeVideo)
        .filter(YoutubeVideo.playerId == dup.playerId)
        .update({YoutubeVideo.playerId: playerInfo.playerId},
        synchronize_session=False))

    session.commit()

#logging.basicConfig(filename='sqlalchemy.log', level=logging.DEBUG)
#logging.getLogger('sqlalchemy.engine').setLevel(logging.INFO) 
logger = utils.getLogger()


parser = argparse.ArgumentParser(description="Boxscore Fetcher")
parser.add_argument("--hour_threshold", help="hours to wait after game ends to fetch box scores")
parser.add_argument("--max_retries", help="number of times to retries")
parser.add_argument("--sqlalchemy.url", help="db connection string")
parser.add_argument("--config_file", default="boxscore_fetcher.ini", help="config file that contains all the settings")
args = parser.parse_args()

settings = { "boxscore_fetcher": {} }
if args.config_file is not None:
    cfgFile = os.path.join(os.path.dirname(__file__), args.config_file)
    settings = utils.getConfigSettings(cfgFile)

argNames = ["max_retries", "hour_threshold", "sqlalchemy.url"]
for argName in argNames:
    argValue = getattr(args, argName)
    if argValue is not None:
        settings["boxscore_fetcher"][argName] = argValue


cfg = utils.BoxScoreFetcherConfig(settings)

Session = utils.initDB(settings["boxscore_fetcher"])
session = Session()

maxRetries = cfg.getMaxRetries()
hourThreshold = cfg.getHourThreshold()
logger.debug("maxRetries={0} hourThreshold={1}".format(
    maxRetries, hourThreshold))

games = GameSchedule.getUnprocessedGames(hourThreshold=hourThreshold, maxRetries=maxRetries)
logger.info("Found {0} game(s) to process".format(len(games)))

scoreboards = {}
playerInfoCache = {}
nbaPlayers = nba.NbaPlayers()

for game in games:
    try:
        logger.info("Fetching gamelogs for game: {0}".format(game))
        gd = game.gameDate
        if gd not in scoreboards:
            scoreboards[gd] = nba.ScoreBoard(session, gd)

        scoreboard = scoreboards[gd]
        gamelogs = []
        gamelogs = scoreboard.fetch(game.homeTeamId, game.awayTeamId)
        for gl in gamelogs:
            # set game id since game_id is different between game_schedule and nba's
            gl.gameId = game.gameId

            # Make sure player exists in DB
            playerInfo = None
            if gl.playerId not in playerInfoCache:
                playerInfoCache[gl.playerId] = PlayerInfo.getById(gl.playerId)

            playerInfo = playerInfoCache[gl.playerId]

            # Should be commented out later
            if playerInfo is None:
                playerInfo = utils.convertToPlayerInfo(nbaPlayers.getById(gl.playerId))
                try:
                    session.add(playerInfo)
                    session.flush()
                except IntegrityError as e:
                    session.rollback()

                    if "for key 'SEO_NAME'" in e.message:
                        dup = PlayerInfo.getBySeoName(playerInfo.seoName, dbSession=session)
                        logger.info("new playerinfo={0}".format(playerInfo))
                        logger.info("dup playerinfo={0}".format(dup))

                        # temporarily rename seoName of the dup so new one can be added
                        dup.seoName = dup.seoName + "temp"
                        session.merge(dup)
                        session.add(playerInfo)
                        session.commit()

                        # now updating existing records referencing the dup playerInfo
                        updateExistingData(session, dup.playerId, playerInfo.playerId)

                        session.delete(dup)
                        session.commit()

        logger.info("Saving {0} gamelogs".format(len(gamelogs)))
        #logger.debug("gamelogs={0}".format(gamelogs))

        session.add_all(gamelogs)
        # might raise IntegrityError
        session.flush()

    except (nba.ParseError, IntegrityError, nba.GameNotFinalError, RequestException) as e:
        logger.error("Failed to save gamelogs: {0}".format(e))
        #logger.exception("Failed to save gamelogs: ")
        session.rollback()

        if game.fetchState == GameSchedule.FAIL_STATE:
            game.retries = game.retries + 1
        else:
            game.fetchState = GameSchedule.FAIL_STATE

    else:
        game.fetchState = GameSchedule.DONE_STATE

    finally:
        game.fetchDate = datetime.datetime.utcnow()
        session.merge(game)

        logger.info("Committing changes:  game={0}, count(gamelogs)={1}".format(
            game, len(gamelogs)))
        session.commit()
