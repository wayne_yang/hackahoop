from bs4 import BeautifulSoup
from teams import TeamInfo
import db_utils
import sys
import player


insertSql = ("INSERT INTO nba_stats.depth_chart (team_id,position,depth,player_id)"
    "VALUES (%s, %s, %s, %s)")
f = open("/home/wyang/Downloads/rotoworld_depth_chart.html")
soup = BeautifulSoup(f.read())
conn = db_utils.connect(db_utils.dbConfig)
cursor = conn.cursor()
rows = soup.select("table#cp1_tblDepthCharts > tr")

for i in [i for i in range(len(rows)) if i % 2 == 0]:
    teams = [elem.text for index, elem in enumerate(rows[i].select("a"))
        if index % 2 == 0]
    i = i + 1

    for index, table in enumerate(rows[i].select("table")):
        #print teams[index]
        position = None
        depth = 0
        for playerRow in table.select("tr"):
            posTD, playerTD = playerRow.select("td")
            playerName = playerTD.select("a")[0].text.encode("utf8")
            if posTD.text != "":
               position = posTD.text
               depth = 1
            else:
               depth = depth + 1
            
            res = player.findPlayer(playerName, conn=conn)
            if len(res) == 0:
                sys.stderr.write("NOT FOUND {0} {1} {2} {3}\n".format(depth, teams[index], position, playerName))
                continue
            elif len(res) > 1:
                sys.stderr.write("Multiple Matches for team={0} player={1}\n".format(teams[index], playerName))
                continue
                
            playerInfo = res[0] 
            teamInfo = TeamInfo[teams[index]]
            record = (teamInfo[1], position, depth, playerInfo[0])
            cursor.execute(insertSql, record)

            player.updatePlayerPosition(playerInfo[0], position, conn=conn)

conn.commit()
cursor.close()
conn.close()
