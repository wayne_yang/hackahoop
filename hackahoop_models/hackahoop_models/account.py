from .meta import Base, DBSession

from sqlalchemy import Column, ForeignKey, Unicode, Integer, Enum, DateTime, Text
from sqlalchemy.sql.expression import func
from sqlalchemy.orm import relationship, contains_eager

from datetime import datetime

FB_USER_DENIED_ERROR = 200

class AuthId(Base):
    __tablename__ = 'auth_id'

    id = Column(Integer, primary_key=True)
    displayName = Column("display_name", Unicode(80), default=u'')
    active = Column(Enum(u'Y',u'N',u'D', name=u'active'), default=u'Y')
    created = Column(DateTime, default=func.now())

    users = relationship('AuthUser')

    @classmethod
    def getById(cls, id):
        return (DBSession.query(cls)
            .join(cls.users)
            .options(contains_eager(cls.users))
            .filter(cls.id==id)
            .first())

    @classmethod
    def add(cls, displayName):
        authId = cls(displayName=displayName, created=datetime.now(), active="Y")
        DBSession.add(authId)
        return authId

    def getCreateTime(self, fmt="%b %d, %Y"):
        return self.created.strftime(fmt)

    def __repr__(self):
        s = (u"<AuthId(id={0}, displayName={1}, active={2}, created={3},"
            "users={4})>")
        return s.format(self.id, self.displayName, self.active, self.created,
            self.users)

class Provider:
    FACEBOOK = "facebook"


class AuthUser(Base):
    __tablename__ = 'auth_user'
    id = Column(Integer, primary_key=True)
    authId = Column("auth_id", Integer, ForeignKey(AuthId.id), index=True)
    provider = Column(Unicode(80), default=u'local', index=True)
    login = Column(Unicode(80), default=u'', index=True)
    salt = Column(Unicode(24))
    _password = Column('password', Unicode(80), default=u'')
    email = Column(Unicode(80), default=u'', index=True)
    created = Column(DateTime, default=func.now())
    active = Column(Enum(u'Y',u'N',u'D', name=u'active'), default=u'Y')
    firstName = Column("first_name", Unicode(80))
    lastName = Column("last_name", Unicode(80))
    gender = Column("gender", Enum(u"M", u"F"), default=None)
    timezone = Column("timezone", Unicode(10), default=None)
    accessToken = Column("access_token", Text)

    """
    def _set_password(self, password):
        self.salt = self.get_salt(24)
        password = password + self.salt
        self._password = BCRYPTPasswordManager().encode(password, rounds=12)

    def _get_password(self):
        return self._password
    """
    @classmethod
    def add(cls, authId, login, provider,
        email, firstName, lastName, gender, timezone, accessToken):
        user = cls(login=login,
            provider=provider, email=email,
            firstName=firstName,lastName=lastName,
            gender=gender, timezone=timezone, accessToken=accessToken)
        authId.users.append(user)
        return user

    @classmethod
    def getById(cls, id):
        return DBSession.query(cls).filter(cls.id==id).first()    

    @classmethod
    def getByLogin(cls, provider, login):
        return (DBSession.query(cls)
            .filter(cls.login == login)
            .filter(cls.provider == provider)
            .first())

    @classmethod
    def getByEmail(cls, email):
         return DBSession.query(cls).filter(cls.email==email).first()

    def __repr__(self):
        s = (u"<AuthUser(id={0}, authId={1}, provider={2}, login={3},"
            "email={4}, created={5}, active={6}, firstName={7},"
            "lastName={8})>")
        return s.format(self.id, self.authId, self.provider, self.login,
            self.email, self.created, self.active, self.firstName, self.lastName)


def addNewUser(provider, profile, accessToken):
    authId = AuthId.add(profile["displayName"]) 
    login = profile["accounts"][0]["userid"]
    email = profile["verifiedEmail"] if "verifiedEmail" in profile else None
    firstName = profile["name"]["givenName"]
    lastName = profile["name"]["familyName"]

    gender = None
    if "gender" in profile:
        gender = "M" if profile["gender"] == "male" else "F"

    timezone = None
    if "utcOffset" in profile:
        timezone = profile["utcOffset"]
    authUser = AuthUser.add(authId, login, provider, email, firstName,
        lastName, gender, timezone, accessToken)

    # flush now so user object will have all the fields populated by defaults
    DBSession.flush()

    return (authId, authUser) 


