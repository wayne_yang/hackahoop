import json
import traceback

from datetime import datetime
import calendar

from pyramid.httpexceptions import (
    HTTPUnauthorized, HTTPBadRequest, HTTPForbidden)

from hackahoop_models.rate import Rating

from ..utils.ga import MeasurementProtocol
from requests.exceptions import RequestException

import logging
log = logging.getLogger(__name__)


class HackAHoopAnalytics(object):
    def __init__(self, request):
        self._liveTracking = request.registry.settings['ga.liveTracking']
        self._mpVersion = request.registry.settings["ga.mp.version"]
        self._trackingId = request.registry.settings["ga.trackingId"]
        self._clientId = self.parseUniversalAnalyticsCookie(request)
        self._mp = MeasurementProtocol(
                self._mpVersion,
                self._trackingId,
                self._clientId)

    def __repr__(self):
        return ("HackAHoopAnalytics<livetracking={0}, mpVersion={1},"
                " trackingId={2}, clientId={3}, mp={4}>".format(
                self._liveTracking, self._mpVersion, self._trackingId,
                self._clientId, self._mp))

    @classmethod
    def parseUniversalAnalyticsCookie(cls, request):
        """
        u'_ga': u'GA1.3.767678717.1378960939'
        """
        if "_ga" in request.cookies:
            parts = request.cookies["_ga"].split(".")
            if len(parts) == 4:
                return "{0}.{1}".format(parts[2], parts[3])

        return None    

    def trackLoginEvent(self, providerName):
        if not self._liveTracking:
            log.debug("ga.liveTracking is false. Skip login event tracking")
            return

        if self._clientId is None:
            log.warn("Failed to parse UA cookie to get client ID")
        else:
            log.debug("Sending login event to UA: _mp={0} providerName={1}".format(
                self._mp, providerName))
            try:
                customDimensions = {
                    "cd1": providerName,
                }
                self._mp.sendEventTracking(
                    "Account",
                    "Login",
                    providerName,
                    customDimensions=customDimensions)

            except RequestException as e:
                log.error("Failed to send event to MP: {0}".format(e))

    
    def trackSignUpEvent(self, providerName):
        if not self._liveTracking:
            log.debug("ga.liveTracking is false. Skip signup event tracking")
            return

        if self._clientId is None:
            log.warn("Failed to parse UA cookie to get client ID")
        else:
            log.debug("Sending account signup event to UA: _mp={0} providerName={1}".format(
                self._mp, providerName))
            try:
                customDimensions = {
                    "cd1": providerName,
                }
                self._mp.sendEventTracking(
                    "Account",
                    "SignUp",
                    providerName,
                    customDimensions=customDimensions)

            except RequestException as e:
                log.error("Failed to send event to MP: {0}".format(e))

    
def logExcInfo(logFunc, excInfo):
    logFunc("error occurred: {0} {1}".format(excInfo[0], excInfo[1]))
    logFunc("".join(traceback.format_tb(excInfo[2])))

def check_csrf_token(request, tokenName="csrf_token"):
    if tokenName not in request.params:
        return False

    if request.session.get_csrf_token() != request.params[tokenName]:
        return False

    return True

def getError(message):
    return {
        "error": {
            "message": message
        }
    }

def getErrorInJson(message):
    return json.dumps(getError(message))

def getBadRequestError(message, formatter=getErrorInJson):
    """
    Return a HTTPBadRequest with a json error body
    """
    return HTTPBadRequest(body=formatter(message))

def getUnauthorizedError(message, formatter=getErrorInJson):
    return HTTPUnauthorized(body=formatter(message))

def getForbiddenError(message, formatter=getErrorInJson):
    return HTTPForbidden(body=formatter(message))

def getCameFrom(request):
    # TODO: The default url should come from config
    return request.GET.get('from',
        request.POST.get('from', request.route_url("index")))


def getParam(request, name, transforms=[], raises=False, default=None):
    if name not in request.params:
        return default
        
    result = request.params[name] 
    for transform in transforms:
        try:
            result = transform(result) 
        except:
            if raises:
                raise
            else:
                return default
    return result


def getRatingsByAuthor(request, authId, limit):
    return _getRatings(Rating.getByAuthor, request, authId, limit,
        showPlayer=True, showAuthor=False)

def getRatingsByPlayer(request, playerId, limit):
    return _getRatings(Rating.getByPlayer,
        request, playerId, limit, showPlayer=False, showAuthor=True)

def _getRatings(rating_func, request, id, limit, showPlayer=True,
        showAuthor=True):
    """
        Return ratings, prevLink, and nextLink.
        Works with 'user_posts' and 'discussion' views

        rating_func is either Rating.getByPlayer or Rating.getByAuthor
        id is either authId or playerId
    """
    # check param 'after' and 'before'
    offset = getParam(request, "offset", transforms=[int], default=0)
    posts = []

    dateTransforms = [float, datetime.utcfromtimestamp]

    beforeDT = afterDT = None
    if "after" in request.GET:
        afterDT = getParam(request, "after", transforms=dateTransforms)
    elif "before" in request.GET:
        beforeDT = getParam(request, "before", transforms=dateTransforms)

    if afterDT:
        posts = rating_func(id, limit=limit, afterDT=afterDT)
    elif beforeDT:
        posts = rating_func(id, limit=limit, offset=offset, beforeDT=beforeDT)
    else:
        posts = rating_func(id, limit=limit)


    nextLink = None
    if len(posts) == limit:
        afterTS = calendar.timegm(posts[-1].createTime.timetuple())
        nextLink = "{0}?offset={1}&after={2}".format(
            request.current_route_path(**request.matchdict), offset + limit, afterTS)

    prevLink = None
    if offset > 0 and len(posts) > 0:
        beforeTS = calendar.timegm(posts[0].createTime.timetuple())
        prevLink = "{0}?offset={1}&before={2}".format(
            request.current_route_path(**request.matchdict), offset - limit, beforeTS)

    return {
        "ratings": posts,
        "nextLink": nextLink,
        "prevLink": prevLink,
        "showPlayer": showPlayer,
        "showAuthor": showAuthor,
        "paginate": True,
    }
