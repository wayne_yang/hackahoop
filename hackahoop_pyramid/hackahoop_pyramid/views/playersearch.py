from hackahoop_models.meta import DBSession
from hackahoop_models.playerinfo import PlayerInfo
from sqlalchemy import or_
from pyramid.view import view_config

import logging
log = logging.getLogger(__name__)

@view_config(route_name='playersearch', renderer='json')
def playersearch_json_view(request):
    GET = request.GET
    if 'term' not in GET:
        return Response("Param term is required", content_type='text/plain', status_int=400)

    term = GET.get('term').strip()
    limit = 10

    players = PlayerInfo.searchActivePlayer(term)

    result = []
    for player in players:
        result.append({
            "id": player.seoName,
            "label": "{0} - {1} {2}".format(player.abbreviation, player.firstName, player.lastName),
            "value": player.seoName,
            "playerName": "{0} {1}".format(player.firstName, player.lastName),
        })
    return result
