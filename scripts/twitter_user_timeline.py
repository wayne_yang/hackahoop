from twitter_api import user_timeline
import json
import sys
import argparse

parser = argparse.ArgumentParser(description='Fetch twitter timeline data')
parser.add_argument("screenName", nargs="+", help="a twitter screen name")
parser.add_argument("--sinceId", help="Set the since_id for the timeline call")
args = parser.parse_args()


if len(sys.argv) < 2:
    sys.exit("Usage {0} screenName1 screenName2 ...".format(sys.argv[0]))


for screenName in sys.argv[1:]:
    params = {
        "screen_name": screenName,
        "include_rts": "true",
        "count": 200,
    }

    lastMaxId = sys.maxint
    data = json.loads(user_timeline(params))
    result = []

    while len(data) > 0 and data[0]["id"] < lastMaxId:
        result = result + data

        # max id is inclusive so substract 1 since that tweet was processed already in last request
        lastMaxId = data[-1]["id"] - 1
        params['max_id'] = lastMaxId
        sys.stderr.write("lasteMaxId={0} count={1}\n".format(lastMaxId, len(data)))
        data = json.loads(user_timeline(params))
        #sys.stderr.write("newData[0][id]={0}\n".format(data[0]["id"]))

    print json.dumps(result)
