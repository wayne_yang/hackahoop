from .meta import Base, DBSession
from .account import AuthId
from .playerinfo import PlayerInfo

from sqlalchemy import Column, ForeignKey, Unicode, Integer, DateTime, UnicodeText, desc, asc
from sqlalchemy.orm import relationship, contains_eager
from sqlalchemy.sql import func

from datetime import datetime

from markdown import markdown
import bleach

ALLOWED_TAGS = ["p", "h1", "h2", "h3", "h4", "h5", "h6", "pre"] + bleach.ALLOWED_TAGS

def sanitizeInput(html):
    return bleach.clean(html, tags=[])

class RatingDataError(Exception): pass

class Rating(Base):
    __tablename__ = "rating"
    UpValue = 1
    DownValue = -1

    id = Column(Integer, primary_key=True)
    authorId = Column("author_id", Integer, ForeignKey(AuthId.id))
    playerId = Column("player_id", Integer, ForeignKey(PlayerInfo.playerId))
    title = Column(Unicode)
    ratingValue = Column("rating_value", Integer)
    content = Column(UnicodeText)
    createTime = Column("create_time", DateTime)

    author = relationship("AuthId")
    player = relationship("PlayerInfo")
    
    def __repr__(self):
        s = (u"<Rating(id={0}, authorId={1}, title={2}, ratingValue={3},"
            ",createTime={4}, content={5})>")
        return s.format(self.id, self.authorId, self.title, self.ratingValue,
            self.content, self.createTime)

    def getDisplayContent(self):
        return bleach.clean(markdown(self.content), tags=ALLOWED_TAGS)

    def getCreateTime(self, fmt="%b %d, %Y"):
        return self.createTime.strftime(fmt)

    @classmethod
    def getByPlayer(cls, playerId, limit=20, offset=0, afterDT=None, beforeDT=None):
        query = (DBSession.query(cls)
            .filter(cls.playerId == playerId))

        if afterDT is not None:
            query = query.filter(cls.createTime < afterDT)

        if beforeDT is not None:
            query = query.filter(cls.createTime > beforeDT)
       
        return query.order_by(desc(cls.createTime))[offset:offset+limit]

    @classmethod
    def getByPlayers(cls, players, limit=20, offset=0, afterDT=None, beforeDT=None):
        """
        players - list of playerIds
        """
        query = (DBSession.query(cls)
            .filter(cls.playerId.in_(players)))

        if afterDT is not None:
            query = query.filter(cls.createTime < afterDT)

        if beforeDT is not None:
            query = query.filter(cls.createTime > beforeDT)
       
        return query.order_by(desc(cls.createTime))[offset:offset+limit]


    @classmethod
    def getByAuthor(cls, authId, limit=20, offset=0, afterDT=None, beforeDT=None):
        query = (DBSession.query(cls)
            .join(cls.player)
            .options(contains_eager(cls.player))
            .filter(cls.authorId == authId))

        if afterDT is not None:
            query = query.filter(cls.createTime < afterDT)

        if beforeDT is not None:
            query = query.filter(cls.createTime > beforeDT)

        return query.order_by(desc(cls.createTime))[offset:offset+limit]

    @classmethod
    def getRatingCount(cls, playerId):
        rows = (DBSession.query(cls.ratingValue, func.count(cls.ratingValue))
            .filter(cls.playerId == playerId)
            .group_by(cls.ratingValue).all())
        
        result = {}
        for row in rows:
            result[row[0]] = row[1]

        if cls.UpValue not in result:
            result[cls.UpValue] = 0
        if cls.DownValue not in result:
            result[cls.DownValue] = 0

        return result

    @classmethod
    def getCountByAuthor(cls, authId):
         return (DBSession.query(cls)
            .filter(cls.authorId == authId)
            .count())


    @classmethod
    def add(cls, authorId, playerId, title, ratingValue, content):
        """
        Raise RatingDataError on invalid input
        """
        validateRatingData(authorId, playerId, title, ratingValue, content)

        # content = sanitizeInput(content)

        # sanitize title but store raw content 
        title = sanitizeInput(title)
        createTime = datetime.now()

        rating = cls(authorId=authorId, playerId=playerId, title=title, ratingValue=ratingValue,
            content=content, createTime=createTime)
        DBSession.add(rating)
        return rating

    @classmethod
    def remove(cls, authorId, rateId):
        (DBSession.query(cls)
            .filter(cls.id == rateId)
            .filter(cls.authorId == authorId)
            .delete())

def validateRatingData(authorId, playerId, title, ratingValue, content):
    checkInt("user", authorId)
    checkInt("player", playerId)
 
    if title is None or title.strip() == "":
        raise RatingDataError("Title is missing or empty")

    ratingValue = checkInt("ratingValue", ratingValue)
    if ratingValue != -1 and ratingValue != 1:
        raise RatingDataError("Invalid rating value: {0}".format(ratingValue))
        
    if content is None or content.strip() == "":
        raise RatingDataError("Content is missing or empty")
    

def checkInt(key, val):
    try:
        val = int(val)
    except ValueError:
        raise RatingDataError("Invalid {0} value: {1}".format(key, val))
    return val
