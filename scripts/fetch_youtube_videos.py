import youtube_api
import json

data = open("youtube.keywords").readlines()
for line in data:
    line = line.rstrip()
    playerId, keyword = line.split(",")

    resp = youtube_api.search({
        "q": keyword,
        "type": "video",
        "maxResults": 25,
    })

    jsonStr = json.dumps(resp)
    f = open("youtube/{0}.json".format(playerId), "w")
    f.write(jsonStr)
    f.close()
