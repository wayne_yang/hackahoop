import requests
import os
from hackahoop_models.gamelog import GameLog
from hackahoop_models.playerinfo import PlayerInfo
from hackahoop_models.team import Team
from bs4 import BeautifulSoup
import logging
import utils
import datetime
import nba
import transaction
import urllib
from sqlalchemy.exc import IntegrityError

def getLogger():
    #logging.basicConfig(filename="./sync_nba_rosters.log", level=logging.DEBUG)

    logger = logging.getLogger(__name__)
    logger.setLevel(logging.DEBUG)
    # create formatter
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
    ch = logging.StreamHandler()
    ch.setFormatter(formatter)
    logger.addHandler(ch)
    return logger

logger = getLogger()

class GameNotFinalError(Exception):
    def __repr__(self):
        return "Game is not final"
    def __str__(self):
        return "Game is not final"

class GameNotExist(Exception):
    def __repr__(self):
        return "Game does not exist"
    def __str__(self):
        return "Game does not exist"



class ParseError(Exception):
    def __init__(self, msg):
        self.msg = msg
    def __str__(self):
        return repr(self.msg)

class NbaBase(object):
    _ua = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/30.0.1599.101 Safari/537.36"


class NbaStatBoxScoreFetcher(NbaBase):
    """
    http://data.nba.com/data/5s/xml/nbacom/2013/scores/20131029/CHIMIA/boxscore.xml?q=1382475866488
    """
    __apiUrl = "http://data.nba.com/data/5s/xml/nbacom/{year}/scores/{yyyymmdd}/{matchup}/boxscore.xml"

    def __init__(self, session, commitNewUser=True):
        self._nbaPlayers = None
        self._session = session
        self._commitNewUser = commitNewUser


    def fetch(self, gameId, gameDate, homeTeamId, visitTeamId):
        """
        gameDate - an instance of datetime
        returns a list of GameLog
        """
        homeTeam = Team.getById(homeTeamId)
        visitTeam = Team.getById(visitTeamId)
        matchup = "{visitTeam}{homeTeam}".format(
            homeTeam=homeTeam.abbreviation,
            visitTeam=visitTeam.abbreviation,
        )
        url = self.__apiUrl.format(
            year=gameDate.year,
            yyyymmdd=gameDate.strftime("%Y%m%d"),
            matchup=matchup
        )
        response = requests.get(url, headers={
            "User-Agent": self._ua,
        })

        # throws requests.exceptions.HTTPError if on error
        response.raise_for_status()

        xml = response.text
        """
        xml = open("boxscore.xml").read()
        """
        bs = BeautifulSoup(xml, "xml")

        # throw ParseError
        gamelogs = self._parse(bs, gameDate, homeTeam, visitTeam)
        for g in gamelogs:
            g.gameId = gameId

        return gamelogs

    def _parse(self, bs, gameDate, homeTeam, visitTeam):
        seasonId = utils.getSeasonFromDate(gameDate)
        gameNode = bs.find("game")

        if "gstattxt" not in gameNode.attrs or gameNode["gstattxt"] != "Final":
            raise GameNotFinalError()

        htm = bs.find("htm")
        vtm = bs.find("vtm")
        htmScore = int(htm["scr"].split("|")[-1])
        vtmScore = int(vtm["scr"].split("|")[-1])
        homeWin = False
        if htmScore > vtmScore:
            homeWin = True

        homeTeamPlayers = htm.find_all("pl")
        visitTeamPlayers = vtm.find_all("pl")

        # result - list of gamelog
        result = []

        for pl in homeTeamPlayers:
            if pl["dnp"] == "DNP":
                continue

            # fill gamelog
            gamelog = self._parsePlayerEntry(seasonId, gameDate, pl, True,
                homeWin, homeTeam, visitTeam) 
            if gamelog is None:
                continue
            result.append(gamelog)

        for pl in visitTeamPlayers:
            if pl["dnp"] == "DNP":
                continue

            # fill gamelog
            gamelog = self._parsePlayerEntry(seasonId, gameDate, pl, False,
                homeWin, homeTeam, visitTeam) 

            if gamelog is None:
                continue
            result.append(gamelog)

        return result

    def _fetchAndMergePlayerInfo(self, firstName, lastName, team):
        playerinfo = utils.findActivePlayer(firstName, lastName, team.id)
        if playerinfo is None:
            logger.warn("{lastName}, {firstName} from {team} does not exist in DB".format(
                lastName=lastName,
                firstName=firstName,
                team=team.abbreviation))

            # fetch player info
            if self._nbaPlayers is None:
                self._nbaPlayers = NbaPlayers()

            infos = self._nbaPlayers.fetchPlayerInfo(firstName + " " + lastName)
            logger.debug("Fetched player info(s) to add: {0}".format(infos))

            infoCount = len(infos)
            if infoCount == 1:
                playerinfo = infos[0]
                logger.debug("merging to DB: {0}".format(playerinfo))
                self._session.merge(playerinfo)

            elif infoCount > 1:
                # match the team id
                logger.debug("Found more than one playerinfo.  Matching teamId")
                infos = [i for i in infos if i.teamId == team.id]
                if not infos:
                    logger.debug("Failed to find matching teamId {0}".format(team.id))
                    return None

                playerinfo = infos[0]
                logger.debug("Merging to DB: {0}".format(playerinfo))
                self._session.merge(playerinfo)

            if infoCount > 0:
                try:
                    self._session.commit()
                except IntegrityError as e:
                    logger.error("Failed to add new users: {0}".format(e))

        return playerinfo
            
    def _parsePlayerEntry(self, seasonId, gameDate, pl, isHomeTeam, homeWin,
        homeTeam, visitTeam):
        """
        Return a game log
        """
        lastName = firstName = None

        try:
            lastName, firstName = pl["name"].split("|")[2].split(", ")
        except ValueError as e:
            raise ParseError("Failed to parse name from {0}: {1}".format(pl["name"], e))


        # fetch playerinfo to get player id
        team = homeTeam if isHomeTeam else visitTeam
        playerinfo = self._fetchAndMergePlayerInfo(firstName, lastName, team)
        if playerinfo is None:
            return None

        stats = pl["stat"].split("|")
        curTeam = opponent = None
        if isHomeTeam:
            curTeam = homeTeam.abbreviation
            opponent = visitTeam.abbreviation
        else:
            curTeam = visitTeam.abbreviation
            opponent = homeTeam.abbreviation

        g = GameLog(
            seasonId=seasonId,
            gameId=None,
            gameDate=gameDate,
            playerId=playerinfo.playerId,
            curTeam=curTeam,
            opponent=opponent,
            homeGame=isHomeTeam,
            win=(homeWin if isHomeTeam else not homeWin),
            seasonType=GameLog.REGULAR_SEASON
        )

        # populate stats
        g.min = utils.parseMinutes(stats[0])
        g.fgm, g.fga, g.fgPCT = self._parseShooting(stats[1])
        g.fg3m, g.fg3a, g.fg3PCT = self._parseShooting(stats[2])
        g.ftm, g.fta, g.ftPCT = self._parseShooting(stats[3])
        try:
            g.oreb = int(stats[4])
            g.dreb = int(stats[5])
            g.reb = int(stats[6])
            g.ast = int(stats[7])
            g.pf = int(stats[8])
            g.stl = int(stats[9])
            g.tov = int(stats[10])
            g.blk = int(stats[11])
            g.pts = int(stats[14])
        except ValueError as e:
            raise ParseError(repr(e))

        s = ("mint={min}, fgm={fgm} fga={fga} fgPCT={fgPCT} fg3m={fg3m} fg3a={fg3a} "
            "fg3PCT={fg3PCT}, ftm={ftm} fta={fta} ftPCT={ftPCT} oreb={oreb} "
            "dreb={dreb} reb={reb} ast={ast} pf={pf} stl={stl} tov={tov} "
            " blk={blk} pts={pts}")
        return g

    def _parseShooting(self, shootingStat):
        made, attempt = shootingStat.split("-")
        made = int(made)
        attempt = int(attempt)
        pct = float("{0:.3f}".format(made/float(attempt))) if attempt != 0 else 0
        return (made, attempt, pct)


class NbaPlayers(NbaBase):
    _AllPlayersUrl = "http://stats.nba.com/stats/commonallplayers/?LeagueID=00&Season={season}&IsOnlyCurrentSeason={onlyCurrentSeason}"
    _PlayerInfoUrl = "http://stats.nba.com/stats/commonplayerinfo/?PlayerID={playerId}&SeasonType=Regular+Season"


    def __init__(self, seasonId=None, onlyCurrentSeason=True):
        if seasonId is None:
            seasonId = utils.getSeasonFromDate(datetime.datetime.utcnow())
        
        self._seasonId = seasonId
        self._onlyCurrentSeason = onlyCurrentSeason
        self._fullNameDict = None


    def _getFullNameDict(self):
        url = self._AllPlayersUrl.format(
            season=self._getSeasonStr(self._seasonId),
            onlyCurrentSeason=(1 if self._onlyCurrentSeason else 0),
        )
        response = requests.get(url, headers={
            "User-Agent": self._ua,
        })

        # throws requests.exceptions.HTTPError if on error
        response.raise_for_status()
        data = response.json()
        return self._parseIntoFullNameDict(data)

    def getByFullName(self, fullName):
        if self._fullNameDict is None:
            self._fullNameDict = self.getFullNameDict()

        # hack, nba does not like Toure' Murry
        fullName = fullName.replace("'", "")

        if fullName not in self._fullNameDict:
            return []

        playerMeta = self._fullNameDict[fullName]
        result = []
        if len(playerMeta) == 1:
            # no other player with the same name
            playerId = playerMeta[0][0]
            playerData = self.getById(playerId)
            result.append(utils.convertToPlayerInfo(playerData))
        else:
            for meta in playerMeta:
                pinfo = utils.convertToPlayerInfo(self.getById(meta[0]))
                pinfo.seoName = utils.getSEOName(
                    pinfo.firstName,
                    pinfo.lastName,
                    pinfo.suffix,
                    pinfo.school)
                result.append(pinfo)
        return result 

        
    def getById(self, playerId):
        url = self._PlayerInfoUrl.format(playerId=playerId)
        response = requests.get(url, headers={ "User-Agent": self._ua, })

        # throws requests.exceptions.HTTPError if on error
        response.raise_for_status()

        jsonData = response.json()
        for resultSet in jsonData["resultSets"]:
            if resultSet["name"] =="CommonPlayerInfo":
                return utils.convertResultSetToDict(resultSet)[0]
        return None
                

    def _parseIntoFullNameDict(self, data):
        players = data["resultSets"][0]["rowSet"]
        result = {}
        for p in players:
            playerId, lastFirstName = p[0], p[1]
            nameParts = lastFirstName.split(", ")
            if len(nameParts) != 2:
                continue

            firstName, lastName = nameParts[1], nameParts[0]
            fullName = firstName + " " + lastName
            if not fullName in result:
                result[fullName] = []

            result[fullName].append(p)
        return result


    def _getSeasonStr(self, seasonId):
        return "{0}-{1}".format(seasonId, (seasonId + 1 - 2000))


class ScoreBoard(NbaBase):
    """
    http://stats.nba.com/stats/scoreboard/?LeagueID=00&gameDate=11%2F06%2F2013&DayOffset=0&date=Wed+Nov+06+2013+00%3A00%3A00+GMT-0800+
    """
    ScoreBoardUrl = "http://stats.nba.com/stats/scoreboard/?LeagueID=00&gameDate={gameDate}&DayOffset=0&date={dateStr}"
    BoxScoreUrl = "http://stats.nba.com/stats/boxscore?GameID={gameId}&RangeType=0&StartPeriod=0&EndPeriod=0&StartRange=0&EndRange=0"

    GAME_STATUS_FINAL = 3
        
    def __init__(self, session, gameDate):
        """
        Game Date on 11/03 is to get NBA games on 11/03 in US timezone
        """
        self._session = session
        self._gameDate = gameDate
        gameDateStr = urllib.quote_plus(self._gameDate.strftime("%m/%d/%Y"))
        dateStr = urllib.quote_plus(
            self._gameDate.strftime("%a %b %d %Y 00:00:00 GMT-0800 (PST)"))
        url = self.ScoreBoardUrl.format(gameDate=gameDateStr, dateStr=dateStr)

        response = requests.get(url, headers={
            "User-Agent": self._ua,
        })
        response.raise_for_status()
    
        # get GameHeader data
        gameHeader = None
        jsonData = response.json()
        for result in jsonData["resultSets"]:
            if result["name"] == "GameHeader":
                self._gameHeader = self._parseGameHeaderResultSet(result)
            elif result["name"] == "LineScore":
                self._lineScore = self._parseLineScoreResultSet(result)

        if self._gameHeader is None:
            raise ParseError("Fetch result does not contain GameHeader data")

    def _parseLineScoreResultSet(self, resultSet):
        lsDict = utils.convertResultSetToDict(resultSet)
        result = {}
        for d in lsDict:
            key = self._makeLineScoreKey(d)
            result[key] = d
        return result 


    def _parseGameHeaderResultSet(self, resultSet):
        ghDict = utils.convertResultSetToDict(resultSet)
        result = {}
        for d in ghDict:
            key = self._makeGameHeaderKey(d)
            result[key] = d
        return result 

    def _makeGameHeaderKey(self, row):
        return (row["HOME_TEAM_ID"], row["VISITOR_TEAM_ID"])

    def _makeLineScoreKey(self, row): 
        return row["TEAM_ID"]

    def getLineScore(self, teamId):
        return self._lineScore[teamId]

    def fetch(self, homeTeamId, visitTeamId):
        keyData = {"HOME_TEAM_ID": homeTeamId, "VISITOR_TEAM_ID": visitTeamId}
        key = self._makeGameHeaderKey(keyData)

        # check if the game exists in the given game header
        if key not in self._gameHeader:
            raise GameNotExist()

        gh = self._gameHeader[key] 
        if gh["GAME_STATUS_ID"] != self.GAME_STATUS_FINAL:
            raise GameNotFinalError()

        url = self.BoxScoreUrl.format(gameId=gh["GAME_ID"])
        response = requests.get(url, headers={
            "User-Agent": self._ua,
        })
        response.raise_for_status()

        playerStats = None
        jsonData = response.json()
        for resultSet in jsonData["resultSets"]:
            if resultSet["name"] == "PlayerStats":
                playerStats = utils.convertResultSetToDict(resultSet)
                break

        seasonId = utils.getSeasonFromDate(self._gameDate)

        # get lineScore to determine who wins
        htLineScore = self.getLineScore(homeTeamId)
        vtLineScore = self.getLineScore(visitTeamId)

        gamelogs = []
        for stat in playerStats:
            if stat["MIN"] is None:
                # reason is stat["COMMENT"]
                continue

            if stat["MIN"] is None:
                print "MIN is None", stat
 

            win = isHome = False
            curTeam = opponent = None
            if stat["TEAM_ID"] == homeTeamId:
                isHome = True
                curTeam = htLineScore["TEAM_ABBREVIATION"]
                opponent = vtLineScore["TEAM_ABBREVIATION"]
                win = True if htLineScore["PTS"] > vtLineScore else False
            else:
                opponent = htLineScore["TEAM_ABBREVIATION"]
                curTeam = vtLineScore["TEAM_ABBREVIATION"]
                win = True if vtLineScore["PTS"] > htLineScore else False
              
            # leave out game id 
            gl = GameLog(
                seasonId=seasonId,
                playerId=stat["PLAYER_ID"],
                gameDate=self._gameDate,
                curTeam=curTeam,
                opponent=opponent,
                homeGame=isHome,
                win=win,
                min=utils.parseMinutes(stat["MIN"]),
                fgm=stat["FGM"],
                fga=stat["FGA"],
                fgPCT=stat["FG_PCT"],
                fg3m=stat["FG3M"],
                fg3a=stat["FG3A"],
                fg3PCT=stat["FG3_PCT"],
                ftm=stat["FTM"],
                fta=stat["FTA"],
                ftPCT=stat["FT_PCT"],
                oreb=stat["OREB"],
                dreb=stat["DREB"],
                reb=stat["REB"],
                ast=stat["AST"],
                stl=stat["STL"],
                blk=stat["BLK"],
                tov=stat["TO"],
                pf=stat["PF"],
                pts=stat["PTS"],
                seasonType=GameLog.REGULAR_SEASON)
        
            gamelogs.append(gl)
        return gamelogs    


if __name__ == "__main__":
    cfgFile = "../../hackahoop_cron/boxscore_fetcher.ini"
    settings = utils.getConfigSettings(cfgFile)
    Session = utils.initDB(settings["boxscore_fetcher"]) 
    session = Session()
    sb = ScoreBoard(session, datetime.date(2013, 11, 03))
    gameHeader = sb.fetch(1610612747, 1610612737)
    """

    import datetime
    dt = datetime.date(2013, 10, 23)
    fetcher = NbaStatBoxScoreFetcher()

    # nets
    visitTeamId = 1610612751

    # celtics
    homeTeamId = 1610612738
    fetcher.fetch(dt, homeTeamId, visitTeamId)


    nbaPlayers = NbaPlayers()
    print nbaPlayers.getByFullName("Chris Johnson")
    """
