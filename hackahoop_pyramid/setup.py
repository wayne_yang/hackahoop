import os

from setuptools import setup, find_packages

here = os.path.abspath(os.path.dirname(__file__))
README = open(os.path.join(here, 'README.txt')).read()
CHANGES = open(os.path.join(here, 'CHANGES.txt')).read()

requires = [
    'pyramid',
    'SQLAlchemy',
    'transaction',
    'pyramid_tm',
    'pyramid_debugtoolbar',
    'pyramid_beaker',
    'zope.sqlalchemy',
    'waitress',
    'MySQL-python',
    'velruse',
    'pycrypto',
    'requests',
    'hackahoop_models',
    ]

setup(name='hackahoop_pyramid',
      version='0.0.9',
      description='HackAHoop Pyramid Web App',
      long_description=README + '\n\n' + CHANGES,
      classifiers=[
        "Programming Language :: Python",
        "Framework :: Pyramid",
        "Topic :: Internet :: WWW/HTTP",
        "Topic :: Internet :: WWW/HTTP :: WSGI :: Application",
        ],
      author='',
      author_email='',
      url='',
      keywords='web wsgi bfg pylons pyramid',
      packages=find_packages(),
      include_package_data=True,
      zip_safe=False,
      test_suite='hackahoop_pyramid.tests',
      install_requires=requires,
      entry_points="""\
      [paste.app_factory]
      main = hackahoop_pyramid:main
      [console_scripts]
      initialize_analyticsWeb_db = hackahoop_pyramid.scripts.initializedb:main
      """,
      )
