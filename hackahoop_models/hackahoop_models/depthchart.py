import playerinfo
from sqlalchemy.orm import relationship
from .meta import Base, DBSession
from sqlalchemy.orm import contains_eager
from sqlalchemy import asc
from sqlalchemy import (
    Column,
    DateTime,
    Integer,
    SmallInteger,
    String,
    Text,
    Unicode,
    ForeignKey)

class DepthChart(Base):
    __tablename__ = "depth_chart"
    teamId = Column("TEAM_ID", Integer, primary_key=True)
    position = Column("POSITION", String, primary_key=True)
    depth = Column("DEPTH", SmallInteger, primary_key=True)
    playerId = Column("PLAYER_ID", Integer, ForeignKey('playerinfo.PLAYER_ID'))
    player = relationship("PlayerInfo", innerjoin=True)

    @classmethod
    def get(cls, teamId, position):
        return (DBSession.query(cls)
            .join(cls.player)
            .options(contains_eager(cls.player))
            .filter(cls.teamId == teamId)
            .filter(cls.position == position)
            .order_by(asc(cls.depth))
            .all())



    def __repr__(self):
        s = (u"<DepthChart(teamId={0},position={1},depth={2},"
            "playerId={3})>")
        return s.format(self.teamId, self.position, self.depth, self.playerId)

