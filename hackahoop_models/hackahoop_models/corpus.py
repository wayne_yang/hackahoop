from .meta import Base, DBSession
from sqlalchemy import (Column, Integer, String, UnicodeText, Unicode)
from markdown import markdown


class Corpus1(Base):
    __tablename__ = "corpus1"
    id = Column(String, primary_key=True)
    url = Column(Unicode)
    title = Column(Unicode)
    content_type = Column(String)
    news_label = Column(Integer)
    news_labeler_id = Column(Integer)
    injury_label = Column(Integer)
    tokens = Column(UnicodeText)
    tokens_length = Column(Integer)
    raw_text = Column(UnicodeText)

    def __repr__(self):
        s = (u"Corpus(id={}, url={}, content_type={}, news_label={}, injury_label={})")
        return s.format(
            self.id,
            self.url,
            self.content_type,
            self.news_label,
            self.injury_label,
        )

    @classmethod
    def getById(cls, docId):
        return DBSession.query(cls).filter(cls.id == docId).one()

    @classmethod
    def get(cls, offset=0, limit=10):
        query = (DBSession.query(cls)
            .order_by(cls.id))
        return query[offset:offset+limit]

    @classmethod
    def get_query(cls):
        return (DBSession.query(cls)
            .filter(cls.tokens_length >= 100)
            .order_by(cls.id))
            
    def get_html_text(self):
        return markdown(self.raw_text)
