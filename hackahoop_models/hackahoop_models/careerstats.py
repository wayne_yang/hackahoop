from .meta import Base, DBSession
from sqlalchemy import (
    Column,
    SmallInteger,
    Integer,
    Numeric,
    String,
    Date,
    ForeignKey,
    desc)
from sqlalchemy.orm import relationship
from .playerinfo import PlayerInfo
from json import JSONEncoder
from utils import ModelJsonEncoder


class CareerStats(Base):
    __tablename__ = 'playercareerstats'

    playerId = Column('PLAYER_ID', Integer, ForeignKey('playerinfo.PLAYER_ID'), primary_key=True,)
    seasonId = Column('SEASON_ID', Integer, primary_key=True)
    leagueId = Column('LEAGUE_ID', Integer, primary_key=True)
    teamId = Column('TEAM_ID', Integer, primary_key=True)
    teamAbbreviation = Column('TEAM_ABBREVIATION', String)
    playerAge = Column('PLAYER_AGE', SmallInteger)
    gp = Column('GP', SmallInteger)
    gs = Column('GS', SmallInteger)
    min = Column('MIN', Numeric)
    fgm = Column('FGM', Numeric)
    fga = Column('FGA', Numeric)
    fgPCT = Column('FG_PCT', Numeric)
    fg3m = Column('FG3M', Numeric)
    fg3a = Column('FG3A', Numeric)
    fg3PCT = Column('FG3_PCT', Numeric)
    ftm = Column('FTM', Numeric)
    fta = Column('FTA', Numeric)
    ftPCT = Column('FT_PCT', Numeric)
    oreb = Column('OREB', Numeric)
    dreb = Column('DREB', Numeric)
    reb = Column('REB', Numeric)
    ast = Column('AST', Numeric)
    stl = Column('STL', Numeric)
    blk = Column('BLK', Numeric)
    tov = Column('TOV', Numeric)
    pf = Column('PF', Numeric)
    pts = Column('PTS', Numeric)

    #  Link to the PlayerInfo class
    player = relationship("PlayerInfo", primaryjoin="CareerStats.playerId == PlayerInfo.playerId",)
    #playerInfo = relationship("PlayerInfo",
    #    primaryjoin="and_(CareerStats.playerId == PlayerInfo.playerId)")


    def toDict(self):
        return {"seasonId": self.seasonId,
                "playerId": self.playerId,
                "team": self.teamAbbreviation,
                "playerAge": self.playerAge,
                "gp": self.gp,
                "gs": self.gs,
                "min": self.min,
                "fgm": self.fgm,
                "fga": self.fga, 
                "fgPCT":  self.fgPCT,
                "fg3m": self.fg3m,
                "fg3a": self.fg3a,
                "fg3PCT": self.fg3PCT,
                "ftm": self.ftm,
                "fta": self.fta,
                "ftPCT": self.ftPCT, 
                "oreb": self.oreb,
                "dreb": self.dreb,
                "reb": self.reb,
                "ast": self.ast,
                "stl": self.stl,
                "blk": self.blk, 
                "tov": self.tov,
                "pf": self.tov,
                "pts": self.pts }

    def __repr__(self):
        s = ("<CareerStats(playerId={0},seasonId={1},leagueId={2},teamId={3},"
            "teamAbbreviation={4},playerAge={5},gp={6},gs={7},min={8},fgm={9},"
            "fga={10},fgPCT={11},fg3m={12},fg3a={13},fg3PCT={14},ftm={15},"
            "fta={16},ftPCT={17},oreb={18},dreb={19},reb={20},ast={21},stl={22},"
            "blk={23},tov={24},pf={25},pts={26})")
        return s.format(self.playerId, self.seasonId, self.leagueId,
                self.teamId, self.teamAbbreviation, self.playerAge, self.gp,
                self.gs, self.min, self.fgm, self.fga, self.fgPCT, self.fg3m,
                self.fg3a, self.fg3PCT, self.ftm, self.fta, self.ftPCT,
                self.oreb, self.dreb, self.reb, self.ast, self.stl, self.blk,
                self.tov, self.pf, self.pts)

class CareerStatsJsonEncoder(ModelJsonEncoder):
    def default(self, obj):
        if isinstance(obj, CareerStats) or isinstance(obj, CombinedCareerStats):
            return obj.toDict()

        return ModelJsonEncoder.default(self, obj)
            

class CombinedCareerStats(Base):
    __tablename__ = 'overall_careerstats'

    playerId = Column('PLAYER_ID', Integer, ForeignKey('playerinfo.PLAYER_ID'), primary_key=True,)
    seasonId = Column('SEASON_ID', Integer, primary_key=True)
    gp = Column('GP', SmallInteger)
    gs = Column('GS', SmallInteger)
    min = Column('MIN', Numeric)
    fgm = Column('FGM', Numeric)
    fga = Column('FGA', Numeric)
    fgPCT = Column('FG_PCT', Numeric)
    fg3m = Column('FG3M', Numeric)
    fg3a = Column('FG3A', Numeric)
    fg3PCT = Column('FG3_PCT', Numeric)
    ftm = Column('FTM', Numeric)
    fta = Column('FTA', Numeric)
    ftPCT = Column('FT_PCT', Numeric)
    oreb = Column('OREB', Numeric)
    dreb = Column('DREB', Numeric)
    reb = Column('REB', Numeric)
    ast = Column('AST', Numeric)
    stl = Column('STL', Numeric)
    blk = Column('BLK', Numeric)
    tov = Column('TOV', Numeric)
    pf = Column('PF', Numeric)
    pts = Column('PTS', Numeric)

    #  Link to the PlayerInfo class
    player = relationship("PlayerInfo", primaryjoin="CombinedCareerStats.playerId == PlayerInfo.playerId",)
    #playerInfo = relationship("PlayerInfo",
    #    primaryjoin="and_(CareerStats.playerId == PlayerInfo.playerId)")

    @classmethod
    def getByPlayerId(cls, playerId):
        return (DBSession.query(CombinedCareerStats)
            .filter(CombinedCareerStats.playerId == playerId)
            .order_by(desc(CombinedCareerStats.seasonId)).first())

    def toDict(self):
        return {"seasonId": self.seasonId,
                "playerId": self.playerId,
                "gp": self.gp,
                "gs": self.gs,
                "min": self.min,
                "fgm": self.fgm,
                "fga": self.fga, 
                "fgPCT":  self.fgPCT,
                "fg3m": self.fg3m,
                "fg3a": self.fg3a,
                "fg3PCT": self.fg3PCT,
                "ftm": self.ftm,
                "fta": self.fta,
                "ftPCT": self.ftPCT, 
                "oreb": self.oreb,
                "dreb": self.dreb,
                "reb": self.reb,
                "ast": self.ast,
                "stl": self.stl,
                "blk": self.blk, 
                "tov": self.tov,
                "pf": self.tov,
                "pts": self.pts }

    def __repr__(self):
        s = ("<CombinedCareerStats(playerId={0},seasonId={1},"
            "gp={2},gs={3},min={4},fgm={5},"
            "fga={6},fgPCT={7},fg3m={8},fg3a={9},fg3PCT={10},ftm={11},"
            "fta={12},ftPCT={13},oreb={14},dreb={15},reb={16},ast={17},stl={18},"
            "blk={19},tov={20},pf={21},pts={22})")
        return s.format(self.playerId, self.seasonId, self.gp,
                self.gs, self.min, self.fgm, self.fga, self.fgPCT, self.fg3m,
                self.fg3a, self.fg3PCT, self.ftm, self.fta, self.ftPCT,
                self.oreb, self.dreb, self.reb, self.ast, self.stl, self.blk,
                self.tov, self.pf, self.pts)

# TODO:
MaxStat = {
    "fgPCT": 1.0,
    "fg3m": 3.50,
    "ftPCT": 1.0,
    "reb": 14.4,
    "pts": 28.7,
    "ast": 11.1,
    "stl": 2.40,
    "blk": 3.00,
    "tov": 3.9,
}
"""
mysql> select max(fg_pct), max(fg3m), max(ft_pct), max(reb), max(ast), max(stl), max(blk), max(tov), max(pts) from overall_careerstats where season_id = 2012 ;
+-------------+-----------+-------------+----------+----------+----------+----------+----------+----------+
| max(fg_pct) | max(fg3m) | max(ft_pct) | max(reb) | max(ast) | max(stl) | max(blk) | max(tov) | max(pts) |
+-------------+-----------+-------------+----------+----------+----------+----------+----------+----------+
|       1.000 |       3.5 |       1.000 |     14.4 |     11.1 |      2.4 |      3.0 |      3.9 |     28.7 |
+-------------+-----------+-------------+----------+----------+----------+----------+----------+----------+
"""
