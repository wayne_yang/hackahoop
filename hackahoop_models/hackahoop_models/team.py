from sqlalchemy.orm import relationship
from .meta import Base, DBSession
from sqlalchemy import (
    Column,
    DateTime,
    Integer,
    SmallInteger,
    String,
    Text,
    Unicode,
    ForeignKey)


class Team(Base):
    __tablename__ = "team"
    id = Column("id", Integer, primary_key=True)
    name = Column("name", Unicode)
    abbreviation = Column("abbreviation", Unicode)
    code = Column("code", Unicode)
    city = Column("city", Unicode)

    @classmethod
    def getById(cls, teamId):
        return (DBSession.query(cls)
            .filter(cls.id == teamId)
            .first())

    def getFullName(self):
        return "{0} {1}".format(self.city, self.name)

    def __repr__(self):
        s = (u"<Team(id={0},name={1},abbreviation={2},code={3},city={4})>")
        return s.format(self.id, self.name, self.abbreviation,
            self.code, self.city)

