import unittest
import transaction
from sqlalchemy import desc

import logging
logging.basicConfig(filename='unittest.output.log', level=logging.DEBUG)
logging.getLogger('sqlalchemy.engine').setLevel(logging.INFO)


def _initTestingDB():
    from hackahoop_models.meta import DBSession
    from sqlalchemy import engine_from_config
    from paste.deploy.loadwsgi import appconfig
    import os
    cwd = os.path.dirname(__file__)
    testCfg = os.path.join(cwd, '../../', 'test.ini')

    # appconfig returns configuration that would be used as a dictionary
    settings = appconfig('config:{0}'.format(testCfg))
    engine = engine_from_config(settings, prefix='sqlalchemy.')
    DBSession.configure(bind=engine)
    return DBSession


class CareerStatsTests(unittest.TestCase):
    def setUp(self):
        self.session = _initTestingDB()

    def tearDown(self):
        self.session.remove()

    def test_foo(self):
        from hackahoop_models.careerstats import CareerStats
        rows = self.session.query(CareerStats).filter(
            CareerStats.playerId == 977).all()

        self.assertTrue(len(rows) != 0)

    def multiple_join(self):
        from hackahoop_models.careerstats import CombinedCareerStats
        from sqlalchemy.orm import contains_eager, eagerload_all
        from hackahoop_models.playerinfo import PlayerInfo
        rows = (self.session.query(CombinedCareerStats)
            .options(contains_eager(CombinedCareerStats.player, PlayerInfo.team))
            .join(CombinedCareerStats.player)
            .join(PlayerInfo.team)
            .filter(PlayerInfo.rosterStatus == 1)
            .all())
    
class PlayerInfoTests(unittest.TestCase):
    def setUp(self):
       self.session = _initTestingDB()

    def tearDown(self):
       self.session.remove()

    def test_foo(self):
       from hackahoop_models.playerinfo import PlayerInfo
       rows = self.session.query(PlayerInfo).filter(
           PlayerInfo.playerId == 977).all()
       self.assertTrue(len(rows) == 1)

    def test_bar(self):
       from hackahoop_models.playerinfo import PlayerInfo
       info = PlayerInfo.getById(977)
       self.assertTrue(info)


    def test_depth_chart(self):
       from hackahoop_models.playerinfo import PlayerInfo
       player = self.session.query(PlayerInfo).filter(
           PlayerInfo.playerId == 201951).first()

       from hackahoop_models.depthchart import DepthChart
       from sqlalchemy.orm import contains_eager
       from sqlalchemy import asc

       rows = (self.session.query(DepthChart)
           .join(DepthChart.player)
           .options(contains_eager(DepthChart.player))
           .filter(DepthChart.teamId == player.teamId)
           .filter(DepthChart.position == player.position)
           .order_by(asc(DepthChart.depth))
           .all())

       self.assertTrue(rows)
   

class GameLogTests(unittest.TestCase):
    def setUp(self):
       self.session = _initTestingDB()

    def tearDown(self):
       self.session.remove()

    def test_foo(self):
       from hackahoop_models.gamelog import GameLog, GameLogJsonEncoder
       rows = self.session.query(GameLog).filter(
           GameLog.playerId == 977).first()
       
    def test_json(self):
       from hackahoop_models.gamelog import GameLog, GameLogJsonEncoder
       from sqlalchemy import desc
       import json
       rows = self.session.query(GameLog).filter(
           GameLog.playerId == 977).first()
       s = json.dumps(rows, cls=GameLogJsonEncoder)
       self.assertTrue(s)

class TweetTests(unittest.TestCase):
    def setUp(self):
       self.session = _initTestingDB()

    def tearDown(self):
       self.session.remove()

    def test_foo(self):
       from hackahoop_models.playernews import Tweet
       rows = self.session.query(Tweet).first()
       self.assertTrue(rows)

class PlayerTweetsTest(unittest.TestCase):
    def setUp(self):
       self.session = _initTestingDB()

    def tearDown(self):
       self.session.remove()

    def test_foo(self):
       from hackahoop_models.playernews import PlayerTweet, Tweet
       from sqlalchemy.orm import joinedload, contains_eager
       rows = (self.session.query(PlayerTweet)
           .join(PlayerTweet.tweet)
           .options(contains_eager(PlayerTweet.tweet))
           #.filter(Tweet.authorScreenName == "FBasketballNews")
           .filter(PlayerTweet.playerId == 1897)
           .order_by(desc(Tweet.createdAt))
           .limit(12)
           .all())
       self.assertTrue(rows)

    def test_rtime(self):
        from hackahoop_models.playernews import getRelativeTime, getDisplayTime
        from datetime import datetime
        a = datetime(2012, 8, 2, 15, 56, 20)
        b = datetime(2012, 8, 2, 15, 56, 50)
        self.assertEquals("30 seconds ago", getDisplayTime(a, curDT=b))

        b = datetime(2012, 8, 3, 15, 56, 20)
        self.assertEquals("Yesterday", getDisplayTime(a, curDT=b))

        b = datetime(2012, 12, 3, 15, 56, 20)
        self.assertEquals("Aug 02", getDisplayTime(a, curDT=b))

        b = datetime(2013, 7, 1, 15, 56, 20)
        self.assertEquals("Aug 02, 2012", getDisplayTime(a, curDT=b))

        b = datetime(2014, 7, 1, 15, 56, 20)
        self.assertEquals("Aug 02, 2012", getDisplayTime(a, curDT=b))

    def test_newsfeed(self):
        from hackahoop_models.playernews import getNewsFeed
        rows = getNewsFeed(31)
        print len(rows)

class DepthChartTest(unittest.TestCase):
    def setUp(self):
       self.session = _initTestingDB()

    def tearDown(self):
       self.session.remove()

    def test_foo(self):
       from hackahoop_models.depthchart import DepthChart
       rows = (self.session.query(DepthChart)
           .filter(DepthChart.teamId == 1610612751)
           .all())
       self.assertTrue(rows)
       self.assertTrue(rows[0].player)


class GameScheduleTest(unittest.TestCase):
    def setUp(self):
        self.session = _initTestingDB()

    def tearDown(self):
        self.session.remove()

    def test_foo(self):
        from hackahoop_models.game_schedule import GameSchedule
        from hackahoop_models.team import Team
        from datetime import datetime
        start = datetime(2013, 10, 29)
        end = datetime(2013, 11, 29)
        games = GameSchedule.getByDateRange(start, end, teamId=1610612751)
        self.assertTrue(games)
               

class TeamTest(unittest.TestCase):
    def setUp(self):
       self.session = _initTestingDB()

    def tearDown(self):
       self.session.remove()

    def test_foo(self):
        from hackahoop_models.team import Team
        row = (self.session.query(Team)
            .filter(Team.name == "Raptors")
            .first())
        self.assertTrue(row)


class SimilarPlayerTest(unittest.TestCase):
    def setUp(self):
       self.session = _initTestingDB()

    def tearDown(self):
       self.session.remove()

    def test_foo(self):
        from hackahoop_models.similar_player import SimilarPlayer
        rows = SimilarPlayer.getByPlayerId(977)

        self.assertTrue(rows)


class YoutubeVideoTest(unittest.TestCase):
    def setUp(self):
       self.session = _initTestingDB()

    def tearDown(self):
       self.session.remove()

    def test_foo(self):
        from hackahoop_models.youtube_video import YoutubeVideo
        rows = YoutubeVideo.getByPlayerId(977)
        self.assertTrue(rows)

class AccountTest(unittest.TestCase):
    def setUp(self):
       self.session = _initTestingDB()

    def tearDown(self):
       self.session.remove()

    def test_auth_id(self):
        from hackahoop_models.account import AuthId

