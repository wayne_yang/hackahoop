from pyramid.view import view_config
from hackahoop_models.playernews import getNewsFeed, FollowRelationship
from hackahoop_models.rate import Rating
import logging
log = logging.getLogger(__name__)

@view_config(route_name='index', renderer='../templates/index.pt')
def index_view(request):
    newsfeed = None
    ratingResult = {}
    if request.user:
        userId = request.user["userId"]
        newsfeed = getNewsFeed(userId)

        following = FollowRelationship.getFollowing(userId, limit=10)
        playerIds = [i.playerId for i in following]

        ratings =  Rating.getByPlayers(playerIds, limit=10)
        ratingResult = {
            "ratings": ratings,
            "showPlayer": True,
            "showAuthor": True,
            "prevLink": False,
            "nextLink": False,
            "paginate": False
        }

    return { "tweets": newsfeed, "ratingResult": ratingResult, "editPerm": False }
