FullStatCategory = [
    {
        "name": "min",
        "displayName": "Minutes",
        "shortName": "MIN",
    },
    {
        "name": "pts",
        "displayName": "Points",
        "shortName": "PTS",
    },
    {
        "name": "reb",
        "displayName": "Rebounds",
        "shortName": "REB",
    },
    {
        "name": "ast",
        "displayName": "Assists",
        "shortName": "AST",
    },
    {
        "name": "stl",
        "displayName": "Steals",
        "shortName": "ST",
    },
    {
        "name": "blk",
        "displayName": "Blocks",
        "shortName": "BLK",
    },
    {
        "name": "fgm",
        "displayName": "Field Goals Made",
        "shortName": "FGM",
    },
    {
        "name": "fga",
        "displayName": "Field Goals Attempted",
        "shortName": "FGA",
    },
    {
        "name": "fgPCT",
        "displayName": "Field Goals %",
        "shortName": "FG%",
    },
    {
        "name": "ftm",
        "displayName": "Free Throws Made",
        "shortName": "FTM",
    },
    {
        "name": "fta",
        "displayName": "Free Throws Attempted",
        "shortName": "FTA",
    },
    {
        "name": "ftPCT",
        "displayName": "Free Throws %",
        "shortName": "FT%",
    },
    {
        "name": "fg3m",
        "displayName": "Three Pointers Made",
        "shortName": "FG3M",
    },
    {
        "name": "fg3a",
        "displayName": "Three Pointers Attempted",
        "shortName": "FG3A",
    },
    {
        "name": "fg3PCT",
        "displayName": "Three Pointers %",
        "shortName": "FG3%",
        
    },
    {
        "name": "tov",
        "displayName": "Turnovers",
        "shortName": "TO",
    },
]

RotoSubsetStat = set([ "pts", "reb", "ast", "stl", "blk", "fgPCT", "ftPCT", "fg3m", "tov" ])

RotoStatCategory = [i for i in FullStatCategory if i["name"] in RotoSubsetStat ]


DataSeriesColor = [
    # green
    '#8bbc21',

    # red
    '#910000',

    # teal
    '#1aadce',

    # orange
    '#f28f43',

    # purple
    '#492970',
]

StatSliderConfig = [
    {
        "id": "gp",
        "displayName": "GP",
        "min": 0,
        "max": 82,
        "step": 1,
    },
    {
        "id": "min",
        "displayName": "MPG",
        "min": 0,
        "max": 48,
        "step": 1,
    },
    {
        "id": "pts",
        "displayName": "PTS",
        "min": 0,
        "max": 30,
        "step": 0.1,
    },
    {
        "id": "reb",
        "displayName": "REB",
        "min": 0,
        "max": 15,
        "step": 0.1,
    },
    {
        "id": "ast",
        "displayName": "AST",
        "min": 0,
        "max": 15,
        "step": 0.1,
    },

    {
        "id": "fgPCT",
        "displayName": "FG%",
        "min": 0,
        "max": 1,
        "step": 0.01,
    },
    {
        "id": "ftPCT",
        "displayName": "FT%",
        "min": 0,
        "max": 1,
        "step": 0.01,
    },
    {
        "id": "fg3m",
        "displayName": "3PTM",
        "min": 0,
        "max": 5,
        "step": 0.1,
    },
    {
        "id": "stl",
        "displayName": "STL",
        "min": 0,
        "max": 5,
        "step": 0.1,
    },
    {
        "id": "blk",
        "displayName": "BLK",
        "min": 0,
        "max": 5,
        "step": 0.1,
    },
    {
        "id": "tov",
        "displayName": "TO",
        "min": 0,
        "max": 6,
        "step": 0.1,
    },
]

Position = ["PG", "SG", "SF", "PF", "C"]
