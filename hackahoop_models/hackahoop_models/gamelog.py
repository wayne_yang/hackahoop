from .meta import Base
from sqlalchemy import (
    Column,
    SmallInteger,
    Integer,
    Numeric,
    String,
    Date,
    ForeignKey,
    Boolean)
from sqlalchemy.dialects.mysql import BIT
from sqlalchemy.orm import relationship
from json import JSONEncoder
from utils import ModelJsonEncoder
from .meta import DBSession
from .playerinfo import PlayerInfo
from sqlalchemy import desc
import datetime

class GameLog(Base):
    __tablename__ = 'gamelog'
    REGULAR_SEASON = "R"

    seasonId = Column('SEASON_ID', Integer, autoincrement=False, primary_key=True)
    playerId = Column('PLAYER_ID', Integer, autoincrement=False, primary_key=True)
    gameId = Column('GAME_ID', Integer, autoincrement=True, primary_key=True)
    gameDate = Column('GAME_DATE', Date)
    curTeam = Column('CUR_TEAM', String)
    opponent = Column('OPPONENT', String)
    homeGame = Column('HOME_GAME', BIT)
    win = Column('WIN', BIT)
    min = Column('MIN', SmallInteger)
    fgm = Column('FGM', SmallInteger)
    fga = Column('FGA', SmallInteger)
    fgPCT = Column('FG_PCT', Numeric)
    fg3m = Column('FG3M', SmallInteger)
    fg3a = Column('FG3A', SmallInteger)
    fg3PCT = Column('FG3_PCT', Numeric)
    ftm = Column('FTM', SmallInteger)
    fta = Column('FTA', SmallInteger)
    ftPCT = Column('FT_PCT', Numeric)
    oreb = Column('OREB', SmallInteger)
    dreb = Column('DREB', SmallInteger)
    reb = Column('REB', SmallInteger)
    ast = Column('AST', SmallInteger)
    stl = Column('STL', SmallInteger)
    blk = Column('BLK', SmallInteger)
    tov = Column('TOV', SmallInteger)
    pf = Column('PF', SmallInteger)
    pts = Column('PTS', SmallInteger)
    seasonType = Column('SEASON_TYPE', String)

    @classmethod
    def getByPlayerId(cls, playerId, limit=None, sinceDays=7,
                      sinceDT=None, untilDT=None):
        query = DBSession.query(cls).filter(cls.playerId == playerId)

        if sinceDT is not None:
            query = query.filter(cls.gameDate > sinceDT)

        if untilDT is not None:
            query = query.filter(cls.gameDate < untilDT)

        if sinceDays is not None:
            try:
                sinceDays = int(sinceDays)
                if sinceDays > 0:
                    sinceDate = datetime.date.today() - datetime.timedelta(days=sinceDays) 
                    query = query.filter(cls.gameDate >= sinceDate)
            except ValueError: pass

        query = query.order_by(desc(cls.gameDate))
        if limit is not None:
            query = query.limit(limit)

        return query.all()

    def toDict(self):
        return {"seasonId": self.seasonId,
                "playerId": self.playerId,
                "gameId": self.gameId,
                "gameDate": self.gameDate,
                "curTeam": self.curTeam,
                "opponent": self.opponent,
                "homeGame": self.homeGame,
                "win": self.win,
                "min": self.min,
                "fgm": self.fgm,
                "fga": self.fga, 
                "fgPCT":  self.fgPCT,
                "fg3m": self.fg3m,
                "fg3a": self.fg3a,
                "fg3PCT": self.fg3PCT,
                "ftm": self.ftm,
                "fta": self.fta,
                "ftPCT": self.ftPCT, 
                "oreb": self.oreb,
                "dreb": self.dreb,
                "reb": self.reb,
                "ast": self.ast,
                "stl": self.stl,
                "blk": self.blk, 
                "tov": self.tov,
                "pf": self.tov,
                "pts": self.pts,
                "seasonType": self.seasonType }

    def __repr__(self):
        s = ("<GameLog(seasonId={0},playerId={1},gameId={2},gameDate={3},"
            "curTeam={4},opponent={5},homeGame={6},win={7},min={8},fgm={9},"
            "fga={10},fgPCT={11},fg3m={12},fg3a={13},fg3PCT={14},ftm={15},"
            "fta={16},ftPCT={17},oreb={18},dreb={19},reb={20},ast={21},stl={22},"
            "blk={23},tov={24},pf={25},pts={26},seasonType={27})")
        return s.format(self.seasonId, self.playerId, self.gameId,
                self.gameDate, self.curTeam, self.opponent, self.homeGame,
                self.win, self.min, self.fgm, self.fga, self.fgPCT, self.fg3m,
                self.fg3a, self.fg3PCT, self.ftm, self.fta, self.ftPCT,
                self.oreb, self.dreb, self.reb, self.ast, self.stl, self.blk,
                self.tov, self.pf, self.pts, self.seasonType)
    
class GameLogJsonEncoder(ModelJsonEncoder):
    def default(self, obj):
        if isinstance(obj, GameLog):
            return obj.toDict()

        return ModelJsonEncoder.default(self, obj)
