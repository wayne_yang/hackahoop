import json
import sys
import argparse
import db_utils
from os import listdir
from os.path import isfile, join, basename



def import_search_history(keywordFile="youtube.keywords"):
    insertSQL = ("INSERT INTO youtube_search_history (player_id, etag, "
        " keyword VALUES (%s, %s, %s)")

    cnx = db_utils.connect(db_utils.dbConfig)
    cursor = cnx.cursor()
    with open(keywordFile) as kf:
        for line in kf.readlines():
            playerId, keyword = line.rstrip().split(",")
            with open("youtube/{0}.json".format(playerId)) as resp:
                data = json.loads(resp.read())
                cursor.execute(insertSQL, (playerId, data["etag"], keyword))
    cnx.commit()
    cursor.close()
    cnx.close()


def import_video(resDir="youtube/"):
    insertSQL = ("INSERT INTO youtube_video (player_id, video_id, rank, etag,"
        "thumbnail_default, thumbnail_high, thumbnail_medium, title"
        ") VALUES (%s, %s, %s, %s, %s, %s, %s, %s)")

    cnx = db_utils.connect(db_utils.dbConfig)
    cursor = cnx.cursor()

    files = [join(resDir, f) for f in listdir(resDir) if isfile(join(resDir, f))] 
    for f in files:
        bn = basename(f)
        if not bn.endswith(".json"):
            continue

        playerId, fileExt = bn.split(".")    

        with open(f) as ff:
            data = json.loads(ff.read())
            for i, video in enumerate(data["items"]):
                snippet = video["snippet"]
                thumbnails = snippet["thumbnails"]
                record = (playerId, video["id"]["videoId"], i, video["etag"],
                    thumbnails["default"]["url"], thumbnails["high"]["url"],
                    thumbnails["medium"]["url"], snippet["title"])
                cursor.execute(insertSQL, record)
    cnx.commit()
    cursor.close()
    cnx.close()

        
    


if __name__ == "__main__":
    #import_search_history()
    import_video()
