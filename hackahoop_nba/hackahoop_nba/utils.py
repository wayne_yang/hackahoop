import re
from hackahoop_models.playerinfo import PlayerInfo
import ConfigParser
from sqlalchemy import engine_from_config 
from sqlalchemy.orm import sessionmaker
from hackahoop_models.meta import Base
import datetime

import logging

SuffixPattern = re.compile("(II|III|Jr.)")


def convertToPlayerInfo(playerData):
    lastName = playerData["LAST_NAME"]
    parsedLastName = parseHumanName(lastName)
    suffix = parsedLastName["suffix"]
    if suffix is not None:
        lastName = parsedLastName["name"]

    birthDate = datetime.datetime.strptime(playerData["BIRTHDATE"], "%Y-%m-%dT%H:%M:%S")
    playerinfo = PlayerInfo(
        playerId=playerData["PERSON_ID"],
        firstName=playerData["FIRST_NAME"],
        lastName=lastName,
        seoName=getSEOName(playerData["FIRST_NAME"], lastName, suffix),
        suffix=suffix,
        fullName=playerData["DISPLAY_FIRST_LAST"],
        displayFirstLast=playerData["DISPLAY_FI_LAST"],
        birthDate=birthDate,
        school=playerData["SCHOOL"],
        country=playerData["COUNTRY"],
        lastAffiliation=playerData["LAST_AFFILIATION"],
        jersey=playerData["JERSEY"],
        # TODO: need to convert to fantasy positions
        position=playerData["POSITION"],
        rosterStatus=1,
        teamId=playerData["TEAM_ID"],)
    return playerinfo


def parseMinutes(minStat):
    """
    minStat is in the format MM:SS
    """
    minutes, seconds = minStat.split(":")
    try:
        minutes = int(minutes)
        seconds = int(seconds)
    except ValueError:
        raise ParseError("Failed to parse stat minutes " + minStat)

    if seconds > 0:
        minutes = minutes + 1
    return minutes


def convertResultSetToDict(resultSet):
    result = []
    headers = resultSet["headers"]
    for row in resultSet["rowSet"]:
        entry = {}
        for i in range(len(headers)):
            entry[headers[i]] = row[i]

        result.append(entry)
    return result
        

def getSeasonFromDate(dt):
    if dt.month >= 10 and dt.month <= 12:
        return dt.year
    return dt.year - 1

def getSeasonIdForLastXDay(seasonId, x):
    if x <= 9:
        return seasonId * 10 + x
    elif x > 9:
        return seasonId * 100 + x
    else:
        raise RunTimeError()
        

def initDB(settings):
    Session = sessionmaker()
    engine = engine_from_config(settings, prefix='sqlalchemy.')
    Session.configure(bind=engine)
    Base.metadata.bind = engine
    return Session

def initDBFromFile(cfgFile):
    parser = ConfigIniParser()
    parser.read(cfgFile)
    settings = parser.as_dict()
    return initDB(settings["app:main"])

def getConfigSettings(cfgFile):
    parser = ConfigIniParser()
    parser.read(cfgFile)
    return parser.as_dict()

class ConfigIniParser(ConfigParser.ConfigParser):
    def as_dict(self):
        d = dict(self._sections)
        for k in d:
            d[k] = dict(self._defaults, **d[k])
            d[k].pop('__name__', None)
        return d

def getLogger(logLevel=logging.DEBUG):
    logger = logging.getLogger(__name__)
    logger.setLevel(logLevel)
    logger.propagate = False

    if not logger.handlers:
        # create formatter
        formatter = logging.Formatter('%(asctime)s %(module)s %(funcName)s %(levelname)s - %(message)s')
        ch = logging.StreamHandler()
        ch.setFormatter(formatter)
        logger.addHandler(ch)
        
    return logger


def getSEOName(firstName, lastName, suffix, school=None):
    components = [firstName, lastName]
    if suffix is not None:
        components.append(suffix)
    if school is not None:
        components.extend(school.split(" "))
    return "-".join([i.replace(".", "") for i in components]).lower()



def parseHumanName(fullName):
    """
    Use Simple Heuristic to parse the suffix from the name.  Return name and suffix in a dict
    """
    parts = fullName.split(" ")
    suffix = None
    name = fullName

    if SuffixPattern.match(parts[-1]):
        suffix = parts[-1]
        name = " ".join(parts[:-1])

    return {"name": name, "suffix": suffix}

def findActivePlayer(firstName, lastName, teamId):
    """
    Wrapper over PlayerInfo with additional fallbacks when no result
    TODO:  should probably consider better player identification/matching mechanism
    """
    nameParse = parseHumanName(lastName)
    suffix = nameParse["suffix"]
    if suffix is not None:
        lastName = nameParse["name"]

    # fetch playerinfo to get id
    playerinfo = PlayerInfo.getByName(
        firstName,
        lastName,
        suffix=suffix,
        teamId=teamId,
        rosterStatus=1)


    # HACK
    if playerinfo is None:
        if firstName == u"Timothy":
            firstName = u"Tim"
            playerinfo = PlayerInfo.getByName(
                firstName, lastName, suffix=suffix, teamId=teamId,
                rosterStatus=1)
        elif len(firstName) == 2:
            firstName = firstName[0] + "." + firstName[1] + "."
            playerinfo = PlayerInfo.getByName(
                firstName, lastName, suffix=suffix, teamId=teamId,
                rosterStatus=1)

    return playerinfo 

class BoxScoreFetcherConfig(object):

    def __init__(self, settings):
        self._maxRetries = 3
        self._hourThreshold = 4
        try:
            settings = settings["boxscore_fetcher"]
            self._maxRetries = int(settings["max_retries"])
            self._hourThreshold = int(settings["hour_threshold"])
        except KeyError as e:
            print e
        except ValueError as e:
            print e
    
    def getMaxRetries(self):
        return self._maxRetries
    
    def getHourThreshold(self):
        return self._hourThreshold

    def __repr__(self):
        s = "BoxScoreFetcherConfig(maxRetries={maxRetries}, hourThreshold={hourThreshold})"
        return s.format(
            maxRetries=self._maxRetries,
            hourThreshold=self._hourThreshold)

if __name__ == "__main__":
    #print parseHumanName("Jucar Hello III")

    print getConfigSettings("database.ini")
