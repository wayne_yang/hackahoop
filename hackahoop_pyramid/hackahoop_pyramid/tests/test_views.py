import unittest
from pyramid import testing
import pyramid.request
import logging

logging.basicConfig(filename='unittest.output.log', level=logging.DEBUG)
logging.getLogger('sqlalchemy.engine').setLevel(logging.INFO)

# TODO:  share this def with other tests
def _initTestingDB():
    from hackahoop_models.meta import DBSession
    from sqlalchemy import engine_from_config
    from paste.deploy.loadwsgi import appconfig
    import os
    cwd = os.path.dirname(__file__)
    testCfg = os.path.join(cwd, '../../', 'test.ini')

    # appconfig returns configuration that would be used as a dictionary
    settings = appconfig('config:{0}'.format(testCfg))
    engine = engine_from_config(settings, prefix='sqlalchemy.')
    DBSession.configure(bind=engine)
    return DBSession


class TestCareerStatsView(unittest.TestCase):
    def setUp(self):
        """
        Set Pyramid registry and request thread locals for the duration 
        of a single unit test.
        """
        self.config = testing.setUp()
        self.session = _initTestingDB()
        
    def tearDown(self):
        self.session.remove()
        testing.tearDown()

    def test_param_parse(self):
        from ..views.careerstats_api import getParamAsTuple
        request = pyramid.request.Request.blank('/v1/nba/careerstats?pts=20,30')
        pair = getParamAsTuple(request.GET, 'pts') 
        self.assertEqual(pair[0], 20.0)
        self.assertEqual(pair[1], 30.0)
        

    def test_it(self):
        from ..views.careerstats_api import my_view
        request = pyramid.request.Request.blank('/v1/nba/careerstats?playerId=977&pts=30,40&reb=1.0,5.9&format=json_obj')
        info = my_view(request)
        self.assertTrue(info)

    """
    def test_first_page(self):
        from ..views.careerstats_api import my_view
        request = pyramid.request.Request.blank('/v1/nba/careerstats?pts=14,15&reb=4.0,5.9')
        info = my_view(request)
        self.assertTrue("nextUri" in info and info["nextUri"].startswith("pts=14,15&reb=4.0,5.9"))
        self.assertTrue("prevUri" not in info)

    def test_middle_page(self):
        from ..views.careerstats_api import my_view
        request = pyramid.request.Request.blank('/v1/nba/careerstats?pts=14,15&reb=5.0,5.9&start=40')
        info = my_view(request)
        self.assertTrue("prevUri" in info and info["prevUri"].startswith("pts=14,15&reb=5.0,5.9"))
        self.assertTrue("nextUri" in info and info["nextUri"].startswith("pts=14,15&reb=5.0,5.9"))


    def test_last_page(self):
        from ..views.careerstats_api import my_view
        request = pyramid.request.Request.blank('/v1/nba/careerstats?pts=14,15&reb=5.0,5.9&start=60')
        info = my_view(request)
        print info
        #self.assertTrue("nextUri" not in info)
    """

class TestPlayerSearchView(unittest.TestCase):
    def setUp(self):
        self.config = testing.setUp()
        self.session = _initTestingDB()
        
    def tearDown(self):
        self.session.remove()
        testing.tearDown()

    def test_it(self):
        from ..views.playersearch import playersearch_json_view
        request = pyramid.request.Request.blank('/v1/nba/playersearch?term=Ja')
        info = playersearch_json_view(request)



class TestGameLogApiView(unittest.TestCase):
    def setUp(self):
        self.config = testing.setUp()
        self.session = _initTestingDB()
        
    def tearDown(self):
        self.session.remove()
        testing.tearDown()

    def test_it(self):
        from ..views.gamelog_api import gamelog_api_view
        request = pyramid.request.Request.blank('/v1/nba/gamelog?playerId=202704&limit=3')
        info = gamelog_api_view(request)
        self.assertTrue('gamelog' in info)

        dates = [i["gameDate"] for i in info["gamelog"]]
        prevDate = None
        for d in dates:
            if prevDate is None:
                prevDate = d
                continue
            self.assertTrue(d > prevDate) 
            prevDate = d

    def test_get_gamelog(self):
        from ..views.gamelog_api import gamelog_api_view
        request = pyramid.request.Request.blank('/v1/nba/gamelog?playerId=977&limit=3')
        data = gamelog_api_view(request)
        dates = [i["gameDate"] for i in data["gamelog"]]
        prevDate = None
        for d in dates:
            if prevDate is None:
                prevDate = d
                continue
            self.assertTrue(d > prevDate) 
            prevDate = d
        


class TestSearchView(unittest.TestCase):
    def setUp(self):
        self.config = testing.setUp()
        self.session = _initTestingDB()
        
    def tearDown(self):
        self.session.remove()
        testing.tearDown()

    def test_it(self):
        from ..views.search import search_view
        request = pyramid.request.Request.blank('/search?q=carmelo')
        info = search_view(request)
        self.assertTrue('data' in info)
        self.assertTrue(len(info['data']) == 1)

    def test_notfound(self):
        from ..views.search import search_view
        request = pyramid.request.Request.blank('/search?q=kljlajsdf')
        info = search_view(request)
        self.assertTrue('data' in info)
        self.assertTrue(len(info['data']) == 0)


class TestPlayerNewsView(unittest.TestCase):
    def setUp(self):
        self.config = testing.setUp()
        self.session = _initTestingDB()

    def tearDown(self):
        testing.tearDown()
        self.session.remove()

    def test_missing_params(self):
        from ..views.playernews import playernews_json_view
        #request = pyramid.request.Request.blank('/v1/nba/playernews')
        request = testing.DummyRequest()
        info = playernews_json_view(request)
        self.assertTrue("error" in info)

    def test_invalid_since(self):
        from ..views.playernews import playernews_json_view
        request = testing.DummyRequest(params={"playerId": 977, "since": "abc" })
        info = playernews_json_view(request)
        self.assertTrue("error" in info)

    def test_foo(self):
        from ..views.playernews import playernews_json_view
        request = testing.DummyRequest(params={"playerId": 977, "since": "2013-07-30 00:00:00", "limit": 5})
        info = playernews_json_view(request)
        self.assertTrue(info)


class TestSinglePlayer(unittest.TestCase):
    def setUp(self):
        self.config = testing.setUp()
        self.session = _initTestingDB()

    def tearDown(self):
        testing.tearDown()
        self.session.remove()

    def test_print_followers(self):
        from ..views.single_player import getPrettyPrintFollowers
        
        a = [{"name": "Wayne"}]
        self.assertEquals("Wayne", getPrettyPrintFollowers(a))

        a = [{"name": "Wayne"}, {"name": "Harry"}]
        self.assertEquals("Wayne and Harry", getPrettyPrintFollowers(a))

        a = [{"name": "Wayne"}, {"name": "Harry"}, {"name": "Etta"}]
        self.assertEquals("Wayne, Harry, and Etta", getPrettyPrintFollowers(a))




