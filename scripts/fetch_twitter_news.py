import argparse
import sys
import db_utils
import twitter_api
import json
import player_detection
from import_tweets import format_tweet_time, import_tweets, import_player_tweet

def getCrawlStats(source, id, conn=None):
    if conn is None:
        conn = db_utils.connect(db_utils.dbConfig)

    cursor = conn.cursor()
    cursor.execute("SELECT * from crawl_stats WHERE source = %s and id = %s",
        (source, id))
    rows = cursor.fetchall()
    if not rows:
        return ()
    else:
        return rows[0]

def upsertCrawlStat(recordDict, conn=None):
    if conn is None:
        conn = db_utils.connect(db_utils.dbConfig)
    sql = ("INSERT INTO `crawl_stats` (source, id, last_post_id, "
        "last_post_time, post_count, player_post_count) VALUES (%(source)s,"
        "%(id)s, %(lastPostId)s, %(lastPostTime)s, %(postCount)s,"
        "%(playerPostCount)s) ON DUPLICATE KEY UPDATE post_count = %(postCount)s,"
        "player_post_count = %(playerPostCount)s, last_post_id = %(lastPostId)s,"
        "last_post_time = %(lastPostTime)s")
    cursor = conn.cursor()
    cursor.execute(sql, recordDict)

parser = argparse.ArgumentParser(description='Fetch twitter timeline data')
parser.add_argument("screenName", nargs="+", help="a twitter screen name")
args = parser.parse_args()

conn = db_utils.connect(db_utils.dbConfig)

screenNames = args.screenName

for sn in screenNames:
    stats = getCrawlStats("twitter", sn, conn=conn)
    if not stats:
        stats = ("twitter", sn, None, None, 0, 0)
    
    source, sId, lastPostId, lastPostTime, postCount, playerCount = stats

    params = { "screen_name": sn,
        "include_rts": "true",
        "count": 200, }

    if lastPostId is not None:
        params["since_id"] = lastPostId
        
    data = twitter_api.fetch_all_user_timeline(params)

    # no data or no new data since last crawl
    if len(data) == 0:
        continue

    lastPostId = data[0]["id_str"]
    lastPostTime = format_tweet_time(data[0]["created_at"])

    #print "lastPostId={0}".format(lastPostId)
    #print "lastPostTime={0}".format(lastPostTime)

    for tweet in data:
        postCount = postCount + 1

        compareString = player_detection.get_compare_string(tweet)
        detectResult = player_detection.detect_player(compareString)
        if not detectResult:
            continue
        
        playerCount = playerCount + 1
        import_tweets([ tweet ], conn=conn)

        for result in detectResult:
            import_player_tweet(result["id"], tweet["id_str"], conn=conn)

    # update crawl stats 
    upsertCrawlStat({
        "source": source,
        "id": sId,
        "lastPostId": lastPostId,
        "lastPostTime": lastPostTime,
        "postCount": postCount,
        "playerPostCount": playerCount}
        ,conn=conn)
conn.commit()
conn.close()


"""
data = twitter_api.fetch_all_user_timeline({
    "screen_name": "NBA",
    "include_rts": "true",
    "count": 200,
}) 
"""


