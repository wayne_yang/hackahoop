import db_utils
from teams import TeamInfo


def getAllActivePlayers(fields):
    colList = ",".join(fields)
    sql = "SELECT {0} from playerinfo WHERE rosterstatus = 1".format(colList)
    conn = db_utils.connect(db_utils.dbConfig)
    cursor = conn.cursor()
    cursor.execute(sql)
    result = cursor.fetchall()
    conn.close() 
    return result

def getPlayerInfo(playerIDs, conn=None):
    placeHolders = []
    for i in range(len(playerIDs)):
        placeHolders.append("%s")

    sql = "SELECT * from playerinfo where player_id in ({0})".format(",".join(placeHolders))
    if conn is None:
        conn = db_utils.connect(db_utils.dbConfig)
    cursor = conn.cursor()
    cursor.execute(sql, playerIDs)
    rows = cursor.fetchall()

    result = {}
    for row in rows:
        result[row[0]] = row

    return result

def updatePlayer(playerId, infoDict):
    if not playerId:
        return
    if len(infoDict) == 0:
        return

    conn = db_utils.connect(db_utils.dbConfig)
    cursor = conn.cursor()

    columnList = ",".join(["{0}=%s".format(k) for k in infoDict.keys()])
    sql = "UPDATE playerinfo SET {0} WHERE PLAYER_ID=%s".format(columnList)
    values = infoDict.values()
    values.append(playerId)
    cursor.execute(sql, values)
    conn.commit()
    cursor.close()
    conn.close()


def findPlayer(fullName, teamId=None, suffix=None, conn=None, onlyActiveStatus=True):
    sql = "SELECT a.*, b.name as TEAM_NAME, b.abbreviation as TEAM_ABBREVIATION, b.code as TEAM_CODE, b.city as TEAM_CITY from playerinfo as a INNER JOIN team as b on a.team_id = b.id where a.full_name = %s"

    if conn is None:
        conn = db_utils.connect(db_utils.dbConfig)
    cursor = conn.cursor()

    record = [fullName,]
    if onlyActiveStatus:
        sql = "{0} AND a.rosterstatus = 1".format(sql)

    if suffix:
        sql = "{0} AND a.suffix = %s".format(sql)
        record.append(suffix)

    if teamId is not None:
        sql = "{0} AND a.TEAM_ID = %s".format(sql)
        record.append(teamId)
        
    cursor.execute(sql, record)
    return cursor.fetchall()

def addPlayer(firstName, lastName, suffix, position, teamId, conn=None):
    insertPlayerSQL = ("INSERT INTO nba_stats.playerinfo (FIRST_NAME,"
            "LAST_NAME,FULL_NAME,DISPLAY_FIRST_LAST,POSITION,ROSTERSTATUS,TEAM_ID"
            " VALUES (%s,%s,%s,%s,%s,%s,%s)")

    if conn is None:
        conn = db_utils.connect(db_utils.dbConfig)
    cursor = conn.cursor()
    firstLastName = "{0}. {1}".format(firstName[0], lastName)
    fullName = "{0} {1}".format(firstName, lastName)
    record = (firstName, lastName, fullName, firstLastName, position, 1, teamId)
    cursor.execute(insertPlayerSQL, record)
    conn.commit()

def updatePlayerPosition(playerId, position, conn=None):
    sql = "UPDATE playerinfo SET position=%s WHERE player_id=%s"
    if conn is None:
        conn = db_utils.connect(db_utils.dbConfig)
    cursor = conn.cursor()
    record = (position, playerId)
    cursor.execute(sql, record)
    conn.commit()
