from pyramid.view import view_config
from pyramid.renderers import render
from datetime import datetime
from hackahoop_models.game_schedule import GameSchedule
from utils import getBadRequestError
import logging
log = logging.getLogger(__name__)

@view_config(route_name='game_schedule', renderer='json')
def game_schedule_json_view(request):
    GET = request.GET
    start = GET["start"]
    end = GET["end"]
    teamId = GET["teamId"]
    try:
        teamId = long(teamId)
    except ValueError:
        return getBadRequestError("Invalid value for teamId param")

    if start is None or end is None:
        return getBadRequestError("start or end param is missing") 
    if teamId is None:
        return getBadRequestError("teamId param is missing")
    
    try:
        # TODO: revisit timezone
        start = datetime.fromtimestamp(int(start))
        end = datetime.fromtimestamp(int(end))
    except ValueError:
        return getBadRequestError("Invalid value for start or end param")

    games = GameSchedule.getByDateRange(start, end, teamId)

    result = []

    for game in games:
        data = (game.homeTeam.abbreviation, game.awayTeam.abbreviation)

        if game.homeTeam.id == teamId:
            title = "{0} vs {1}".format(*data)
        else:
            title = "{0} @ {1}".format(*data)

        result.append({
            "title": title,
            "start": game.gameDate.strftime("%Y-%m-%d")
        })

    #print result
    return result

