from .meta import Base
from .playerinfo import PlayerInfo
from .careerstats import CombinedCareerStats
from .meta import DBSession
from sqlalchemy import Column, Integer, String, Time, Date, ForeignKey, SmallInteger
from sqlalchemy.orm import relationship, contains_eager, aliased
from sqlalchemy import asc

class SimilarPlayer(Base):
    __tablename__ = "similar_player"
    playerId = Column("player_id", Integer, primary_key=True)
    similarPlayerId = Column("similar_player_id", Integer, ForeignKey("playerinfo.PLAYER_ID"), primary_key=True)
    score = Column("score", SmallInteger)

    similarPlayer = relationship("PlayerInfo")

    def __repr__(self):
        s = ("<SimilarPlayer(playerId={0},similarPlayerId={1},score={2})")
        return s.format(self.playerId, self.similarPlayerId, self.score)


    @classmethod
    def getByPlayerId(cls, playerId, limit=None, statsSeason=False):
        stats = aliased(CombinedCareerStats, name="stats")
        playerinfo = aliased(PlayerInfo, name="playerInfo")

        entities = [cls, playerinfo]
        if statsSeason:
            entities.append(stats)

        query = (DBSession.query(*entities)
            .select_from(cls)
            .join(playerinfo, cls.similarPlayerId == playerinfo.playerId))

        if statsSeason:
            query = (query.join(stats, cls.similarPlayerId == stats.playerId)
                .filter(stats.seasonId == statsSeason))

        query = (query.filter(cls.playerId == playerId)
            .order_by(asc(cls.score)))

        if limit is not None:
            return query.limit(limit).all()
        else:
            return query.all()
