from .meta import Base
from sqlalchemy import (
    Column,
    DateTime,
    Integer,
    SmallInteger,
    String,
    Text,
    Unicode)
from sqlalchemy import asc
from .meta import DBSession

class YoutubeVideo(Base):
    __tablename__ = "youtube_video"
    playerId = Column("player_id", Integer, primary_key=True)
    videoId = Column("video_id", Unicode, primary_key=True)
    rank = Column("rank", SmallInteger)
    thumbnailDefault = Column("thumbnail_default", String)
    title = Column("title", Unicode)

    @classmethod
    def getByPlayerId(cls, playerId, limit=10):
        return (DBSession.query(cls)
            .filter(cls.playerId == playerId)
            .order_by(asc(cls.rank))
            .limit(limit).all())


    def __repr__(self):
        s = (u"<YoutubeVideo(playerId={0},videoId={1},rank={2},"
            "thumbnailDefault={3},title={4})>")
        return s.format(self.playerId, self.videoId, self.rank,
            self.thumbnailDefault, self.title)



    
