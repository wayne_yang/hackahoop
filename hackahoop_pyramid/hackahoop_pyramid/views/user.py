from pyramid.view import view_config
from pyramid.response import Response

from hackahoop_models.account import AuthId
from hackahoop_models.playernews import FollowRelationship
from hackahoop_models.rate import Rating

import utils

from datetime import datetime
import logging
log = logging.getLogger(__name__)

@view_config(route_name='user', renderer='../templates/user.pt')
def user_view(request):
    if "userId" not in request.matchdict:
        return Response("userId is missing", content_type='text/html', status_int=400)

    # userId is the authId
    userId = None 
    try:
        userId = int(request.matchdict["userId"])
    except ValueError:
        return Response("Invalid user id", content_type='text/html', status_int=400)

    authId = AuthId.getById(userId) 

    # add pagination
    following = FollowRelationship.getFollowing(userId, limit=None)

    ratingCount = Rating.getCountByAuthor(userId) 
    
    editPerm = False
    if request.user != None and authId.id == request.user["userId"]:
        editPerm = True
    log.debug("editPerm={0}".format(editPerm))

    return {
        "authId": authId,
        "user": authId.users[0],
        "following": following,
        "editPerm": editPerm,
        "followingCount": len(following),
        "ratingCount": ratingCount
    }

@view_config(route_name='user_posts', renderer='../templates/user_posts.pt')
def user_posts_view(request):
    if "userId" not in request.matchdict:
        return Response("userId is missing", content_type='text/html', status_int=400)

    # userId is the authId
    userId = None 
    try:
        userId = int(request.matchdict["userId"])
    except ValueError:
        return Response("Invalid user id", content_type='text/html', status_int=400)

    authId = AuthId.getById(userId) 

    followingCount = FollowRelationship.getFollowingCount(userId)

    limit = 10
    ratingRes = utils.getRatingsByAuthor(request, userId, limit)

    ratingCount = Rating.getCountByAuthor(userId) 

    editPerm = False
    if request.user != None and authId.id == request.user["userId"]:
        log.debug("authId.id={0} user['userId']={1}".format(authId.id, request.user["userId"]))
        editPerm = True
    log.debug("editPerm={0}".format(editPerm))

    return {
        "authId": authId,
        "user": authId.users[0],
        "followingCount": followingCount,
        "ratingResult": ratingRes,
        "ratingCount": ratingCount,
        "editPerm": editPerm,
    }
