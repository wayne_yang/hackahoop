from .meta import Base, DBSession
from .team import Team
from sqlalchemy.orm import relationship
from sqlalchemy import (
    Column, Integer, String, Time, Date, ForeignKey, DateTime, Enum)
from sqlalchemy.orm import joinedload
from sqlalchemy import or_, asc

from datetime import datetime, timedelta

class GameSchedule(Base):
    __tablename__ = "game_schedule"
    gameId = Column("game_id", Integer, primary_key=True)
    homeTeamId = Column("home_team", Integer, ForeignKey("team.id"))
    awayTeamId = Column("away_team", Integer, ForeignKey("team.id"))
    # gameDate and gameTime are in Eastern timezone
    gameDate = Column("game_date", Date)
    gameTime = Column("game_time", Time)
    gameDateUtc = Column("game_date_utc", DateTime)
    fetchState = Column("fetch_state", Enum("DONE", "FAIL"))
    fetchDate = Column("fetch_date", DateTime)
    retries = Column("retries", Integer)

    homeTeam = relationship("Team", foreign_keys=[homeTeamId]) 
    awayTeam = relationship("Team", foreign_keys=[awayTeamId]) 

    DONE_STATE = "DONE"
    FAIL_STATE = "FAIL"

    def __repr__(self):
        s = ("<GameSchedule(gameId={0},homeTeamId={1},awayTeamId={2},gameDate={3},gameTime={4}"
            ",gameDateUtc={5}, fetchState={6}, retries={7})")
        return s.format(
            self.gameId,
            self.homeTeamId,
            self.awayTeamId,
            self.gameDate,
            self.gameTime,
            self.gameDateUtc,
            self.fetchState,
            self.retries)

    @classmethod
    def getUnprocessedGames(cls, hourThreshold=4, maxRetries=3):
        dtThreshold = datetime.utcnow() - timedelta(hours=hourThreshold)
        return (DBSession.query(cls)
            .filter(cls.gameDateUtc < dtThreshold)
            .filter(or_(cls.fetchState == cls.FAIL_STATE, cls.fetchState == None))
            .filter(cls.retries < maxRetries)
            .all())
        
 
    @classmethod
    def getByDateRange(cls, startDT, endDT, teamId=None):
        """
        Return game schedules for teamId based on [start, end) datetime.
        """
        orCond = or_(cls.homeTeamId == teamId, cls.awayTeamId == teamId)
        query = (DBSession.query(cls)
            .options(joinedload(cls.homeTeam, innerjoin=True))
            .options(joinedload(cls.awayTeam, innerjoin=True)))

        if teamId is not None:
            query = query.filter(orCond)

        query = (query.filter(cls.gameDate >= startDT)
            .filter(cls.gameDate < endDT)
            .order_by(asc(cls.gameDate)))

        return query.all()
