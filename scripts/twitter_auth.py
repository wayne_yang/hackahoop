import base64
import urllib, urllib2
import json
import sys


def twitter_app_auth(consumerKey, consumerSecret):
    consumerKey = urllib.quote_plus(consumerKey)
    consumerSecert = urllib.quote_plus(consumerSecret)
    bearToken = base64.b64encode("{0}:{1}".format(consumerKey, consumerSecret))

    url = "https://api.twitter.com/oauth2/token"
    headers = {
        "Authorization" : "Basic {0}".format(bearToken),
        "Content-Type" : "application/x-www-form-urlencoded;charset=UTF-8"
    }

    body = "grant_type=client_credentials"

    # create your HTTP request
    req = urllib2.Request(url, body, headers)
    resp = None
    try:
        resp = urllib2.urlopen(req)
    except urllib2.HTTPError as e:
        sys.stderr.write("Got Error HTTPError: {0}\n".format(e))
        sys.exit()

    content = resp.read()
    data = json.loads(content)
    return data["access_token"]
