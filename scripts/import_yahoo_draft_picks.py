from bs4 import BeautifulSoup
from teams import TeamInfo
import db_utils
import sys

"""
http://sports.yahoo.com/nba/draft?round=1
http://sports.yahoo.com/nba/draft?round=2
"""

conn = db_utils.connect(db_utils.dbConfig)
cursor = conn.cursor()
f = open("/home/wyang/Downloads/yahoo_draft_first_round.html")
soup = BeautifulSoup(f.read())
rows = soup.select("#draft-tracker table tbody tr")

insertSql = ("INSERT INTO nba_stats.playerinfo (FIRST_NAME,"
            "LAST_NAME,DISPLAY_FIRST_LAST,BIRTHDATE,SCHOOL,COUNTRY,"
            "LAST_AFFILIATION,JERSEY,POSITION,ROSTERSTATUS,TEAM_ID,"
            "TEAM_NAME,TEAM_ABBREVIATION,TEAM_CODE,TEAM_CITY) VALUES ("
            "%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)")



for i in [i for i in range(len(rows)) if i % 2 == 0]:
    name = rows[i].find("td", class_="player").text
    pos = rows[i].find("td", class_="position").text
    school = rows[i].find("td", class_="school").text
    teamFullName = rows[i].select("td.team a img")[0].attrs['title']

    if teamFullName in TeamInfo:
        teamName, teamId, teamAbbr, teamCode, teamCity = TeamInfo[teamFullName]
        firstName, lastName = name.split(" ", 1)
        firstLastName = "{0}. {1}".format(firstName[0], lastName)
        birthDate = '0000-00-00'
        country = ''
        last_affiliation = ''
        jersey = 0
        rosterStatus = 1
        record = (firstName, lastName, firstLastName, birthDate, school, 
            country, last_affiliation, jersey, pos, rosterStatus, teamId,
            teamName, teamAbbr, teamCode, teamCity)
        print record
        # cursor.execute(insertSql, record)
       

conn.commit()
cursor.close()
conn.close()

    

