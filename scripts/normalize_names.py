import sys
import db_utils
from player import getAllActivePlayers, updatePlayer



def normalize_name(firstName, lastName, suffix):
    components = [firstName, lastName]
    if suffix is not None:
        components.append(suffix)
    return "-".join([i.replace(".", "") for i in components]).lower()

if __name__ == "__main__":
    fields = ["player_id", "first_name", "last_name", "suffix"]
    players = getAllActivePlayers(fields)

    check = set()
    longName = ""
    for player in players:
        name = normalize_name(player[1], player[2], player[3])
        """
        if name in check:
            print "DUP {0}".format(name)
        """
        check.add(name)
        if len(name) > len(longName):
            longName = name

        updatePlayer(player[0], {"SEO_NAME": name}) 
