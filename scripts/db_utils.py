import mysql.connector
import sys

dbConfig = {
    'user': 'nba_stats',
    'password': 'gooodboy',
    'host': 'localhost',
    'database':'nba_stats'
}


def connect(cfg=dbConfig):
    try:
        cnx = mysql.connector.connect(**cfg)
    except mysql.connector.Error as err:
        print "MySQL Connection error - error number is {0}".format(err.errno)
        sys.exit(1)
    return cnx

