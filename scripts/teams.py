"""
echo "SELECT CONCAT(quote(team_name), ': [', CONCAT_WS(',', quote(team_name), team_id, quote(team_abbreviation), quote(team_code), quote(team_city)), '],') FROM playerinfo group by team_name having team_name <> ''" | mysql --skip-column-names -u root -p nba_stats

"""


Teams = {
    '76ers': ['76ers',1610612755,'PHL','sixers','Philadelphia'],
    'Bobcats': ['Bobcats',1610612766,'CHA','bobcats','Charlotte'],
    'Bombers': ['Bombers',1610610034,'BOM','bombers','St. Louis'],
    'Bucks': ['Bucks',1610612749,'MIL','bucks','Milwaukee'],
    'Bulls': ['Bulls',1610612741,'CHI','bulls','Chicago'],
    'Capitols': ['Capitols',1610610036,'WAS','capitols','Washington'],
    'Cavaliers': ['Cavaliers',1610612739,'CLE','cavaliers','Cleveland'],
    'Celtics': ['Celtics',1610612738,'BOS','celtics','Boston'],
    'Clippers': ['Clippers',1610612746,'LAC','clippers','Los Angeles'],
    'Falcons': ['Falcons',1610610028,'DEF','falcons','Detroit'],
    'Grizzlies': ['Grizzlies',1610612763,'MEM','grizzlies','Memphis'],
    'Hawks': ['Hawks',1610612737,'ATL','hawks','Atlanta'],
    'Heat': ['Heat',1610612748,'MIA','heat','Miami'],
    'Pelicans': ['Pelicans',1610612740,'NOH','pelicans','New Orleans'],
    'Huskies': ['Huskies',1610610035,'HUS','huskies','Toronto'],
    'Ironmen': ['Ironmen',1610610031,'PIT','ironmen','Pittsburgh'],
    'Jazz': ['Jazz',1610612762,'UTA','jazz','Utah'],
    'Jets': ['Jets',1610610029,'JET','jets','Indianapolis'],
    'Kings': ['Kings',1610612758,'SAC','kings','Sacramento'],
    'Knicks': ['Knicks',1610612752,'NYK','knicks','New York'],
    'Lakers': ['Lakers',1610612747,'LAL','lakers','Los Angeles'],
    'Magic': ['Magic',1610612753,'ORL','magic','Orlando'],
    'Mavericks': ['Mavericks',1610612742,'DAL','mavericks','Dallas'],
    'Nets': ['Nets',1610612751,'BKN','nets','Brooklyn'],
    'Nuggets': ['Nuggets',1610612743,'DEN','nuggets','Denver'],
    'Olympians': ['Olympians',1610610030,'INO','olympians','Indianapolis'],
    'Pacers': ['Pacers',1610612754,'IND','pacers','Indiana'],
    'Pistons': ['Pistons',1610612765,'DET','pistons','Detroit'],
    'Raptors': ['Raptors',1610612761,'TOR','raptors','Toronto'],
    'Rebels': ['Rebels',1610610026,'CLR','rebels','Cleveland'],
    'Redskins': ['Redskins',1610610033,'SHE','redskins','Sheboygan'],
    'Rockets': ['Rockets',1610612745,'HOU','rockets','Houston'],
    'Spurs': ['Spurs',1610612759,'SAS','spurs','San Antonio'],
    'Stags': ['Stags',1610610025,'CHS','stags','Chicago'],
    'Steamrollers': ['Steamrollers',1610610032,'PRO','steamrollers','Providence'],
    'Suns': ['Suns',1610612756,'PHX','suns','Phoenix'],
    'Thunder': ['Thunder',1610612760,'OKC','thunder','Oklahoma City'],
    'Timberwolves': ['Timberwolves',1610612750,'MIN','timberwolves','Minnesota'],
    'Trail Blazers': ['Trail Blazers',1610612757,'POR','blazers','Portland'],
    'Warriors': ['Warriors',1610612744,'GOS','warriors','Golden State'],
    'Wizards': ['Wizards',1610612764,'WAS','wizards','Washington'],
}


CurTeams = {
    '76ers': ['76ers',1610612755,'PHL','sixers','Philadelphia'],
    'Bobcats': ['Bobcats',1610612766,'CHA','bobcats','Charlotte'],
    'Bucks': ['Bucks',1610612749,'MIL','bucks','Milwaukee'],
    'Bulls': ['Bulls',1610612741,'CHI','bulls','Chicago'],
    'Cavaliers': ['Cavaliers',1610612739,'CLE','cavaliers','Cleveland'],
    'Celtics': ['Celtics',1610612738,'BOS','celtics','Boston'],
    'Clippers': ['Clippers',1610612746,'LAC','clippers','Los Angeles'],
    'Grizzlies': ['Grizzlies',1610612763,'MEM','grizzlies','Memphis'],
    'Hawks': ['Hawks',1610612737,'ATL','hawks','Atlanta'],
    'Heat': ['Heat',1610612748,'MIA','heat','Miami'],
    'Pelicans': ['Pelicans',1610612740,'NOH','pelicans','New Orleans'],
    'Jazz': ['Jazz',1610612762,'UTA','jazz','Utah'],
    'Kings': ['Kings',1610612758,'SAC','kings','Sacramento'],
    'Knicks': ['Knicks',1610612752,'NYK','knicks','New York'],
    'Lakers': ['Lakers',1610612747,'LAL','lakers','Los Angeles'],
    'Magic': ['Magic',1610612753,'ORL','magic','Orlando'],
    'Mavericks': ['Mavericks',1610612742,'DAL','mavericks','Dallas'],
    'Nets': ['Nets',1610612751,'BKN','nets','Brooklyn'],
    'Nuggets': ['Nuggets',1610612743,'DEN','nuggets','Denver'],
    'Pacers': ['Pacers',1610612754,'IND','pacers','Indiana'],
    'Pistons': ['Pistons',1610612765,'DET','pistons','Detroit'],
    'Raptors': ['Raptors',1610612761,'TOR','raptors','Toronto'],
    'Rockets': ['Rockets',1610612745,'HOU','rockets','Houston'],
    'Spurs': ['Spurs',1610612759,'SAS','spurs','San Antonio'],
    'Suns': ['Suns',1610612756,'PHX','suns','Phoenix'],
    'Thunder': ['Thunder',1610612760,'OKC','thunder','Oklahoma City'],
    'Timberwolves': ['Timberwolves',1610612750,'MIN','timberwolves','Minnesota'],
    'Trail Blazers': ['Trail Blazers',1610612757,'POR','blazers','Portland'],
    'Warriors': ['Warriors',1610612744,'GOS','warriors','Golden State'],
    'Wizards': ['Wizards',1610612764,'WAS','wizards','Washington'],
}

TeamInfo = {}
for key, val in Teams.iteritems():
    fullName = "{0} {1}".format(val[4], key)
    TeamInfo[fullName.lower()] = val


class Team:
    def __init__(self, name, id, abbr, code, city):
        self.name = name
        self.id = id
        self.abbr = abbr
        self.code = code
        self.city = city
    def getAbbreviation(self):
        return self.abbr

    def getCode(self):
        return self.code

    def getName(self):
        return self.name

    def getFullName(self):
        return "{0} {1}".format(self.city, self.name)

    def getId(self):
        return self.id

    def __repr__(self):
        return "Team=({0}, {1}, {2}, {3}, {4})".format(
            self.name, self.id, self.abbr, self.code, self.city)


CurTeamObjects = []
teamIdMap = {}
fullNameMap = {}
for key, val in CurTeams.iteritems():
    team = Team(*val)
    CurTeamObjects.append(team)
    teamIdMap[team.getId()] = team
    fullNameMap[team.getFullName()] = team


def getTeamById(teamId):
    return teamIdMap[teamId] if teamId in teamIdMap else None


def getTeamByFullName(name):
    return fullNameMap[name] if name in fullNameMap else None
