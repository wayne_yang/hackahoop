import db_utils
from compute_similarplayer import findNearestNeighbours
import player
from mysql.connector.errors import IntegrityError
import sys

conn = db_utils.connect(db_utils.dbConfig)
cursor = conn.cursor()
query = ("select current_player.player_id from current_player left join overall_careerstats on (current_player.player_id = overall_careerstats.player_id) where SEASON_ID=2012 order by player_id");
cursor.execute(query)
rows = cursor.fetchall()
playerList = [i[0] for i in rows]
cursor.close()


conn2 = db_utils.connect(db_utils.dbConfig)
statCursor = conn2.cursor()
query = ("select MIN, PTS, FGM, FGA, FTM, FTA, FG3M, FG3A, REB, AST, STL, BLK, TOV from current_player as cp left join overall_careerstats as cs on ( cp.player_id=cs.player_id) where SEASON_ID=2012 order by cp.player_id")
statCursor.execute(query)
careerStats = statCursor.fetchall()


conn3 = db_utils.connect(db_utils.dbConfig)
importCursor = conn3.cursor()
insertSQL = "INSERT INTO nba_stats.similar_player (player_id, similar_player_id, score) VALUES (%s, %s, %s)"

info = player.getPlayerInfo(playerList)

for player in playerList:
    name = info[player][3]
    similar = findNearestNeighbours(player, 5, careerStats=careerStats)

    for i in range(len(similar)):
        #print player, similar[i], i
        record = (player, similar[i], i)
        try:
            importCursor.execute(insertSQL, record)
        except IntegrityError as e:
            print record, e
            sys.exit()


conn.commit()
conn2.commit()
conn3.commit()
importCursor.close()
conn.close()
