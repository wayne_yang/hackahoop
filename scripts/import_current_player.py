import mysql.connector
import json
import sys
import time
import datetime
import db_utils

encoding = "utf-8"

insertSql = ("INSERT INTO nba_stats.playerinfo (PLAYER_ID,FIRST_NAME,"
            "LAST_NAME,DISPLAY_FIRST_LAST,BIRTHDATE,SCHOOL,COUNTRY,"
            "LAST_AFFILIATION,JERSEY,POSITION,ROSTERSTATUS,TEAM_ID,"
            "TEAM_NAME,TEAM_ABBREVIATION,TEAM_CODE,TEAM_CITY) VALUES ("
            "%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)")

cnx = db_utils.connect(db_utils.dbConfig)
cursor = cnx.cursor()

for line in sys.stdin:
    parts = line.split('|')
    #print parts[1]

    #record = (playerId, firstName, lastName, displayFirstLast, birthDate, school, country, lastAffiliation, jersey, position, rosterStatus, teamId, teamName, teamAbbreviation, teamCode, teamCity) 
    #cursor.execute(insertSql, record)


cnx.commit()
cursor.close()
cnx.close()

