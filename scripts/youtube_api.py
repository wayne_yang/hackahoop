from apiclient.discovery import build
import json
import argparse
import sys

DEVELOPER_KEY = "AIzaSyDzglmRvkS9gmGOrbJbMIBoJ_T4fXUnvwo"
YOUTUBE_API_SERVICE_NAME = "youtube"
YOUTUBE_API_VERSION = "v3"

def search(options):
    youtube = build(YOUTUBE_API_SERVICE_NAME, YOUTUBE_API_VERSION,
        developerKey=DEVELOPER_KEY)

    search_response = youtube.search().list(
        q=options["q"],
        part="id,snippet",
        type=options["type"],
        maxResults=options["maxResults"]).execute()

    return search_response


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Search Youtube Videos')
    parser.add_argument("q", help="keyword")
    args = parser.parse_args()

    resp = search({
        "q": args.q,
        "type": "video",
        "maxResults": 25,})
    print json.dumps(resp)
