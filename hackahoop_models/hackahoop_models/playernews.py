from .meta import Base, DBSession
from .facebook import FacebookFriends
from .account import AuthUser
from .playerinfo import PlayerInfo
from .careerstats import CombinedCareerStats

from sqlalchemy import (
    Column,
    DateTime,
    Integer,
    String,
    Text,
    Unicode,
    ForeignKey)
from sqlalchemy.dialects.mysql import BIT
from sqlalchemy.orm import relationship, contains_eager, aliased
from sqlalchemy.exc import IntegrityError
from sqlalchemy import desc

import json
import re
import urllib
from datetime import datetime, timedelta

import logging
log = logging.getLogger(__name__)

def getDisplayTime(dt, curDT=None):
    if curDT is None:
        curDT=datetime.utcnow()
    elapsed = curDT - dt
    if elapsed.days < 1:
        return getRelativeTime(dt, curDT=curDT)
    elif elapsed.days == 1:
        return "Yesterday"
    elif curDT.year == dt.year:
        return dt.strftime('%b %d')
    else:
        return dt.strftime('%b %d, %Y')


def getRelativeTime(dt, curDT=None):
    """
    dt - datetime object in utc timezone
    """
    if curDT is None:
        curDT=datetime.utcnow()

    elapsed = curDT - dt
    rtime = "{0} {1} ago"
    totalSeconds = elapsed.total_seconds()
    if totalSeconds < 60:
        unit = totalSeconds > 1 and "seconds" or "second"
        return rtime.format(int(totalSeconds), unit) 
    elif totalSeconds < 3600:
        minutes = int(totalSeconds/60)
        unit = minutes > 1 and "minutes" or "minute"
        return rtime.format(minutes, unit)
    elif totalSeconds < 86400:
        hours = int(totalSeconds/3600)
        unit = hours > 1 and "hours" or "hour"
        return rtime.format(hours, unit)
    elif elapsed.days < 365:
        unit = elapsed.days > 1 and "days" or "day"
        return rtime.format(elapsed.days, unit)
    else:
        years = int(elapsed.days/365)
        unit = years > 1 and "years" or "year"
        return rtime.format(years, unit)

class Tweet(Base):
    __tablename__ = "tweet"
    id = Column("id", String, primary_key=True)

    # 'unicode' string
    authorName = Column("author_name", Unicode)
    authorScreenName = Column("author_screen_name", Unicode)
    text = Column("text", Unicode)
    entities = Column("entities", Unicode)
    parsedEntities = None
    expandedText = None

    retweetCount = Column("retweet_count", Integer)
    favoriteCount = Column("favorite_count", Integer)
    verified = Column("verified", BIT)
    createdAt = Column("created_at", DateTime)
    profileImageUrl = Column("profile_image_url", String)

    def __repr__(self):
        s = (u"<Tweet(id={0},authorName={1},authorScreenName={2},text={3},"
            "entities={4},retweetCount={5},favoriteCount={6},verified={7},createdAt={8})")
        return s.format(self.id, self.authorName,
            self.authorScreenName, self.text,
            self.entities, self.retweetCount, self.favoriteCount, self.verified,
            self.createdAt)

    def getTweetLink(self):
        return "https://twitter.com/{0}/status/{1}".format(
            self.authorScreenName, self.id
        )

    def getDisplayCreatedTime(self):
        return getDisplayTime(self.createdAt)
    
    def getExpandedText(self):
        if self.expandedText is not None:
            return self.expandedText

        if self.parsedEntities is None:
            self.parsedEntities = json.loads(self.entities)

        #log.debug(self.parsedEntities)

        # [{u'url': u'http://t.co/3nfChD7zuI', u'indices': [114, 136], u'expanded_url': u'http://bit.ly/ZDCfkA', u'display_url': u'bit.ly/ZDCfkA'}]
        result = self.text
        if "urls" in self.parsedEntities and self.parsedEntities["urls"]:
            urls = self.parsedEntities["urls"]
            newUrl = u"<a href=\"{0}\" title=\"{1}\" target=\"_blank\">{2}</a>"
            for url in urls:
                result = result.replace(
                    url["url"],
                    newUrl.format(url["url"],
                        url["expanded_url"],
                        url["display_url"]))

        # u'user_mentions': [{u'indices': [3, 9], u'id_str': u'25319414', u'screen_name': u'NBATV', u'name': u'NBA TV', u'id': 25319414}, {u'indices': [35, 44], u'id_str': u'20265254', u'screen_name': u'nyknicks', u'name': u'NBA New York Knicks', u'id': 20265254}, {u'indices': [49, 57], u'id_str': u'21308488', u'screen_name': u'bobcats', u'name': u'Charlotte Bobcats', u'id': 21308488}, {u'indices': [60, 76], u'id_str': u'72878707', u'screen_name': u'MettaWorldPeace', u'name': u'Metta World Peace', u'id': 72878707}]
        if "user_mentions" in self.parsedEntities:
            #log.debug(self.parsedEntities["user_mentions"])
           
            for m in self.parsedEntities["user_mentions"]:
                screenNameBytes = m["screen_name"].encode("utf8")
                result = re.sub(
                    r'@({0})'.format(screenNameBytes),
                    r'<a href="https://twitter.com/\1" target="_blank">@\1</a>',
                    result,
                    flags=re.IGNORECASE|re.UNICODE)

        # u'hashtags': [{u'indices': [92, 108], u'text': u'NBASummerLeague'}], u'urls': []}
        if "hashtags" in self.parsedEntities:
            for hashtag in self.parsedEntities["hashtags"]:
                hashTagTextBytes = hashtag["text"].encode("utf8")
                queryStr = urllib.quote_plus("#{0}".format(hashTagTextBytes))
                result = re.sub(
                    r'#({0})'.format(hashTagTextBytes),
                    r'<a href="https://twitter.com/search?q={0}">#\1</a>'.format(queryStr),
                    result,
                    flags=re.IGNORECASE|re.UNICODE)

        self.expandedText = result
        return result


class PlayerTweet(Base):
    __tablename__ = "player_tweets"
    playerId = Column("player_id", Integer, primary_key=True)
    tweetId = Column("tweet_id", String, ForeignKey('tweet.id'), primary_key=True)

    tweet = relationship("Tweet", order_by=Tweet.createdAt.desc(), innerjoin=True)
    #tweet = relationship("Tweet", primaryjoin="Tweet.id == PlayerTweet.tweetId")

    def __repr__(self):
        s = (u"<PlayerTweet(playerId={0},tweetId={1},tweet={2})")
        return s.format(self.playerId, self.tweetId, self.tweet)

    @classmethod
    def getByPlayerId(cls, playerId, limit=20, sinceDT=None):
        query = (DBSession.query(cls)
            .join(cls.tweet)
            .options(contains_eager(cls.tweet))
            .filter(cls.playerId == playerId))

        if sinceDT is not None:
            query = query.filter(Tweet.createdAt < sinceDT)

        return query.order_by(desc(Tweet.createdAt)).limit(limit).all()


class FollowRelationship(Base):
    __tablename__ = "follow_relationship"
    playerId = Column("player_id", Integer, ForeignKey("player_tweets.player_id"), primary_key=True)
    userId = Column("user_id", Integer, ForeignKey("auth_id.id"), primary_key=True)

    @classmethod
    def add(cls, playerId, userId, raiseExcept=False):
        """
        raiseExcept=True, method will throw IntegrityError if the entry already exists in db
        """
        rel = FollowRelationship(playerId=playerId, userId=userId)
        DBSession.add(rel)

        try:
            DBSession.flush()
        except IntegrityError as e:
            log.info("Failed to add follow relationship: {0}".format(e))
            if raiseExcept:
                raise

        return rel

    @classmethod
    def remove(cls, playerId, userId):
        (DBSession.query(cls)
            .filter(cls.playerId == playerId)
            .filter(cls.userId == userId)
            .delete())

    @classmethod
    def followBy(cls, playerId, userId):
        return (DBSession.query(cls)
            .filter(cls.playerId == playerId)
            .filter(cls.userId == userId)
            .first())

    @classmethod
    def getFollowers(cls, playerId):
        """
        Return followers for a given player in the form of:
        
            [(userId, login), ...]

        """
        rows = (DBSession.query(cls.userId, AuthUser.login).select_from(cls)
            .join(AuthUser, AuthUser.authId == cls.userId)
            .filter(cls.playerId == playerId)
            .all())
        return rows

    @classmethod
    def getFollowing(cls, userId, limit=20, statsSeason=False):
        stats = aliased(CombinedCareerStats, name="stats")
        entities = [PlayerInfo.playerId, PlayerInfo.fullName, PlayerInfo.seoName]
        if statsSeason:
            entities.append(stats)

        query = (DBSession.query(*entities)
            .select_from(cls)
            .join(PlayerInfo, cls.playerId == PlayerInfo.playerId)
            .filter(cls.userId == userId))

        if statsSeason:
            query = (query.join(stats, cls.playerId == stats.playerId)
                .filter(stats.seasonId == statsSeason))

        if limit:
            query.limit(limit)
        return query.all()

    @classmethod
    def getFollowingCount(cls, userId):
        return (DBSession.query(cls)
            .filter(cls.userId == userId)
            .count())


    def __repr__(self):
        return u"<FollowRelationship(playerId={0}, userId={1})>".format(
            self.playerId, self.userId)
            

def getNewsFeed(userId, limit=20, beforeDT=None):
    if limit < 0:
        limit = 20
    query = (DBSession.query(Tweet).select_from(FollowRelationship)
        .join(PlayerTweet)
        .join(Tweet)
        .filter(FollowRelationship.playerId == PlayerTweet.playerId)
        .filter(PlayerTweet.tweetId == Tweet.id)
        .filter(FollowRelationship.userId == userId))

    if beforeDT is not None:
        query = query.filter(Tweet.createdAt < beforeDT)

    return (query.order_by(desc(Tweet.createdAt))
        .limit(limit)
        .all())


def getFriendFollowers(authId, playerId):
    # get existing followers' facebook IDs: (authId, facebookId)
    followers = FollowRelationship.getFollowers(playerId)
    #log.debug("followers={0}".format(followers))

    followerDict = {}
    for f in followers:
        followerDict[f[1]] = f

    # name and id tuple
    friends = FacebookFriends.getFriends(authId)
    #log.debug("friends={0}".format(friends))

    result = []
    for friend in friends:
        friendId = friend["id"]
        if friendId in followerDict:
            friend["authId"] = followerDict[friendId][0] 
            result.append(friend)
    return result
