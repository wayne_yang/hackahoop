from pyramid.response import Response
from pyramid.view import view_config
from pyramid.renderers import render
from hackahoop_models.gamelog import GameLog
from datetime import datetime

def format_highcharts_line(rows):
    """
    rows - list of GameLog objects
    """

    """
seasonId=22011,playerId=202704,gameId=21100053,gameDate=2011-12-31,curTeam=OKC,opponent=PHX,homeGame=,win=,min=5,fgm=1,fga=3,fgPCT=0.333,fg3m=0,fg3a=2,fg3PCT=0.000,ftm=0,fta=0,ftPCT=0.000,oreb=0,dreb=0,reb=0,ast=1,stl=0,blk=0,tov=0,pf=0,pts=2,seasonType=R
    """
    result = [i.toDict() for i in rows]
    result.reverse()
    return {
        'gamelog': result
    }


formatMapper = {
    'highcharts_line': format_highcharts_line
}


@view_config(route_name='gamelog_api', renderer='json')
def gamelog_api_view(request):
    """
    @urlparam limit - max number of records to return
    @urlparam playerId - the player id
    @urlparam cols - the comma-separate list of columns
    @urlparam format
    """
    GET = request.GET
    playerId = GET.get('playerId')
    limit = None

    format = "highcharts_line"
    if "format" in GET:
        format = GET["format"]

    """
    TODO: needs to enforce limit on the parameter
    """

    if 'limit' in GET:
        try:
            limit = int(GET["limit"])
        except ValueError: pass
            
    sinceDays = None
    if "sinceDays" in GET:
        try:
            sinceDays = int(GET["sinceDays"])
        except ValueError: pass

    since = None
    until = None
    if "since" in GET:
        try:
            since = datetime.strptime(GET["since"], "%m-%d-%Y")
        except ValueError: pass

    if "until" in GET:
        try:
            until = datetime.strptime(GET["until"], "%m-%d-%Y")
        except ValueError: pass
       
    data = GameLog.getByPlayerId(playerId, 
        limit=limit, sinceDays=sinceDays,
        sinceDT=since, untilDT=until)

    if format in formatMapper:
        return formatMapper[format](data)
    else:
        return formatMapper['highcharts_line'](data)

