from .meta import Base, DBSession
from .team import Team
from sqlalchemy.orm import contains_eager, relationship
from sqlalchemy import or_
import json

from sqlalchemy import (
    Column,
    SmallInteger,
    Integer,
    Numeric,
    String,
    Unicode,
    Date,
    ForeignKey)


class PlayerInfo(Base):
    __tablename__ = 'playerinfo'
    playerId = Column('PLAYER_ID', Integer, primary_key=True)
    seoName = Column('SEO_NAME', Unicode)
    firstName = Column('FIRST_NAME', Unicode)
    lastName = Column('LAST_NAME', Unicode)
    suffix = Column('SUFFIX', Unicode)
    fullName = Column('FULL_NAME', Unicode)
    displayFirstLast = Column('DISPLAY_FIRST_LAST', Unicode)
    birthDate = Column('BIRTHDATE', Date)
    school = Column('SCHOOL', Unicode)
    country = Column('COUNTRY', Unicode)
    lastAffiliation = Column('LAST_AFFILIATION', Unicode)
    jersey = Column('JERSEY', SmallInteger)
    position = Column('POSITION', String)
    rosterStatus = Column('ROSTERSTATUS', Integer)
    teamId = Column('TEAM_ID', Integer, ForeignKey("team.id"))

    team = relationship("Team")

    def copyFrom(self, other):
        self.playerId = other.playerId
        self.seoName = other.seoName
        self.firstName = other.firstName
        self.lastName = other.lastName
        self.suffix = other.suffix
        self.fullName = other.fullName
        self.displayFirstLast = other.displayFirstLast
        self.birthDate = other.birthDate
        self.school = other.school
        self.country = other.country
        self.lastAffiliation = other.lastAffiliation
        self.jersey = other.jersey
        self.position = other.position
        self.rosterStatus = other.rosterStatus
        self.teamId = other.teamId

    @classmethod
    def searchActivePlayer(cls, keyword, limit=10):
        """
        Returns a list of KeyTuple.  Each tuple has the labels:
            - abbreviation, firstName, lastName, seoName
        """
        if keyword.find(" ") == -1:
            cond = or_(cls.firstName.like("{0}%".format(keyword)),
                cls.lastName.like("{0}%".format(keyword)))
        else:
            cond = cls.fullName.like("%{0}%".format(keyword))

        result = (DBSession.query(Team.abbreviation, cls.firstName, cls.lastName, cls.seoName)
            .select_from(cls)
            .join(Team, Team.id == cls.teamId)
            .filter(cond)
            .filter(cls.rosterStatus == 1)
            .limit(limit))
        return result 
 

    @classmethod
    def getById(cls, playerId):
        return (DBSession.query(cls)
            .join(cls.team)
            .options(contains_eager(cls.team))
            .filter(cls.playerId == playerId)
            .first())

    @classmethod
    def getBySeoName(cls, seoName, dbSession=None):
        session = dbSession or DBSession
        return (session.query(cls)
            .join(cls.team)
            .options(contains_eager(cls.team))
            .filter(cls.seoName == seoName)
            .first())

    @classmethod
    def getByName(cls, 
        firstName,
        lastName,
        teamId = None,
        suffix = None,
        rosterStatus = None):

        query = (DBSession.query(cls)
            .filter(cls.firstName == firstName)
            .filter(cls.lastName == lastName))
        
        if rosterStatus is not None:
            query = query.filter(cls.rosterStatus == rosterStatus)

        if teamId is not None:
            query = query.filter(cls.teamId == teamId)

        if suffix is not None:
            query = query.filter(cls.suffix == suffix)

        return query.first()

    def getQuotedFullName(self):
        return json.dumps(self.fullName);

    def __repr__(self):
        s = (u"<PlayerInfo(playerId={0},firstName={1},lastName={2},"
            "displayFirstLast={3},birthDate={4},school={5},country={6},"
            "lastAffiliation={7},jersey={8},position={9},rosterStatus={10},"
            "teamId={11}, seoName={12}>")
        return s.format(self.playerId, self.firstName, self.lastName,
                self.displayFirstLast, self.birthDate, self.school,
                self.country, self.lastAffiliation, self.jersey, self.position,
                self.rosterStatus, self.teamId, self.seoName)
