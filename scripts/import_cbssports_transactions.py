from bs4 import BeautifulSoup
from teams import Teams
import db_utils
import sys

"""
http://www.cbssports.com/nba/transactions/free-agents
"""

conn = db_utils.connect(db_utils.dbConfig)
cursor = conn.cursor()
f = open("/home/wyang/Downloads/cbssports_transactions.html")
soup = BeautifulSoup(f.read())

# should move to player module
selectPlayerSQL = "SELECT player_id from playerinfo where first_name = %s and last_name = %s and rosterstatus = 1"
updatePlayerSQL = "UPDATE playerinfo SET team_name = %s, team_id = %s, team_abbreviation = %s, team_code = %s, team_city = %s WHERE player_id = %s"

tables = soup.select("div#layoutRailRight table")
for tr in tables[1].find_all("tr", align="right"):
    tds = tr.find_all("td")
    nameParts = tds[0].text.split(u", ")
    oldTeam = tds[5].text
    newTeam = tds[6].text
    if newTeam != "--" and oldTeam != newTeam:
        cursor.execute(selectPlayerSQL, (nameParts[1], nameParts[0]))
        rows = cursor.fetchall()
        if len(rows) > 1:
            sys.stderr.write("More than one row matched for player {0} {1}\n".format(
                nameParts[1], nameParts[0]));
        elif len(rows) < 1:
            sys.stderr.write("No rows matched for player {0} {1}\n".format(
                nameParts[1], nameParts[0]));
            
        # print rows, nameParts[0], nameParts[1]
        playerId = rows[0][0]
        teamName, teamId, teamAbbr, teamCode, teamCity = Teams[newTeam]    
        #print (teamName, teamId, teamAbbr, teamCode, teamCity, playerId)
        cursor.execute(updatePlayerSQL, (teamName, teamId, teamAbbr, teamCode, teamCity, playerId))

    #print u" ".join([td.text for td in tds])


conn.commit()
cursor.close()
conn.close()

