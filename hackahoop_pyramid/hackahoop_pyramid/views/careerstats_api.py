from pyramid.response import Response
from pyramid.view import view_config
from pyramid.renderers import render

from sqlalchemy.exc import DBAPIError
from sqlalchemy.orm import contains_eager

from hackahoop_models.meta import DBSession
from hackahoop_models.careerstats import CombinedCareerStats
from hackahoop_models.playerinfo import PlayerInfo
from sqlalchemy import asc, desc

import urllib
import logging
log = logging.getLogger(__name__)


rangeParamMap = {
    'playerAge' : {},
    'gp' : {},
    'gs' : {},
    'min' : {},
    'fgm' : {},
    'fga' : {},
    'fgPCT' : {},
    'fg3m' : {},
    'fg3a' : {},
    'fg3PCT' : {},
    'ftm' : {},
    'fta' : {},
    'ftPCT' : {},
    'oreb' : {},
    'dreb' : {},
    'reb' : {},
    'ast' : {},
    'stl' : {},
    'blk' : {},
    'tov' : {},
    'pf' : {},
    'pts' : {},
}

aColumns = [None, None, None, CombinedCareerStats.gp, CombinedCareerStats.min, CombinedCareerStats.fgPCT, CombinedCareerStats.ftPCT, CombinedCareerStats.fg3m, CombinedCareerStats.pts, CombinedCareerStats.reb, CombinedCareerStats.ast, CombinedCareerStats.stl, CombinedCareerStats.blk, CombinedCareerStats.tov]

scalarParamMap = (
    'playerId', 'seasonId', 'leagueId', 'teamId'
)

enumParamMap = {
    "pos": PlayerInfo.position
}

class InputParamError(Exception):
    def __init__(self, errMsg):
        self.errMsg = errMsg
    def __str__(self):
        return repr(self.errMsg)

def getParamAsTuple(params, paramName):
    """ Parse a paramName in params that looks like (x, y) 
        into a tuple consisting of a lower and upper range float values 

        params - MultiDict type
    """
    try:
        value = params.getone(paramName)
        parts = value.split(',', 1)

        log.debug("getParamAsTuple: parts={0}".format(parts))

        if len(parts) != 2:
            raise InputParamError("Param {0} is not in the format (x,y)".format(paramName))
        start = float(parts[0])

        end = None
        if parts[1] != '':
            end = float(parts[1])

    except ValueError as e:
        raise InputParamError("Param {0} contains non-numeric value: {1}".format(paramName, e))

    except KeyError:
        raise InputParamError("Param {0} is specified more than once".format(paramName))

    return (start, end)

def getParamAsInt(params, paramName):
    try:
        value = int(params.getone(paramName))

    except ValueError:
        raise InputParamError("Param {0} contains non-numeric value".format(paramName))

    except KeyError:
        raise InputParamError("Param {0} is specified more than once".format(paramName))

    return value


@view_config(route_name='careerstats_api', renderer='json')
def my_view(request):
    format = "html"
    GET = request.GET
    if "format" in GET:
        format = GET.get("format")

    if format.find("json") == 0:
        return render2json(request)

    elif format == "datatable":
        return render2datatable(request)

    else:
        return render2html(request)

def _configureQuery(GET):
    """
    Return dict of valid query params
    @throws InputParamError
    """
    params = {}
    query = (DBSession.query(CombinedCareerStats)
        .options(contains_eager(CombinedCareerStats.player, PlayerInfo.team))
        .join(CombinedCareerStats.player)
        .join(PlayerInfo.team)
        .filter(PlayerInfo.rosterStatus == 1))
    for param in GET:
        if param in rangeParamMap:
            pData = rangeParamMap[param]
            start, end = getParamAsTuple(GET, param)
            dbField = getattr(CombinedCareerStats, param)
            query = query.filter(dbField >= start)
            if end is not None: 
                query = query.filter(dbField <= end)

            log.debug("param={0} value=({1},{2})".format(param, start, end))
            params[param] = GET.get(param)

        elif param in scalarParamMap:
            value = getParamAsInt(GET, param)
            dbField = getattr(CombinedCareerStats, param)
            query = query.filter(dbField == value)
            log.debug("param={0} value={1})".format(param, value))
            params[param] = GET.get(param)

        elif param in enumParamMap:
            dbField = enumParamMap[param]
            dbValues = GET["pos"].split(",")
            query = query.filter(dbField.in_(dbValues))
    return (query, params)


def render2datatable(request):
    try:
        log.debug("request.GET=%s", request.GET)
        GET = request.GET

        query, params = _configureQuery(GET)

        ### TODO:  int() need to be checked for exceptions

        if "iSortCol_0" in GET:
            iSortingCols = int(GET.get("iSortingCols"))
            for i in range(iSortingCols):
                colIndex = int(GET.get("iSortCol_{0}".format(i)))
                sortableCol =  "bSortable_{0}".format(colIndex)
                if GET.get(sortableCol) == "true":
                    direction = asc
                    if GET.get("sSortDir_{0}".format(i)) == "desc":
                        direction = desc
                    query = query.order_by(direction(aColumns[colIndex]))
                    

        rowCount = query.count()

        # limit/offset
        data = None
        if "iDisplayStart" in GET:
            start = int(GET.get("iDisplayStart"))
            end = start + int(GET.get("iDisplayLength"))
            data = query[start:end]
        else:
            data = query.limit(10)
            
        jsData = []
        for cs in data:
            name = "<a href=\"/player/{0}\">{1} {2}</a>".format(cs.playerId, cs.player.firstName, cs.player.lastName)
            jsData.append([
                cs.playerId, name, cs.player.team.abbreviation, cs.gp,
                cs.min, cs.fgPCT, cs.ftPCT, cs.fg3m,
                cs.pts, cs.reb, cs.ast, cs.stl,
                cs.blk, cs.tov])

        sEcho = int(GET.get("sEcho"))

        return {
            "sEcho": sEcho,
            "iTotalRecords": rowCount,
            "iTotalDisplayRecords": rowCount,
            "aaData": jsData,
        }


    except InputParamError as e:
        return Response(e.errMsg, content_type='text/plain', status_int=400)

    except DBAPIError as e:
        return Response("Failed to connect to data source: {0}".format(e), content_type='text/plain', status_int=500)



def render2json(request):
    try:
        log.debug("request.GET=%s", request.GET)
        GET = request.GET

        params = None
        query = None
        try:
            query, params = _configureQuery(GET)
            #log.debug(query)

        except InputParamError as e:
            return Response(e.errMsg, content_type='text/plain', status_int=400)


        # query param 'limit'
        limit = 20
        if 'limit' in GET:
            limit = int(GET.get('limit'))

        rowCount = query.count()
        start = 0
        end = 0
        if "start" in GET:
            start = int(GET.get("start"))

        uriPrefix = urllib.urlencode(params)
        nextUri = None
        prevUri = None
        if start - limit >= 0:
            prevUri = "{0}&start={1}".format(uriPrefix, start - limit)
            
        if start + limit >= rowCount:
            end = rowCount;
        else:
            end = start + limit
            nextUri = "{0}&start={1}".format(uriPrefix, start + limit)

        # query param 'order'
        order = CombinedCareerStats.min
        data = query.order_by(order).all()

    except DBAPIError as e:
        return Response("Failed to connect to data source: {0}".format(e), content_type='text/plain', status_int=500)

    result = {}
    jsData = []
    if GET.get("format") == "json_obj":
        for cs in data:
            jsData.append({
                "playerId": cs.playerId, "team": cs.player.team.abbreviation, "gp": cs.gp,
                "min": cs.min, "fgPCT": cs.fgPCT, "ftPCT": cs.ftPCT,
                "fg3m": cs.fg3m, "pts": cs.pts, "reb": cs.reb, "ast": cs.ast,
                "stl": cs.stl, "blk": cs.blk, "tov": cs.tov
            })
    else:
        playerLinkStr = "<a href=\"{playerLink}\">{playerName}</a> <a href=\"{playerLink}\" target=\"_blank\"><img src=\"{iconLink}\" alt=\"Open in a new window\"></a>"
        for cs in data:
            # TODO:  this should be done in FE
            name = playerLinkStr.format(
                playerLink=request.route_path("single_player", playerName=cs.player.seoName),
                playerName=cs.player.fullName,
                iconLink=request.static_url("hackahoop_pyramid:static/img/external-link-grey-dark02.png"))
            jsData.append([
                cs.playerId, name, cs.player.team.abbreviation,
                cs.player.position, cs.gp, cs.min, cs.fgPCT, cs.ftPCT,
                cs.fg3m, cs.pts, cs.reb, cs.ast, cs.stl,
                cs.blk, cs.tov])

    result["careerstats"] = jsData
    if len(data) > 0:
        result["player"] = {
            "name": data[0].player.fullName
        }

    #log.debug(jsData)
    return result


def render2html(request):
    try:
        log.debug("request.GET=%s", request.GET)
        GET = request.GET
        query = DBSession.query(CombinedCareerStats)

        params = None
        try:
            query, params = _configureQuery(GET)

        except InputParamError as e:
            return Response(e.errMsg, content_type='text/plain', status_int=400)

        # query param 'limit'
        limit = 20
        if 'limit' in GET:
            limit = int(GET.get('limit'))

        rowCount = query.count()
        start = 0
        end = 0
        if "start" in GET:
            start = int(GET.get("start"))

        uriPrefix = urllib.urlencode(params)
        nextUri = None
        prevUri = None
        log.debug("GET.start={0}".format(start))
        if start - limit >= 0:
            prevUri = "{0}&start={1}".format(uriPrefix, start - limit)
            
        if start + limit >= rowCount:
            end = rowCount;
        else:
            end = start + limit
            nextUri = "{0}&start={1}".format(uriPrefix, start + limit)


        log.debug("start={0},end={1},count={2}".format(start,end,rowCount))
        log.debug("prevUri={0},nextUri={1}".format(prevUri,nextUri))
        log.debug(request.path_info)

        # query param 'order'
        order = CombinedCareerStats.min

        #log.debug("query={0}".format(query))

        data = query.order_by(order)[start:end]

    except DBAPIError as e:
        return Response("Failed to connect to data source: {0}".format(e), content_type='text/plain', status_int=500)

    result = {
        "count": rowCount,
        "start": start + 1,
        "end": end
    }
    if nextUri is not None:
        result['nextUri'] = nextUri

    if prevUri is not None:
        result['prevUri'] = prevUri

    templateData = {'data': data, 'count': rowCount} 
    result["content"] = render('../templates/careerstats_api.pt', templateData , request=request)
    return result

