from pyramid.view import view_config
from pyramid.httpexceptions import HTTPFound
from pyramid.response import Response

from utils import (getForbiddenError, check_csrf_token, getParam,
    getBadRequestError, getUnauthorizedError)

from hackahoop_models.playerinfo import PlayerInfo
from hackahoop_models.rate import Rating, RatingDataError

import json
import logging
log = logging.getLogger(__name__)

@view_config(route_name="delete_rate", renderer="json")
def delete_rate_view(request):
    """
    Delete a rating.  Need id
    """
    if not request.user:
        return getUnauthorizedError("Unauthorized request")

    if not check_csrf_token(request):
        return getForbiddenError("Failed CSRF check")

    rateId = getParam(request, "rateId", transforms=[int]) 
    if rateId == None:
        return getBadRequestError("Invalid rate id")

    Rating.remove(request.user["userId"], rateId)

@view_config(route_name="global_rate_form", renderer="../templates/rate_form.pt")
def global_rate_form_view(request):
    if request.user == None:
        fromPath = "{0}?from={1}".format(
            request.route_path("register"),
            request.current_route_path())
        return HTTPFound(location=fromPath)

    player = None
    redirectUrl = None
    playerTokenJson = []

    if "from" in request.GET:
        redirectUrl = request.GET["from"]
    else:
        redirectUrl = request.route_path("index")

    return {"player": player, "redirectUrl": redirectUrl, "playerTokenJson": playerTokenJson}


@view_config(route_name="rate_form", renderer="../templates/rate_form.pt")
def rate_form_view(request):
    if request.user == None:
        fromPath = "{0}?from={1}".format(
            request.route_path("register"),
            request.current_route_path())
        return HTTPFound(location=fromPath)

    if "playerName" not in request.matchdict:
        # TODO: fixed this 
        return Response("playerName is missing", content_type='text/html', status_int=400)

    playerName = request.matchdict["playerName"]
    player = PlayerInfo.getBySeoName(playerName)

    redirectUrl = None
    playerTokenJson = []
    if player:
        playerTokenJson = [{
            "value": player.seoName,
            "label": player.fullName,
        }]
        if "from" in request.GET:
            redirectUrl = request.GET["from"]
        else:
            redirectUrl = request.route_path('discussions', playerName=player.seoName)

    playerTokenJson = json.dumps(playerTokenJson);
    return {"player": player, "redirectUrl": redirectUrl, "playerTokenJson": playerTokenJson}


@view_config(route_name="rate_submit")
def rate_submit_view(request):
    if not request.user:
        return HTTPFound(location=request.route_path("index"))

    if not check_csrf_token(request):
        # TODO:  for now redirect to front page
        request.session.flash("Invalid rating submission (csrf)")
        log.error("Failed CSRF check: {0}".format(request))
        return HTTPFound(location=request.route_path("index"))
        
    """
    redirectUrl = None
    if "from" in request.params:
        redirectUrl = request.params["from"]
    else:
        redirectUrl = request.route_path("index")
    """

    POST = request.POST
    seoName = POST["playerId"]
    try:
        validatePostData(POST)

        realPlayerId = PlayerInfo.getBySeoName(seoName).playerId
        if not realPlayerId:
            raise RatingDataError("Invalid player: {0}".format(seoName))
            
        rating = Rating.add(request.user["userId"], realPlayerId, POST["title"],
            POST["ratingValue"], POST["opinion"])

    except RatingDataError as e:
        request.session.flash(e)
        log.error("Rating input error: {0}".format(e))
        return HTTPFound(location=request.route_path("index"))

    # save post data to db
    return HTTPFound(location=request.route_path('discussions', playerName=seoName))


class RatingDataError(Exception): pass

def validatePostData(POST):
    if "playerId" not in POST:
        raise RatingDataError("playerId field is missing")
    if "ratingValue" not in POST:
        raise RatingDataError("ratingValue field is missing")
    if "title" not in POST:
        raise RatingDataError("title field is missing")
    if "opinion" not in POST:
        raise RatingDataError("opinion field is missing")


