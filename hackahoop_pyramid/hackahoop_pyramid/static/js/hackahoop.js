$.GA = {
    /* If true, will send request to google, if false will log to console */
    liveTracking: true,

    /**
     * Track an event with the given parameters
     * If category or action are not defined, nothing will happen
     */
    track: function( category, action, label, value, noninteraction ) {
        var active, varsDefined, present, eventFields;

        // Check the Google Analytics script is present
        active = ( typeof ga !== "undefined" );
        if (!active && this.liveTracking) {
            console.log("ga is not defined");
        }
        
        // Category and action are required fields, check this now and exit if not true 
        varsDefined = ( typeof category !== "undefined" && typeof action !== "undefined" );
        present = ( category && action );
        if (!present || !varsDefined) {
            console.log("category or action is not defined for tracking");
        }
        if ( !active || !varsDefined || !present ) { return;}

        eventFields = {
            "hitType": "event",
            "eventCategory": category,
            "eventAction": action,
        }

        // Only add the label if it is present
        if ( typeof label !== "undefined" && label) {
            eventFields["eventLabel"] = label;
        }

        // Only add the value if it is present
        if ( typeof value !== "undefined" && value ) {
            eventFields["eventValue"] = value;
        }

        // By default ignore this, if true however "bounce rate" will not be affected
        if ( typeof noninteraction !== "undefined" && noninteraction === true ) {
            eventFields["nonInteraction"] = noninteraction;
        }

        // Send event to Google
        if ( this.liveTracking ) {
            ga("send", eventFields);
        } else {
            if ( typeof console !== "undefined" && console.log ) {
                console.log( "GA Event: ", eventFields );
            }
        }
        return true;
    },

    /* Assign a callback function that is fired once Google has collected the event */
    setHitCallback: function( callback ) {
        //ga( [ '_set', 'hitCallback', callback ] );
    },

    enableDevMode: function() {
        this.liveTracking = false;
    },

    trackOutboundLink: function(url, category, action, label, newWindow) {
        try { 
            this.track(category, action, label);
        } catch(err) {}
         
        setTimeout(function() {
            if (newWindow) {
                window.open(url);
            } else {
                document.location.href = url;
            }
        }, 100);
    },
};

$.fn.isOnScreen = function(){
    var win = $(window);
    var viewport = {
        top : win.scrollTop(),
        left : win.scrollLeft()
    };
    viewport.right = viewport.left + win.width();
    viewport.bottom = viewport.top + win.height();
     
    var bounds = this.offset();
    bounds.right = bounds.left + this.outerWidth();
    bounds.bottom = bounds.top + this.outerHeight();

    /*
    console.log("viewport"); console.log(viewport);
    console.log("bounds"); console.log(bounds);
    */
     
    return (!(viewport.right < bounds.left || viewport.left > bounds.right || viewport.bottom < bounds.top || viewport.top > bounds.bottom));
};


var HACKAHOOP = HACKAHOOP || {};
HACKAHOOP.FollowingList = (function() {
  var init = function(settings) {
        HACKAHOOP.FollowingList.cfg = {
            $unfollowBtn:  $(".unfollow-btn"),
            $followingCounter:  $(".following-count"),
            $errorBlock: $(".alert-error"),
            $emptyFollowBlock: $("#empty-follow"),
        };
        $.extend(HACKAHOOP.FollowingList.cfg, settings)

        /* setup work */
        HACKAHOOP.FollowingList.cfg.$unfollowBtn.click(unfollowHandler);
    };
    var unfollowHandler = function() {
        var $form = $(this).parent("form");
        var url = $form.attr("action");
        var data = $form.serialize()
        $.post(url, data, function() {
            $form.closest("tr.following-entry").fadeOut(200, function() {
                $(this).remove();
            });
            if (decrementCounter() == 0) {
                HACKAHOOP.FollowingList.cfg.$emptyFollowBlock.fadeOut(200, function() { $(this).show(); });
            }
        })
        .fail(function(jqXHR, textStatus, errorThrown) {
            console.log("Failed to unfollow player: " + jqXHR.responseText);
            HACKAHOOP.FollowingList.cfg.$errorBlock.show();
        });
    };
    var decrementCounter = function() {
        var curCount = parseInt(
            HACKAHOOP.FollowingList.cfg.$followingCounter.text());
        if (curCount != NaN) {
            curCount--;
            if (curCount < 0) curCount = 0;
            HACKAHOOP.FollowingList.cfg.$followingCounter.text(curCount);
        } else {
            curCount = 0;
        }
        return curCount;
    };
    return {
        init: init,
    }
})();

HACKAHOOP.PlayerList = (function() {
    var dtOptions = {
        "bAutoWidth": false,
        "bDeferRender": true,
        "bFilter": false,
        "bProcessing": true,
        "aaSorting": [[ 4, "desc" ]],
        "iDisplayLength": 25,
        "aoColumnDefs": [
            { "aTargets": ["_all"], "sClass": "text-left" },
            { "aTargets": [0], "bVisible": false  },
            { "aTargets": [0, 1, 2], "bSortable": false },
        ]
    };

    var init = function(settings) {
        HACKAHOOP.PlayerList.cfg = {
            ajaxUrl: "/v1/nba/careerstats",
            $sliders: $("div.range-slider"),
            $seasonSelect: $("#seasonId"),
            $dataTable: $("#result-table").dataTable(dtOptions),
            $posCheckbox: $("input:checkbox[name=position]"),
        };
        $.extend(HACKAHOOP.PlayerList.cfg, settings)

        setup();
    };

    var setup = function() {
        var cfg = HACKAHOOP.PlayerList.cfg;
        cfg.$sliders.each(function(index, element) {
            var $el = $(element);
            var id = $el.attr("id");
            var min = parseFloat($el.attr("data-min-val"));
            var max = parseFloat($el.attr("data-max-val"));
            var step = parseFloat($el.attr("data-step"));

            var parts = id.split("-", 1);
            if (parts.length != 1) {
                console.log("sliderId " + id + " is not in expected format");
                return;
            }
            var statName = parts[0];
            //console.log("statName=" + statName + " id=" + id + " min=" + min + " max=" + max + " step=" + step); 
            //console.log("min=" + typeof(min) + " max=" + typeof(max) + " step=" + typeof(step)); 
            $el.slider({
                range: true,
                min: min,
                max: max,
                values: [min, max],
                step: step,
                slide: handleSlide(statName),
                change: handleChange(HACKAHOOP.PlayerList.cfg.$sliders)
            });
        });

        cfg.$posCheckbox.change(handleChange(cfg.$sliders));

        cfg.$seasonSelect.change(handleChange(cfg.$sliders));

        handleChange(cfg.$sliders)();
    };

    var updateDataTable = function(json) {
        var cfg = HACKAHOOP.PlayerList.cfg;
        cfg.$dataTable.fnClearTable(true);
        cfg.$dataTable.fnAddData(json.careerstats);
        cfg.$dataTable.$('tr').on("click", function() {
            $(this).toggleClass('row_selected');
        });
    }

    /* slider hanlders */
    var handleSlide = function(statName) {
        return function(event, ui) {
            $("#" + statName + "-range-text").text(ui.values[0] + " - " + ui.values[1]);
        };
    };

    var handleChange = function($sliders) {
        return function(event, ui) {
            var cfg = HACKAHOOP.PlayerList.cfg;
            var ajaxOptions = {
                url: cfg.ajaxUrl,
                type: "GET",
                dataType : "json",
                success: updateDataTable,
                error: function( xhr, status ) {
                    alert( "Sorry, there was a problem!" );
                },
            };
            var seasonId = cfg.$seasonSelect.find(":selected").val();

            if (ui !== undefined) {
                $.GA.track('PlayerList', 'ChangeSlider', this.id);
            }
            var query = {
                format: "json",
                seasonId: seasonId,
            };
            //query['order'] = statName;
            
            var positions = [];
            cfg.$posCheckbox.filter(":checked").each(function(index, elem) {
                positions.push(elem.value);
            });
            if (positions.length > 0) {
                query["pos"] = positions.join();
            }

            cfg.$sliders.each(function(index, elem) {
                var $el = $(elem);
                var parts = $el.attr("id").split("-", 1);
                if (parts.length != 1) {
                    console.log("sliderId " + id + " is not in expected format");
                }
                var id = parts[0];
                var values =  $el.slider("option", "values");
                query[id] = values[0] + "," + values[1];
            });

            ajaxOptions["data"] = query;

            $.ajax(ajaxOptions);
        }
    };
    return {
        init: init,
    }
})();

HACKAHOOP.RateList = (function() {
    /* check if a html element contains a css name */
    var hasClass = function(element, cls) {
        return (' ' + element.className + ' ').indexOf(' ' + cls + ' ') > -1;
    }
    var decrementRateCount = function() {
        var $rateCounter = $(".profile-stat-navbar .rate-count");
        var count = $rateCounter.text();
        count = parseInt(count);
        if (count != NaN) {
            count--;
            $rateCounter.text(count);
        }
    }
    var init = function(settings) {
        HACKAHOOP.RateList.cfg = {
            $rateEntries: $("li.rating-entry"),
            $cmdToggle: $("li.cmd-toggle"),
        };
        $.extend(HACKAHOOP.RateList.cfg, settings);

        HACKAHOOP.RateList.cfg.$rateEntries.click(function(e) {
            if (e.target.tagName.toLowerCase() == "a" && hasClass(e.target, "delete_rate_yes")) {
                $(e.currentTarget).find("form.delete-rate-form").submit();
            }
        });
        HACKAHOOP.RateList.cfg.$cmdToggle.click(function(e) {
            // only handles a tag that has a toggle class
            if (e.target.tagName.toLowerCase() == "a" && hasClass(e.target, "toggle")) {
                var $ct = $(e.currentTarget);
                $ct.find("span.option").toggle();
                $ct.find("span.text-error").hide();
            }
        });
        $("form.delete-rate-form").submit(function(e) {
            var $form = $(this);
            $.post($form.attr('action'), $form.serialize(), function(response) {
                $form.closest("li.rating-entry").fadeOut(200, function() { $(this).remove(); });
                decrementRateCount();
            },'json')
                .fail(function(jqXHR, textStatus, errorThrown) {
                    var $errorSpan = $form.find("span.text-error");
                    $errorSpan.text("delete failed: " + errorThrown);
                    console.log("Failed to delete rating: " + jqXHR.responseText);
                    $errorSpan.show();
                });
            e.preventDefault();
        });
    };
    return {
        init: init,
    }
})();


HACKAHOOP.YouTube = (function() {
    /* youtube videos */
    var ytEmbedApi = "http://www.youtube.com/embed/";
    var ytApiParams = {
        enablejsapi: 1,
        origin: window.location.origin,
    };

    var setupVideoClick = function() {
        HACKAHOOP.YouTube.cfg.$videoItems.on("click", function(event) {
            $("#video-list li.highlight").removeClass("highlight");
            var $videoItem = $(this);
            $videoItem.addClass("highlight");
            var videoId = $videoItem.attr("data-video-id");

            HACKAHOOP.YouTube.ytplayer.cueVideoById(videoId);
        });
    },
    setupSideBarScroll = function() {
        var ytSidebarScrolled = false;
        $("div.video-sidebar").scroll(function() {
            if (!ytSidebarScrolled) { 
                ytSidebarScrolled = true;
                $.GA.track('Videos', 'ScrollSideBar');
            }
        });
    },
    setUpWindowScroll = function() {
        var ytLoaded = false;
        $(window).scroll(function() {
            if (!ytLoaded && HACKAHOOP.YouTube.cfg.$ytPlayer.isOnScreen()) {
                ytLoaded = true;
                loadFirstVideo();    
                $.GA.track('Videos', 'VideoSecShown');
            }
        })
    },
    loadFirstVideo = function() {
        var firstVidId = HACKAHOOP.YouTube.cfg.$videoItems.first().attr("data-video-id");
        HACKAHOOP.YouTube.cfg.$ytPlayer.attr("src", _makeYTLink(firstVidId));
    },
    _makeYTLink = function(videoId) {
        return ytEmbedApi + videoId + "?" + $.param(ytApiParams);
    },
    getVideoItems = function() {
        return HACKAHOOP.YouTube.cfg.$videoItems;
    },
    setup = function() {
        setupVideoClick();
        setupSideBarScroll();
        loadFirstVideo();    
        //setUpWindowScroll();
    },
    init = function(settings) {
        HACKAHOOP.YouTube.cfg = {
            $videoItems: $("li.video-item"),
            $ytPlayer: $("#ytplayer"),
        };
        $.extend(HACKAHOOP.YouTube.cfg, settings);

        setup();
    };

    return {
        init: init,
    }
})();


HACKAHOOP.CareerStats = (function() {
    var url = "/v1/nba/careerstats";
    var fetch = function(playerId, seasonId, successCB) {
        $.ajax({
            url: url,
            data: { playerId: playerId, seasonId: seasonId, format: "json_obj" },
            type: 'GET',
            dataType: 'json',
            success: successCB,
            error: function(xhr, status) {
                //TODO: fix this
                alert("Sorry, there was a problem!");
            }
        });
    };
    return {
        fetch: fetch,
    }
})();


HACKAHOOP.GameLogs = (function() {
    var url = '/v1/nba/gamelog';
    var fetch = function(playerId, sinceDays, limit, sinceDT, untilDT, successCB) {
        var data = { playerId: playerId, format: "highcharts_line" };
        if (limit != null) {
            data["limit"] = limit;
        }
        if (sinceDays != null) {
            data["sinceDays"] = sinceDays;
        }
        if (sinceDT != null) {
            data["since"] = (sinceDT.getMonth() + 1).toString() 
                + "-"
                + sinceDT.getDate().toString() 
                + "-"
                + sinceDT.getFullYear().toString();
        }
        if (untilDT != null) {
            data["until"] = (untilDT.getMonth() + 1).toString()
                + "-"
                + untilDT.getDate().toString()
                + "-"
                + untilDT.getFullYear().toString();
        }

        $.ajax({
            url: url,
            data: data,
            type: 'GET',
            dataType: 'json',
            success:  successCB,
            error: function(xhr, status) {
                //TODO: fix this
                alert("Sorry, there was a problem!");
            }
        });
    };
    return {
        fetch: fetch,
    }
})();


HACKAHOOP.RadarChart = (function() {
    var init = function(settings) {
        HACKAHOOP.RadarChart.cfg = {
            rotoStatCategories: null,
            maxStat: null, // required
            $rows: $("tr.similar-player"),
            seasonId: 2012,
            $radarChart: $("#radar-chart"),
            clickTracking: {},
            chartOptions: {
                chart: {
                    polar: true,
                    type: 'area',
                },
                title: { text: null, },
                credits: { enabled: false, },
                legend: { enabled: false, },
                xAxis: {
                    tickmarkPlacement: 'on',
                    lineWidth: 0
                },
                yAxis: {
                    gridLineInterpolation: 'polygon',
                    lineWidth: 0,
                    min: 0,
                    max: 1,
                    labels: {
                        enabled: false,
                    }
                },
                tooltip: {
                    shared: true,
                    pointFormat: '<span style="color:{series.color}">{series.name}: <b>{point.value}</b><br/>',
                },
            },
        };
        $.extend(true, HACKAHOOP.RadarChart.cfg, settings);

        // setup work
        HACKAHOOP.RadarChart.cfg.$rows.click(setupClick);

        initChart();
    },
    initChart = function() {
        var cfg = HACKAHOOP.RadarChart.cfg;
        var $radarChart = cfg.$radarChart;
        if (cfg.chartOptions.series[0].data.length == 0) {
            $radarChart.hide();
            $("#no-radar-chart").show();
        } else {
            // build plot categories from roto stat categories
            var plotCategories = [];
            var count = cfg.rotoStatCategories.length;
            for (var i = 0; i < count; i++) {
                plotCategories.push(cfg.rotoStatCategories[i].displayName);
            }
            cfg.chartOptions.xAxis.categories = plotCategories;
            
            $radarChart.highcharts(cfg.chartOptions);
        }
    },
    setupClick = function(e) {
        var $row = $(e.currentTarget);
        if (e.target.tagName == "A") {
            /* follow link if player name is clicked */
            return;
        }
        rowToDataSeries($row);
        var checkbox = $row.find("input[type=checkbox]").get(0);
        if ($row.hasClass("active")) {
            removeFromChart($row);
            checkbox.checked = false;
        } else {
            var tracking = HACKAHOOP.RadarChart.cfg.clickTracking;
            var playerId = $row.attr("data-player-id");
            if (!tracking.hasOwnProperty(playerId)) {
                tracking[playerId] = true;
                $.GA.track('Player', 'AddToRadarChart', playerId);
            }

            addRowToChart($row);
            checkbox.checked = true;
        }
        $row.toggleClass("active");
    },
    removeFromChart = function($row) {
        $row.data('data-radar-series').remove();
        //TODO:   put css in a class
        $row.find("td").eq(0).css("background-color", "");
    },

    /**
     * Read data from $tr object and cache the data series in $row for later removal
     */
    addRowToChart = function($row) {
        var playerName = $row.attr("data-player-name");
        var color = $row.attr("data-series-color");
        var data = rowToDataSeries($row);
        var series = HACKAHOOP.RadarChart.cfg.$radarChart.highcharts().addSeries({
            name: playerName,
            data: data,
            marker: { enabled: false },
            color: color,
        });
        $row.data('data-radar-series', series);

        //TODO:   put css in a class
        $row.find("td").eq(0).css("background-color", color);
    },
    rowToDataSeries = function($row) {
        var map = rowToMap($row);
        var data = [];
        var statCount = HACKAHOOP.RadarChart.cfg.rotoStatCategories.length;
        for (var i = 0; i < statCount; i++) {
            var statId = HACKAHOOP.RadarChart.cfg.rotoStatCategories[i].name;
            data.push({
                y: map[statId]/HACKAHOOP.RadarChart.cfg.maxStat[statId],
                //name: radarStatNames[i],
                value: map[statId], 
            });
        }
        return data;
    },
    rowToMap = function($row) {
        var res = {};
        $row.find("td").each(function() {
            var $this = $(this);
            var name = $this.attr("data-name");
            if (name !== undefined) {
                res[name] = $this.attr("data-value");
            }
        });
        return res;
    },
    addToRadarChartCB = function($row, color) {
        return function(json) {
            var data = ajaxDataToDataSeries(json);
            var series = HACKAHOOP.RadarChart.cfg.$radarChart.highcharts().addSeries({
                name: json.player.name,
                data: data,
                marker: { enabled: false },
                color: color,
            });

            // cache data
            $row.data('careerstats', json);
            $row.data('data-radar-series', series);
       };
    },
    ajaxDataToDataSeries = function(json) {
        var careerstats = json.careerstats[0];
        var data = [];
        var statCount = HACKAHOOP.RadarChart.cfg.rotoStatCategories.length;
        for (var i = 0; i < statCount; i++) {
            var statId = HACKAHOOP.RadarChart.cfg.rotoStatCategories[i].name;
            data.push({
                y: careerstats[statId]/HACKAHOOP.RadarChart.cfg.maxStat[statId],
                //name: radarStatNames[i],
                value: careerstats[statId], 
            });
        }
        return data;
    };

    return {
        init: init,
    }
})();

HACKAHOOP.RateForm = (function() {
    var init = function(settings) {
        HACKAHOOP.RateForm.cfg = {
            $title: $("#title"),
            $opinion: $("#opinion"),
            $helpLinks: $("#markdown-help-toggle a"),
            $form: $("form.opinion-form"),
            $radioInput: $("input[type=radio]"),
            $helpDiv: $("div.markdown-help"),
            $playerId: $("#playerId"),
            playerTokens: [],
            tokenFieldConfig: {
                autocomplete: HACKAHOOP.PlayerSearch.baseAutoCompleteConfig,
            }
        };
        $.extend(HACKAHOOP.RateForm.cfg, settings);

        setup();
    },
    showError = function(fieldName) {
        $("#" + fieldName + "-ctl-grp").addClass("error");
        $("#" + fieldName + "-error-ctl").show();
    },
    removeError = function(fieldName) {
        return function() {
            $("#" + fieldName + "-ctl-grp").removeClass("error");
            $("#" + fieldName + "-error-ctl").hide();
        }
    },
    toggleHelp = function(e) {
        e.preventDefault();
        $(this).parent("span").find("a").toggle();
        HACKAHOOP.RateForm.cfg.$helpDiv.toggle();
    },
    beforeCreateTokenHandler = function(e) {
        var tokens = $(this).tokenfield('getTokens');
        if (tokens.length > 0) {
            $(this).tokenfield('setTokens', []);
            return false;
        }
    },
    afterCreateTokenHandler = function(e) {
        var id = $(this).attr("id");
        $("#" + id + "-tokenfield").attr("placeholder", "");
        removePlayerInputError();
    },
    submitHandler = function() {
        var cfg = HACKAHOOP.RateForm.cfg;
        if (!validatePlayerName()) {
            return false;
        }
        if (cfg.$title.val().trim() == "") {
            showError("title");
            return false;
        }

        var $checked = $("input[name=ratingValue]:checked")
        if ($checked.length == 0) {
            showError("rating");
            return false;
        }
        if (cfg.$opinion.val().trim() == "") {
            showError("opinion");
            return false;
        }

        $.GA.track('Player', 'Rate', $checked.val());
        this.submit();
    },
    showPlayerInputError = function() {
        showError("playerId");
        $("div.tokenfield").addClass("error");
    },
    removePlayerInputError = function() {
        removeError("playerId")();
        $("div.tokenfield").removeClass("error");
    },
    validatePlayerName = function() {
        var tokens = HACKAHOOP.RateForm.cfg.$playerId.tokenfield("getTokens");
        if (tokens.length == 0) {
            showPlayerInputError();
            return false;
        } else {
            removePlayerInputError();
            return true;
        }
    },
    setup = function() {
        var cfg = HACKAHOOP.RateForm.cfg;
        cfg.$form.submit(submitHandler);
        cfg.$title.keydown(removeError("title"));
        cfg.$radioInput.change(removeError("rating"));
        cfg.$opinion.keydown(removeError("opinion"));
        cfg.$helpLinks.click(toggleHelp);
        cfg.$playerId.on("beforeCreateToken", beforeCreateTokenHandler)
            .on("afterCreateToken", afterCreateTokenHandler)
            .tokenfield(cfg.tokenFieldConfig);

        // disabled player input field if player name is given
        if (cfg.playerTokens.length > 0) {
            cfg.$playerId.tokenfield("setTokens", cfg.playerTokens);
        }

        $("#" + cfg.$playerId.attr("id") + "-tokenfield").blur(validatePlayerName);
    };
    return {
        init: init,
    }
})();


HACKAHOOP.PlayerSearch = (function() {
    var baseAutoCompleteConfig = {
        source: "/v1/nba/playersearch",
        minLength: 1,
        delay: 300,
    },
    init = function(settings) {
        var acConfig = $.extend({}, baseAutoCompleteConfig, {
            select: selectHandler,
            focus: focusHandler,
        });

        HACKAHOOP.PlayerSearch.cfg = {
            $searchbox: $("#searchbox"),
            $form: $('#search-form'),
            autocompleteConfig: acConfig,
        };
        $.extend(HACKAHOOP.PlayerSearch.cfg, settings);

        setup();
    },
    selectHandler = function(event, ui) {
        // ui.item == {id, value, label}
        var item = ui.item;
        var cfg = HACKAHOOP.PlayerSearch.cfg;
        cfg.$searchbox.val(item.playerName);
        cfg.$form.attr('action', '/player/' + item.value)
        cfg.$form.submit();
        return false;
    },
    focusHandler = function(event, ui) {
        var item = ui.item
        HACKAHOOP.PlayerSearch.cfg.$searchbox.val(item.playerName);
        return false;
    },
    monkeyPatchAutocomplete = function() {
        $.ui.autocomplete.prototype._renderItem = function (ul, item) {
            var keywords = $.trim(this.term).split(' ').join('|');
            var output = item.label.replace(new RegExp("(" + keywords + ")", "gi"), '<strong>$1</strong>');
            return $("<li>")
                .append($("<a>").html(output))
                .appendTo(ul);
        };
    },
    submitHandler = function() {
        if (HACKAHOOP.PlayerSearch.cfg.$searchbox.val().trim() == '') {
            return false;
        }
    },
    setup = function() {
        monkeyPatchAutocomplete();
        var cfg = HACKAHOOP.PlayerSearch.cfg;
        cfg.$searchbox.autocomplete(cfg.autocompleteConfig);
        cfg.$form.submit(submitHandler);
    };

    return {
        init: init,
        baseAutoCompleteConfig: baseAutoCompleteConfig,
    }
})();


HACKAHOOP.Follow = (function() {
    var init = function(settings) {
        HACKAHOOP.Follow.cfg = {
            $form: $("#follow-form"),
            $following: $("#follow-form .following"),
            $unfollow: $("#follow-form .unfollow"),
            $follow: $("#follow-form .follow"),
            $buttons: $("#follow-form .btn"),
            playerId: $("#follow-form input[name='playerId']").val(),
        };
        $.extend(HACKAHOOP.Follow.cfg, settings);

        var cfg = HACKAHOOP.Follow.cfg;
        cfg.$following.on("mouseover mouseout", mouseEventHandler);
        cfg.$unfollow.on("mouseover mouseout", mouseEventHandler);
        cfg.$buttons.click(submitHandler);
    };

    var mouseEventHandler = function(event) {
        var cfg = HACKAHOOP.Follow.cfg;
        if (event.type == 'mouseover') {
            cfg.$following.hide();
            cfg.$unfollow.show();
        } else {
            /* mouseout might be generated when hiding button */
            if (!cfg.$follow.is(":visible")) {
                cfg.$following.show();
            }
            cfg.$unfollow.hide();
        }
    };

    var submitHandler = function() {
        var cfg = HACKAHOOP.Follow.cfg;
        var $this = $(this);
        var data = cfg.$form.serialize();
        var url = $this.attr("data-action");
        if ($this.hasClass("follow")) {
            cfg.$follow.hide();
            cfg.$following.show();

            $.GA.track('Player', 'Follow', cfg.playerId);
        } else {
            cfg.$follow.show();
            cfg.$following.hide();
            cfg.$unfollow.hide();

            $.GA.track('Player', 'Unfollow', cfg.playerId);
        }
        $.post(url, data, function() {
        }).fail(function() {
            alert("There was an error in follow/unfollow action");
        });
        return false;
    };

    return {
        init: init,
    }
})();

HACKAHOOP.GameLogChart = (function() {
    var init = function(settings) {
        HACKAHOOP.GameLogChart.cfg = {
            playerId: null, 
            careerStats: null,
            initialSinceDays: 7,
            $gameLogSection: $("#gamelog-section"),
            $chart: $('#gamelog-chart'),
            $noChart: $('#no-gamelog-chart'),
            $gamelogToolbar: $('#gamelog-toolbar'),
            $zoomRangeFilters: $("#zoom-range a"),
            $gamelogTable: $("#gamelog-table"),
            $gamelogDateFrom: $("#gamelog-date-from"),
            $gamelogDateTo: $("#gamelog-date-to"),
            gameRangeFilterTracking: {},
            dateRangeFilterTracking: false,
            $statFilter: $("#stat-filter"),
            statFilterTracking: {},
            seasonAvgSeriesOptions: {
                name: 'Season Average',
                color: '#FF0000',
                lineWidth: 2,
                marker: { enabled: false },
                enableMouseTracking: false,
                type: 'line',
            },

            /* date picker options */
            fromDatePickerOptions: {
                defaultDate: -7,
                changeMonth: true,
                numberOfMonths: 1,
                onClose: function(selectedDate) {
                    HACKAHOOP.GameLogChart.cfg.$gamelogDateTo.datepicker(
                        "option", "minDate", selectedDate);
                },
                onSelect: dateRangeHandler,
            },
            toDatePickerOptions: {
                defaultDate: 0,
                changeMonth: true,
                numberOfMonths: 1,
                onClose: function( selectedDate ) {
                    HACKAHOOP.GameLogChart.cfg.$gamelogDateFrom.datepicker(
                        "option", "maxDate", selectedDate);
                },
                onSelect: dateRangeHandler,
            },

            /* highchart options */
            chartOptions: {
                chart: {
                    events: {
                        load: chartLoadHandler,
                    },
                },
                credits: { enabled: false, },
                title: { text: 'Minutes' },
                subtitle: { text: null },
                tooltip: {
                    crosshairs: true,
                    formatter: function() {
                        /* TODO: might want to move this to server side */
                        var pt = this.point;
                        var s = (pt.win ? 'WIN' : 'LOSS') + ' on ';
                        s += Highcharts.dateFormat('%m/%d/%Y', this.x) + '<br />';
                        s += pt.curTeam + (pt.homeGame ? ' vs ' : ' @ ') + pt.opponent + '<br />';
                        s += getActiveStatFilter().text + ': <b>' + this.y + '</b>';
                        return s;
                    }
                },
                plotOptions: {
                    /* line: { dataLabels: { enabled: true }, }, */
                    area: {
                        marker: { 
                            enabled: true,
                            lineColor: '#058dc7',
                            lineWidth: 2,
                        },
                        color: '#E5F3F9',
                        lineColor: '#058dc7',
                    },
                },
                legend: {
                    align: "center",
                    layout: "horizontal",
                    verticalAlign: "bottom",
                    enabled: false,
                },
                xAxis: {
                    type: 'datetime',
                    title: {
                        text: 'Game Date',
                    },
                    dateTimeLabelFormats: {
                        day: '%b %e',
                        week: '%b %e',
                    }
                },
                /* yAxis: { title: { text: 'MIN', } }, */
                series: [],
            }
        };
        $.extend(true, HACKAHOOP.GameLogChart.cfg, settings);

        setup();
    };

    var setup = function() {
        var cfg = HACKAHOOP.GameLogChart.cfg;
        cfg.$chart.highcharts(cfg.chartOptions);
        cfg.$zoomRangeFilters.click(zoomRangeHandler);
        cfg.$statFilter.change(filterStat);

        /* initialize date to 1w ago */
        cfg.$gamelogDateFrom.datepicker(cfg.fromDatePickerOptions);
        cfg.$gamelogDateFrom.datepicker("setDate",
            new Date(new Date().setDate(new Date().getDate() - cfg.initialSinceDays)));

        /* initialize to today's date */
        cfg.$gamelogDateTo.datepicker(cfg.toDatePickerOptions);
        cfg.$gamelogDateTo.datepicker("setDate", new Date());

        cfg.$chart.data('season_avg_data', cfg.careerStats);
    };

    var getDisplayDate = function(dateObj) {
        return ('0' + (dateObj.getMonth() + 1)).slice(-2)
            + "/" + ('0' + dateObj.getDate().toString()).slice(-2)
            + "/" + dateObj.getFullYear().toString();
    }

    var dateRangeHandler = function(dateText, inst) {
        /* unhighlight the zoom range selector */ 
        $("#zoom-range a.active").toggleClass("active");

        var cfg = HACKAHOOP.GameLogChart.cfg;
        var since = cfg.$gamelogDateFrom.datepicker("getDate");
        var until = cfg.$gamelogDateTo.datepicker("getDate");
        var seriesName = getDisplayDate(since) + " - " + getDisplayDate(until);
        var callback = loadGameLogChart(seriesName, seriesName);

        if (!cfg.dateRangeFilterTracking) {
            $.GA.track('Player', 'DateRangeFilter', seriesName);
            cfg.dateRangeFilterTracking = true;
        }

        HACKAHOOP.GameLogs.fetch(cfg.playerId, null, null, since, until, callback);
    };

    var zoomRangeHandler = function(event) {
        var $this = $(this);

        /* highlight selected zoom range */
        $("#zoom-range a.active").toggleClass("active");
        $this.toggleClass("active");

        var zoomLevel = parseInt($this.attr("data-value"));

        var cfg = HACKAHOOP.GameLogChart.cfg;
        cfg.$gamelogDateFrom.datepicker("setDate",
            new Date(new Date().setDate(new Date().getDate() - zoomLevel)));

        if (!cfg.gameRangeFilterTracking.hasOwnProperty(zoomLevel)) {
            $.GA.track('Player', 'ZoomGameRange', zoomLevel);
            cfg.gameRangeFilterTracking[zoomLevel] = true;
        }

        var seriesData = getGameLogDataSeriesCache(cfg.playerId, zoomLevel);
        var seriesName = "Last " + zoomLevel + " Days";
        if (seriesData != undefined) {
            updateGameLogChart(seriesData, seriesName);
        } else {
            var callback = loadGameLogChart(seriesName, zoomLevel);
            var limit = null;
            HACKAHOOP.GameLogs.fetch(cfg.playerId, zoomLevel, limit, null, null, callback);
        }
    };

    /**
     * points: highcharts' series.data
     */
    var updateRawDataWithStat = function(points, statVal) { 
        for (var i = 0; i < points.length; i++) {
            points[i].y = points[i][statVal];
        }
    }

    var updateDataPointsWithStat = function(points, statVal, redraw) {
        for (var i = 0; i < points.length; i++) {
            points[i].update(points[i][statVal], redraw);
        }
    }

    var filterStat = function(event) {
        /* Remove highlight in zoom range */
        $("#zoom-range a.active").toggleClass("active");

        var selectedOption = this.options[this.selectedIndex];
        var selectedVal = selectedOption.value;
        var selectedText = selectedOption.text;
        var cfg = HACKAHOOP.GameLogChart.cfg;

        if (!cfg.statFilterTracking.hasOwnProperty(selectedVal)) {
            $.GA.track('Player', 'FilterStat', selectedVal);
            cfg.statFilterTracking[selectedVal] = true;
        }

        var highcharts = HACKAHOOP.GameLogChart.cfg.$chart.highcharts();
        var points = highcharts.series[0].data;

        var redraw = false;
        updateDataPointsWithStat(points, selectedVal, redraw);

        highcharts.yAxis[0].setTitle({ text: selectedText }, false);
        highcharts.setTitle({ text: selectedText });
        highcharts.redraw();

        //updateSeasonAvgSeries({value: selectedVal, text: selectedText});
    };

    /* Called when charts load and filter by gamelog */
    var chartLoadHandler = function() {
        var playerId = HACKAHOOP.GameLogChart.cfg.playerId;
        var zoomLevel = getCurrentZoomLevel();
        var seriesName = "Last " + zoomLevel + " Days";
        var callback = loadGameLogChart(seriesName, zoomLevel);
        var limit = null;
        var sinceDays = HACKAHOOP.GameLogChart.cfg.initialSinceDays;
        HACKAHOOP.GameLogs.fetch(playerId, sinceDays, limit, null, null, callback);
    };

    var getActiveStatFilter = function() {
        var $option = $("#stat-filter option:selected");
        return {
            text: $option.text(),
            value: $option.val(),
        }
    };
    var getCurrentZoomLevel = function() {
        var activeZoomRange = $("#zoom-range a.active");
        return parseInt(activeZoomRange.attr("data-value"));
    };

    var gamelog2DataSeries = function(json) {
        var newData = json.gamelog;
        var selectedStat = getActiveStatFilter();
        for (var i in newData) {
            var point = newData[i];
            var parts = point["gameDate"].split("-");
            point['x'] = Date.UTC(parts[0], parts[1] - 1, parts[2]);
            point['y'] = point[selectedStat.value];
        }
        return newData;
    };

    var makeSeriesDataCacheKey = function(playerId, zoomLevel) {
        return 'GLDataSeries-' + playerId + "-" + zoomLevel;
    };
    var getGameLogDataSeriesCache = function(playerId, zoomLevel) {
        var key = makeSeriesDataCacheKey(playerId, zoomLevel);
        return HACKAHOOP.GameLogChart.cfg.$gameLogSection.data(key);    
    };
    var putGameLogDataSeriesCache = function(playerId, zoomLevel, seriesData) {
        var key = makeSeriesDataCacheKey(playerId, zoomLevel);
        HACKAHOOP.GameLogChart.cfg.$gameLogSection.data(key, seriesData);
    };
    var makeSeriesCacheKey = function(playerId) {
        return "gl-series-" + playerId;
    };
    /* series is highcharts series */
    var putGameLogSeries = function(playerId, series) {
        var key = makeSeriesCacheKey(playerId);
        HACKAHOOP.GameLogChart.cfg.$gameLogSection.data(key, series);
    };
    var getGameLogSeries = function(playerId) {
        var key = makeSeriesCacheKey(playerId);
        return HACKAHOOP.GameLogChart.cfg.$gameLogSection.data(key);
    }

    var getCurrentSeasonAvgChartData = function(options) {
        var existingData;
        var selectedStat;
        if (options !== undefined) {
            if (options.hasOwnProperty("existingData")) {
                existingData = options["existingData"];
            }
            else if (options.hasOwnProperty("selectedStat")) {
                selectedStat = options["selectedStat"];
            }
        }
        if (existingData == undefined) {
            existingData = HACKAHOOP.GameLogChart.cfg.$chart.highcharts().series[0].data;
        }
        if (selectedStat == undefined) {
            selectedStat = getActiveStatFilter();
        }

        var seasonStat = HACKAHOOP.GameLogChart
            .cfg.$chart.data('season_avg_data')[selectedStat.value];
        var lowX = existingData[0].x;
        var highX = existingData[existingData.length - 1].x;
        return [
            [lowX, seasonStat],
            [highX, seasonStat]];
    };

    var updateSeasonAvgSeries = function(selectedStat) {
        var options = {};
        if (selectedStat !== undefined) {
            options["selectedStat"] = selectedStat;
        }
        var newData = getCurrentSeasonAvgChartData(options);
        var cfg = HACKAHOOP.GameLogChart.cfg;
        var highcharts = cfg.$chart.highcharts();
        if (highcharts.series.length == 1) {
            var series = $.extend({}, cfg.seasonAvgSeriesOptions, {data: newData});
            highcharts.addSeries(series);
        } else {
            highcharts.series[1].setData(newData);
        }
    };

    var loadGameLogChart = function(seriesName, filterCriteria) {
        return function (json) {
            // convert to data point series format
            var seriesData = gamelog2DataSeries(json);

            updateGameLogChart(seriesData, seriesName);

            // put data series in cache
            putGameLogDataSeriesCache(HACKAHOOP.GameLogChart.cfg.playerId, filterCriteria, seriesData);
            //updateSeasonAvgSeries(selectedStat);
        };
    };


    var updateGameLogChart = function(seriesData, seriesName) {
        // change y value to the selected stat
        var selectedStat = getActiveStatFilter();
        updateRawDataWithStat(seriesData, selectedStat.value);

        var cfg = HACKAHOOP.GameLogChart.cfg;
        if (seriesData.length == 0) {
            cfg.$chart.hide();
            cfg.$gamelogTable.hide();
            cfg.$noChart.show();
            return;
        } else {
            cfg.$chart.show();
            cfg.$gamelogTable.show();
            cfg.$noChart.hide();
        }

        var hc = cfg.$chart.highcharts();
        var playerId = cfg.playerId;
        var redraw = false;

        var seriesObj = getGameLogSeries(playerId);
        if (seriesObj == undefined) {
            seriesObj = hc.addSeries({
                name: seriesName,
                data: seriesData,
                type: 'area',
            }, redraw);

            // put series in cache
            putGameLogSeries(playerId, seriesObj);

        } else {
            seriesObj.update({name: seriesName}, redraw);
            seriesObj.setData(seriesData, redraw);
        }

        // update y-axis
        hc.yAxis[0].setTitle({ text: selectedStat.text }, redraw);
        hc.redraw();

        updateGameLogTable(seriesData);
    };

    var updateGameLogTable = function(seriesData) {
        var cfg = HACKAHOOP.GameLogChart.cfg;
        var dataCount = seriesData.length;
        var statCount = cfg.fullStatCategories.length;
        var statName = null;
        var tableRows = [];
        cfg.$gamelogTable.find("tbody tr").remove();
        for (var i = 0; i < dataCount; i++) {
            var data = seriesData[i];
            tableRows.push("<tr>");
            tableRows.push("<td>" + data.gameDate + "</td>");
            
            for (var j = 0; j < statCount; j++) {
                statName = cfg.fullStatCategories[j].name;
                tableRows.push("<td>" + data[statName] + "</td>");
            }
            tableRows.push("</tr>");
        }
        cfg.$gamelogTable.find("tbody").append(tableRows.join(""));
    }

    return {
        init: init,
    }
})();

HACKAHOOP.GameCalendar = (function() {
    var init = function(settings) {
        HACKAHOOP.GameCalendar.cfg = {
            $calendar: $("#calendar"),
            calendarOptions: {
                eventSources: [{
                    url: "/v1/nba/game_schedule",
                    color: '#2f7ed8',
                    error: function() {
                        // TODO: fix
                        alert("There was an error while fetching game schedule");
                    },
                }],
            }
        };
        $.extend(true, HACKAHOOP.GameCalendar.cfg, settings);
        
        var cfg = HACKAHOOP.GameCalendar.cfg;
        cfg.$calendar.fullCalendar(cfg.calendarOptions);
    };

    return {
        init: init,
    }
})();


HACKAHOOP.NewsfeedData = (function() {
    var url = '/v1/nba/newsfeed';
    var fetch = function(ajaxOptions) {
        var options = {
            url: url,
            type: 'GET',
            dataType: 'json',
            error: function(xhr, status) {
                //TODO: fix this
                alert("Sorry, there was a problem!");
            }
        };
        $.extend(true, options, ajaxOptions);
        $.ajax(options);
    };
    return {
        fetch: fetch,
    }
})();

HACKAHOOP.Newsfeed = (function() {
    var init = function(settings) {
        HACKAHOOP.Newsfeed.cfg = {
            $streamList: $("#stream-list"),
            $pagination: $("#pagination"),
            $loaderBlock: $("#loaderBlock"),
            $moreBtn: $("#more-btn"),
        };
        $.extend(true, HACKAHOOP.Newsfeed.cfg, settings);
        
        setup();
    };

    var setup = function() {
        var cfg = HACKAHOOP.Newsfeed.cfg;
        cfg.$moreBtn.click(showMore);
    };

    var showMore = function() {
        event.preventDefault();
        // track in ga
        $.GA.track('Newsfeed', 'ShowMore');
        var cfg = HACKAHOOP.Newsfeed.cfg;
        var before = cfg.$streamList.attr('data-last-date');
        var ajaxOptions = {
            data: { before: before },
            beforeSend: showLoader,
            success: fetchSuccess,
            complete: hideLoader,
        };
        HACKAHOOP.NewsfeedData.fetch(ajaxOptions);
    };

    var showLoader = function() {
        HACKAHOOP.Newsfeed.cfg.$loaderBlock.show();
    };

    var hideLoader = function() {
        HACKAHOOP.Newsfeed.cfg.$loaderBlock.hide();
    };

    var fetchSuccess = function(json) {
        var cfg = HACKAHOOP.Newsfeed.cfg;
        var $ul = cfg.$streamList;
        $ul.append(json.newsfeed);
        if (json.count == 0) {
            cfg.$pagination.hide();
        } else if (json.count > 0) {
            $ul.attr('data-last-date', json.lastCreateTime)
        }
    };

    return {
        init: init,
    }
})();
