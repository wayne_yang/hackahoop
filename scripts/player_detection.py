import sys
import player

playerInfo = {}

players = player.getAllActivePlayers(["full_name", "first_name", "last_name", "player_id"])


# load full names list
for player in players:
    fullName, firstName, lastName, playerId = player
    playerInfo[fullName.lower()] = {
        "fullName": fullName,
        "firstName": firstName,
        "lastName": lastName,
        "id": playerId,
    }

def detect_player(compareString):
    #print compareString.encode("utf-8")
    compareString = compareString.lower()
    result = []
    for fullName in playerInfo.keys():
        player = playerInfo[fullName]
        lastName = player["lastName"].lower()
        firstName = player["firstName"].lower()

        if compareString.find(fullName) != -1:
            result.append(player)
        elif compareString.find(fullName.replace(".", "").replace(" ", "")) != -1:
            result.append(player)
    return result

def get_compare_string(tweet):
    stringList = []
    userMentions = tweet["entities"]["user_mentions"]
    for mention in userMentions:
        stringList.append(mention["name"])
    stringList.append(tweet["text"])
    return " ".join([i for i in stringList])


if __name__ == "__main__":
    print detect_player("Derrick Rose Classic #NBASummerLeague: @DRose in 2008! http://t.co/cBvruDPYbN")


