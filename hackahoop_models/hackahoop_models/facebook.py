from .meta import Base, DBSession
from sqlalchemy import Column, Integer, Text

import requests
import json

class FacebookFriends(Base):
    __tablename__ = "facebook_friends"
    authId = Column("auth_id", Integer, primary_key=True)
    friends = Column("friends", Text)

    @classmethod
    def add(cls, authId, facebookFriends):
        newObj = cls(authId=authId, friends=facebookFriends)
        DBSession.add(newObj)

    @classmethod
    def getFriends(cls, authId):
        row = (DBSession.query(cls.friends)
            .filter(cls.authId == authId)
            .first())
        if not row :
            return []

        # [{u'name': u'Bruce Wayne', u'id': u'100032'}, ...]
        return json.loads(row[0])

class FacebookLikes(Base):
    __tablename__ = "facebook_likes"
    authId = Column("auth_id", Integer, primary_key=True)
    likes = Column("likes", Text)

    @classmethod
    def add(cls, authId, likes):
        newObj = cls(authId=authId, likes=likes)
        DBSession.add(newObj)

class FacebookInterests(Base):
    __tablename__ = "facebook_interests"
    authId = Column("auth_id", Integer, primary_key=True)
    interests = Column("interests", Text)

    @classmethod
    def add(cls, authId, interests):
        newObj = cls(authId=authId, interests=interests)
        DBSession.add(newObj)


FRIENDS = "friends"
INTERESTS = "interests"
LIKES = "likes"

def saveFacebookFriends(authId, profileId, accessToken):
    """
        authId: AuthId.id
    """
    data = fetchFacebookData(profileId, FRIENDS, accessToken)
    if data:
        FacebookFriends.add(authId, json.dumps(data))

def saveFacebookInterests(authId, profileId, accessToken):
    data = fetchFacebookData(profileId, INTERESTS, accessToken)
    if data:
        FacebookInterests.add(authId, json.dumps(data))

def saveFacebookLikes(authId, profileId, accessToken):
    data = fetchFacebookData(profileId, LIKES, accessToken)
    if data:
        FacebookLikes.add(authId, json.dumps(data))


def fetchFacebookData(profileId, dataType, accessToken):
    """
    profileId - the facebook profile id.  e.g. 233564
    """
    url = "https://graph.facebook.com/{0}/{1}?access_token={2}"
    r = requests.get(url.format(profileId, dataType, accessToken))

    mergedData = []
    parsedData = {}
    if r.status_code == 200:
        parsedData = json.loads(r.content)
        mergedData = parsedData["data"]

    while "paging" in parsedData and "next" in parsedData["paging"]:
        r = requests.get(parsedData["paging"]["next"])
        if r.status_code != 200:
            break
        parsedData = json.loads(r.content)
        mergedData = mergedData + parsedData["data"]

    return mergedData

