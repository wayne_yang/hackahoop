from pyramid.view import view_config

from datetime import datetime

from hackahoop_models.playernews import getNewsFeed
from utils import (
    getForbiddenError,
    getBadRequestError,
    getUnauthorizedError)

import logging
log = logging.getLogger(__name__)

tweetTemplate =  u"""<li class="stream-item"><div class="tweet-container"><div class="tweet-content"><div class="tweet-header"><a href="http://twitter.com/{authorScreenName}" target="_blank"> <img class="tweet-avatar" src="{profileImageUrl}"><strong>{authorName}</strong><span>@{authorScreenName}</span></a><span class="time"><a href="{tweetLink}" target="_blank">{displayCreatedTime}</a></span></div>{expandedText}</div></div></li>"""

@view_config(route_name="newsfeed", renderer="json")
def newsfeed_json_view(request):
    """
    Required login
    /v1/nba/newsfeed
        - playerId
        - before
        - limit
    """
    if not request.user:
        return getUnauthorizedError("Unauthenticated request")

    userId = request.user["userId"]
    beforeDT = None
    if "before" in request.GET:
        try:
            beforeDT = datetime.strptime(request.GET["before"], "%Y-%m-%d %H:%M:%S")
        except ValueError:
            return getBadRequestError("before param is not valid")

    newsfeed = getNewsFeed(userId, beforeDT=beforeDT) 
    count = len(newsfeed)
    result = ""
    for index, tweet in enumerate(newsfeed):
       """
       result += tweetTemplate.format( 
            expandedText = tweet.getExpandedText(),
            authorName = tweet.authorName,
            authorScreenName = tweet.authorScreenName,
            displayCreatedTime = tweet.getDisplayCreatedTime(),
            createdTime = tweet.createdAt.strftime('%Y-%m-%d %H:%M:%S'),
            profileImageUrl = tweet.profileImageUrl,
            tweetLink = tweet.getTweetLink())
       """
       a = tweetTemplate.format( 
            expandedText = tweet.getExpandedText(),
            authorName = tweet.authorName,
            authorScreenName = tweet.authorScreenName,
            displayCreatedTime = tweet.getDisplayCreatedTime(),
            createdTime = tweet.createdAt.strftime('%Y-%m-%d %H:%M:%S'),
            profileImageUrl = tweet.profileImageUrl,
            tweetLink = tweet.getTweetLink())
       result += a

            
    
    resp = {"newsfeed": result, "count": count}
    if count > 0:
        resp["lastCreateTime"] = newsfeed[-1].createdAt.strftime('%Y-%m-%d %H:%M:%S')

    return resp
