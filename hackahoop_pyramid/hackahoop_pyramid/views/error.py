from pyramid.view import notfound_view_config
from pyramid.view import view_config

from utils import logExcInfo

import logging
log = logging.getLogger(__name__)


@notfound_view_config(renderer="../templates/notfound.pt")
def notfound_view(request):
    excInfo = request.exc_info
    log.info("File not found: {0}".format(excInfo[1]))
    request.response.status_code = 404
    return {}

@view_config(context=Exception, renderer="../templates/error.pt")
def general_error_page(request):
    excInfo = request.exc_info
    if excInfo != None:
        logExcInfo(log.error, excInfo)
    request.response.status_code = 500
    return {}
