import mysql.connector
import json
import sys
import time
import datetime
import db_utils

encoding = "utf-8"

insertSql = ("INSERT INTO nba_stats.playerinfo (PLAYER_ID,FIRST_NAME,"
            "LAST_NAME,DISPLAY_FIRST_LAST,BIRTHDATE,SCHOOL,COUNTRY,"
            "LAST_AFFILIATION,JERSEY,POSITION,ROSTERSTATUS,TEAM_ID,"
            "TEAM_NAME,TEAM_ABBREVIATION,TEAM_CODE,TEAM_CITY) VALUES ("
            "%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)")

cnx = db_utils.connect(db_utils.dbConfig)
cursor = cnx.cursor()

for line in sys.stdin:
    data = json.loads(line)
    player = data["resultSets"][0]["rowSet"][0]
    playerId = player[0]
    firstName = player[1].encode(encoding)
    lastName = player[2].encode(encoding)
    displayFirstLast = player[5].encode(encoding)
    birthDate = player[6][-9].encode(encoding)
    #timeStruct = datetime.datetime.strptime(birthDateRaw, "%Y-%m-%d%Z%H:%M:%S")

    school = player[7].encode(encoding)
    country = player[8].encode(encoding)
    lastAffiliation = player[9].encode(encoding)
    jersey = player[10]
    if not jersey:
        jersey = -1
    position = player[11].encode(encoding)
    rosterStatus = player[12].encode(encoding)
    teamId = player[13]
    teamName = player[14].encode(encoding)
    teamAbbreviation = player[15].encode(encoding)
    teamCode = player[16].encode(encoding)
    teamCity = player[17].encode(encoding)

    formatStr = ("playerId={0},firstName={1},lastName={2},displayFirstLast={3},birthDayRaw={4},school={5},country={6},lastAffiliation={7},jersey={8},position={9},rosterStatus={10},teamId={11},teamName={12},teamAbbreviation={13}," "teamCode={14},teamCity={15}")
    formatStr.format(playerId, firstName, lastName, displayFirstLast, birthDate, school, country, lastAffiliation, jersey, position, rosterStatus, teamId, teamName, teamAbbreviation, teamCode, teamCity) 

    record = (playerId, firstName, lastName, displayFirstLast, birthDate, school, country, lastAffiliation, jersey, position, rosterStatus, teamId, teamName, teamAbbreviation, teamCode, teamCity) 

    cursor.execute(insertSql, record)


cnx.commit()
cursor.close()
cnx.close()

