from hackahoop_models.game_schedule import GameSchedule
import utils
import os
import datetime
import logging
import pytz
import sys
#logging.basicConfig(filename='output.log', level=logging.DEBUG)
#logging.getLogger('sqlalchemy.engine').setLevel(logging.INFO)


cfgFile = os.path.join(os.path.dirname(__file__), 'boxscore_fetcher.ini')
settings = utils.getConfigSettings(cfgFile)
Session = utils.initDB(settings["boxscore_fetcher"])
session = Session()

fmt = '%Y-%m-%d'
startDT = datetime.datetime(2013, 10, 27).strftime(fmt)
endDT = datetime.datetime(2014, 06, 01).strftime(fmt)
games = GameSchedule.getByDateRange(startDT, endDT)

eastern = pytz.timezone('US/Eastern')

targetFmt = '%Y-%m-%d %H:%M:%S'
for game in games:
    d = game.gameDate
    t = game.gameTime
    etDT = eastern.localize(
        datetime.datetime(d.year, d.month, d.day, t.hour, t.minute, t.second))
    utcDT = pytz.utc.normalize(etDT.astimezone(pytz.utc))

    game.gameDateUtc = utcDT.strftime(targetFmt)

session.commit()


