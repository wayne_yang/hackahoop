import twitter_api
import argparse
import json

parser = argparse.ArgumentParser(description='Fetch twitter timeline data')
parser.add_argument("screenName", nargs="+", help="a twitter screen name")
args = parser.parse_args()


screenNames = args.screenName
for sn in screenNames:
    params = {
        "screen_name": sn,
        "include_rts": "true",
        "count": 200, }

    data = twitter_api.fetch_all_user_timeline(params)
    print json.dumps(data)
