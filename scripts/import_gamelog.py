import mysql.connector
import json
import sys
import time
import datetime
import db_utils

encoding = "utf-8"

insertSql = ("INSERT INTO nba_stats.gamelog (SEASON_ID,PLAYER_ID,GAME_ID,"
            "GAME_DATE,CUR_TEAM,OPPONENT,HOME_GAME,WIN,MIN,FGM,FGA,FG_PCT,"
            "FG3M,FG3A,FG3_PCT,FTM,FTA,FT_PCT,OREB,DREB,REB,AST,STL,BLK,TOV,"
            "PF,PTS,PLUS_MINUS,SEASON_TYPE) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,"
            "%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)")

#print insertSql
cnx = db_utils.connect(db_utils.dbConfig)
cursor = cnx.cursor()

for line in sys.stdin:
    data = json.loads(line)
    seasonType = "R"
    if data["parameters"]["SeasonType"] != u"Regular Season":
        print "Invalid seasonType: {0}".format(data["parameters"]["SeasonType"])
        sys.exit(1)
    
    gamelogs = data["resultSets"][0]["rowSet"]
    
    # "SEASON_ID", "Player_ID", "Game_ID", "GAME_DATE", "MATCHUP", "WL", "MIN", 
    # "FGM", "FGA", "FG_PCT", "FG3M", "FG3A", "FG3_PCT", "FTM", "FTA", "FT_PCT",
    # "OREB", "DREB", "REB", "AST", "STL", "BLK", "TOV", "PF", "PTS", "PLUS_MINUS"
    for gamelog in gamelogs:
        seasonId = int(gamelog[0])
        playerId = int(gamelog[1])
        gameId = int(gamelog[2])
        gameDateRaw = gamelog[3]
        timeStruct = datetime.datetime.strptime(gameDateRaw, "%b %d, %Y")
        gameDate = "{0}-{1}-{2}".format(timeStruct.year, timeStruct.month, timeStruct.day)

        # matchup "PHX @ MIN" or "PHX vs. UTA"   
        curTeam = None
        opponent = None
        homeGame = True
        if "@" in gamelog[4]:
            matchUp = gamelog[4].split(" @ ")
            homeGame = False
        else:
            matchUp = gamelog[4].split(" vs. ")
        curTeam = matchUp[0].encode(encoding)
        opponent = matchUp[1].encode(encoding)

        win = None
        if gamelog[5] == u"W":
            win = True
        else:
            win = False

        minutes = gamelog[6]
        fgm = gamelog[7]
        fga = gamelog[8]
        fgPCT = gamelog[9]
        fg3m = gamelog[10]
        fg3a = gamelog[11]
        fg3PCT = gamelog[12]
        ftm = gamelog[13]
        fta = gamelog[14]
        ftPCT = gamelog[15]
        oreb = gamelog[16]
        dreb = gamelog[17]
        reb = gamelog[18]
        ast = gamelog[19]
        stl = gamelog[20]
        blk = gamelog[21]
        to = gamelog[22]
        pf = gamelog[23]
        pts = gamelog[24]
        plusMinus = gamelog[25]

        if minutes is None:
            minutes = 0
        if fgm is None:
            fgm = 0
        if fga is None:
            fga = 0
        if fgPCT is None:
            fgPCT = 0
        if fg3m is None:
            fg3m = 0
        if fg3a is None:
            fg3a = 0
        if fg3PCT is None:
            fg3PCT = 0
        if ftm is None:
            ftm = 0
        if fta is None:
            fta = 0
        if ftPCT is None:
            ftPCT = 0
        if oreb is None:
            oreb = 0
        if dreb is None:
            dreb = 0
        if reb is None:
            reb = 0
        if ast is None:
            ast = 0
        if stl is None:
            stl = 0
        if blk is None:
            blk = 0
        if to is None:
            to = 0
        if pf is None:
            pf = 0
        if pts is None:
            pts = 0
        if plusMinus is None:
            plusMinus = 0
        
        record = (seasonId,playerId,gameId,gameDate,curTeam,opponent,homeGame,
                  win,minutes,fgm,fga,fgPCT,fg3m,fg3a,fg3PCT,ftm,fta,ftPCT,
                  oreb,dreb,reb,ast,stl,blk,to,pf,pts,plusMinus,seasonType) 

        """
        print record
        formatStr = ("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},"
               "{14},{15},{16},{17},{18},{19},{20},{21},{22},{23},{24},{25},"
               "{26},{27},{28}")
        print formatStr.format(seasonId,playerId,gameId,gameDate,curTeam,opponent,homeGame,
                  win,minutes,fgm,fga,fgPCT,fg3m,fg3a,fg3PCT,ftm,fta,ftPCT,
                  oreb,dreb,reb,ast,stl,blk,to,pf,pts,plusMinus,seasonType) 
        """

        cursor.execute(insertSql, record)


cnx.commit()
cursor.close()
cnx.close()
