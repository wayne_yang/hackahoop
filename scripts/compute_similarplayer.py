import db_utils
import numpy as np
from sklearn.neighbors import NearestNeighbors
from sklearn import preprocessing
import player

cnx = db_utils.connect(db_utils.dbConfig);
cursor = cnx.cursor()

# creating an index of current player ids (sorted)
current_player_id = [];
query = ("select current_player.player_id from current_player left join overall_careerstats on (current_player.player_id = overall_careerstats.player_id) where SEASON_ID=2012 order by player_id");
#query = ("select player_id from playerinfo where rosterstatus = 1")

cursor.execute(query);

new_results = cursor.fetchall();
# flatten result
current_player_id = map(list, list(new_results))
current_player_id = sum(current_player_id, [])

def findNearestNeighbours(pid, k, careerStats=None):

    #query = ("select MIN, PTS, FGM, FGA, FTM, FTA, FG3M, FG3A, REB, AST, STL, BLK, TOV from playerinfo as cp left join overall_careerstats as cs on ( cp.player_id=cs.player_id) where SEASON_ID=2012 and rosterstatus = 1 order by cp.player_id")
    query = ("select MIN, PTS, FGM, FGA, FTM, FTA, FG3M, FG3A, REB, AST, STL, BLK, TOV from current_player as cp left join overall_careerstats as cs on ( cp.player_id=cs.player_id) where SEASON_ID=2012 order by cp.player_id")

    if careerStats is None:
        stats_cursor = cnx.cursor();
        stats_cursor.execute(query);
        careerStats = stats_cursor.fetchall();

    #num_rows = int(stats_cursor.rowcount);
    x = map(list, list(careerStats))
    x = sum(x, [])

    D = np.fromiter(x, dtype=float, count=-1)
    #D = D.reshape(num_rows, -1)
    D = D.reshape(len(careerStats), -1)

    D_scaled = preprocessing.scale(D);

#    print D;
#    print D_scaled;

    knn = NearestNeighbors(n_neighbors=k+1)
    knn.fit(D_scaled)

    #test:
    playerIndex = current_player_id.index(pid)
    neighbors = knn.kneighbors(D_scaled[playerIndex], return_distance=False);
    z = map(list, list(neighbors));
    z = sum(z, []);

    #test:
   # print D[current_player_id.index(2738)];
   # print D[current_player_id.index(202696)];

    # exclude the first result
    if z[0] == playerIndex:
        z = z[1:]
    
    return [current_player_id[i] for i in z]

if __name__ == "__main__":

    #result = findNearestNeighbours(202331, 5);
    result = findNearestNeighbours(708 , 5);
    #z = map(list, list(result));
    #z = sum(z, []);

    info = player.getPlayerInfo(result)
    
    for player in result:
        print player, info[player][3]

