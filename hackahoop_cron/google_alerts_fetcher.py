from hackahoop_utils import utils
import os
import argparse
import feedparser
import requests
import json
import sys
import logging
from datetime import datetime
import bleach
from urlparse import urlparse, parse_qs
from requests.exceptions import HTTPError


def post_to_solr(articles, solr_url):
    if articles is None:
        logger.info("No articles to post to solr")
        return None

    headers = {
        "Content-Type": "application/json",
    }
    params = {
        "commit": "true"
    }

    # feed to solr
    r = requests.post(solr_url, data=json.dumps(articles), headers=headers, params=params)
    try:
        r.raise_for_status()

    except HTTPError as e:
        logger.error("Failed to post to solr: {0}".format(e))


def get_feed_age(feed):
    """
    age in seconds
    """
    feed_time = datetime.strptime(feed["feed"]["updated"], "%Y-%m-%dT%H:%M:%SZ")
    now = datetime.utcnow()
    # age of the feed in seconds
    return (now - feed_time).total_seconds()

def convert_to_solr_feed(feed):
    """
    Convert feed according to solr schema and fetch links
    """
    if feed is None:
        logger.info("No feed to process")
        return None

    articles = []
    for entry in feed["entries"]:
        docId = entry["id"]
        title = bleach.clean(entry["title"], tags=[], strip=True)
        published = entry["published"]
        updated = entry["updated"]
        author = entry["author"]
        origUrl = entry["links"][0]["href"]
        parsed = urlparse(origUrl)
        params = parse_qs(parsed.query)
        contentUrl = params["q"][0]
        
        contentSource = "ga"
        articles.append({
            "id": docId,
            "title": title,
            "published_time": published,
            "updated_time": updated,
            "author": author,
            "content_url": contentUrl,
            "content_source": contentSource,
        })
    return articles


if __name__ == "__main__":
    # set up logs
    logger = utils.getLogger()

    # decrease log level for requests
    requests_log = logging.getLogger("requests")
    requests_log.setLevel(logging.WARNING)


    # parse command-line options
    argParser = argparse.ArgumentParser(description="Google Alerts Fetcher for Fantasy Basketball")
    argParser.add_argument("--config_file", default="google_alerts_fetcher.ini", help="config file that contains all the settings")
    argParser.add_argument("--fetch_only", default=False, action='store_true', help="Only fetch links but not post to solr")
    argParser.add_argument("--max_age", type=int, help="process feed if not older than max_age (seconds)")
    args = argParser.parse_args()


    # get configurations
    settings = utils.getConfigSettings(args.config_file)
    cfg = settings["google_alerts_fetcher"]

    if args.max_age is not None:
        cfg["max_age"] = args.max_age
    max_age = int(cfg["max_age"])

    # fetch GA alerts feed
    feed = feedparser.parse(cfg["url"])

    if feed.bozo:
        logger.error("The feed is bozo.")    
        sys.exit()

    feed_age = get_feed_age(feed)

    # do nothing if feed is too old
    if feed_age > max_age:
        logger.info("Skip because max_age={0} < feed_age={1}".format(max_age, feed_age))

    articles = convert_to_solr_feed(feed)
    logger.info("Processing {0} feed items".format(len(articles)))

    headers = {
        "User-Agent": utils.CHROME_UA
    }

    for article in articles:
        logger.info("Fetching {0}".format(article["content_url"]))
        r = requests.get(article["content_url"], headers=headers)
        try:
            r.raise_for_status()
        except HTTPError as e:
            logger.error("Failed to fetch url: {0}".format(e))
            continue

        content = utils.clean_web_page(r.text)
        article["content"] = content
        

    if not args.fetch_only:
        post_to_solr(articles, cfg["solr.update_url"])

