import os
from setuptools import setup, find_packages

here = os.path.abspath(os.path.dirname(__file__))
README = open(os.path.join(here, 'README.txt')).read()

requires = [
    'SQLAlchemy',
    'MySQL-python',
    'zope.sqlalchemy',
    'Markdown',
    'bleach',
]

setup(name='hackahoop_models',
      version='0.0.4',
      description='Contains HackAHoop MySQL model files',
      long_description=README,
      classifiers=[
        "Programming Language :: Python",
        "Framework :: Pyramid",
        "Topic :: Internet :: WWW/HTTP",
        "Topic :: Internet :: WWW/HTTP :: WSGI :: Application",
        ],
      author='',
      author_email='',
      url='',
      keywords='web wsgi bfg pylons pyramid',
      packages=["hackahoop_models"],
      include_package_data=True,
      zip_safe=False,
      install_requires=requires,
      )
