import os
import yaml
from yaml import CLoader as Loader
from fabric.api import env, settings, hide, run, local, sudo, cd
from fabric.tasks import execute
import sys
import argparse

env.hosts = ["localhost"]

def main():
    parser = argparse.ArgumentParser(description="HackAHoop deployment tool")
    parser.add_argument(
        "--skip_prepare_app",
        action="store_true",
        default=False,
        help="Skip virtualenv creation, git clone, and pip install"
    )
    args = parser.parse_args()

    if os.path.exists('deploy.yml'):
        with settings(user='wyang',
                      abort_on_prompts=False):

            p = yaml.safe_load(open('deploy.yml'))
            deploy = Deployment(
                project=p['project'],
                app_name=p['app_name'],
                run_setup_install=os.path.exists('setup.py'),
                build_deps=p.get('build_deps', []),
                run_deps=p.get('run_deps', []),
                python_version=p.get('python_version', '2.7'),
                git_repo=p["git_repo"],
                git_branch=p["git_branch"]
            )

            if not args.skip_prepare_app:
                deploy.prepare_app()

            version = "1.0.0"
            deploy.build_deb(
                version=version,
                file_map=p["file_map"],
            )

            """
            pl = p.get('pipeline')
            if pl:
                deploy.compile_less(pl.get('less_files', []))
                deploy.compress_css(pl.get('css_files', []))
                if pl.get('compile_i18n'):
                    deploy.compile_i18n()
                deploy.collect_static(pl.get('collect_static', []))
                deploy.add_cache_busting(pl.get('add_cache_busting', []))

            """

class Deployment(object):
    BASE_PATH = '/home/hackahoop'

    def __init__(self, project, app_name, run_setup_install, build_deps,
                 run_deps, python_version, git_repo, git_branch):
            self.project = project
            self.original_name = app_name
            self.app_name = app_name.lower()
            self.app_path = os.path.join(self.BASE_PATH, self.app_name)
            self.src_path = os.path.join(self.app_path, self.app_name)
            self.run_setup_install = run_setup_install
            self.build_deps = build_deps or []
            self.run_deps = run_deps or []
            self.pkg_name = self.app_name
            self.python_version = python_version or '2.7'
            self.git_repo = git_repo
            self.git_branch = git_branch
            #self.git_branch = _current_git_branch()
            self.git_commit = self._current_git_commit()

    def __repr__(self):
        s = ("Deployment(project={project}, app_name={app_name}, src_path={src_path}, "
             "build_deps={build_deps}, run_deps={run_deps}, pkg_name={pkg_name})")
        return s.format(
            project=self.project,
            app_name=self.app_name,
            src_path=self.src_path,
            build_deps=self.build_deps,
            run_deps=self.run_deps,
            pkg_name=self.pkg_name,
        )

    def _current_git_commit(self):
        return local('git rev-parse --short HEAD', capture=True)

    def _git_clone(self, repo, branch, directory):
        local("sudo git clone {} -b {} {}".format(repo, branch, directory))

    def prepare_app(self):
        with settings(user='root'):
            if self.build_deps:
                local('sudo apt-get install -qq {}'.format(' '.join(self.build_deps)))

            local('sudo rm -rf {}'.format(self.app_path))
            #local('mkdir -p {}'.format(self.app_path))
            #local('chown hackahoop {}'.format(self.app_path))

            local('sudo virtualenv -p python{} -q {}'
                .format(self.python_version, self.app_path))

            tmp_git_clone = os.path.join(self.app_path, "tmp")
            self._git_clone(self.git_repo, self.git_branch, tmp_git_clone)

            tmp_git_clone_app = os.path.join(tmp_git_clone, self.app_name)
            local("sudo mv {} {}".format(tmp_git_clone_app,
                                         self.src_path))

            local("sudo rm -rf {}".format(tmp_git_clone))

        local('sudo {} install -r {}'
            .format(os.path.join(self.app_path, 'bin/pip'),
                    os.path.join(self.src_path, 'requirements.txt')))

        with cd(self.src_path):
            if self.run_setup_install:
                local('{} setup.py install'
                    .format(os.path.join(self.app_path, 'bin/python')))

    def build_deb(self, version, file_map):
            """
            TODO:  do autoversion

            if not version:
                version = _next_version_for(self.pkg_name)
            """
            run('rm -rf {}'.format(os.path.join(self.src_path, '.git')))
            with cd(self.BASE_PATH), settings(sudo_user="root"):
                #run('rm -rf *')
                #run('mv {} .'.format(os.path.join(self.src_path, 'debian')))
                deps_str = ('-d ' + ' -d '.join(self.run_deps)
                            if self.run_deps else '')
                hooks_str = ' '.join(
                    '{} debian/{}'.format(opt, fname)
                    for opt, fname in [
                        ('--before-remove', 'prerm'),
                        ('--after-remove', 'postrm'),
                        ('--before-install', 'preinst'),
                        ('--after-install', 'postinst'),
                    ]
                    if os.path.exists(os.path.join('debian', fname))
                )

                fmap_str = " ".join(
                    "{}={}".format(os.path.join(self.src_path, k), v) 
                    for k,v in file_map.items())
                    
                fpm_cmd = (
                    'fpm -s dir -t deb -f -n {self.pkg_name} -v {version} '
                    '-a all -x "*.bak" -x "*.orig" {hooks} '
                    '--description '
                    '"Branch: {self.git_branch} Commit: {self.git_commit}" '
                    '{deps} {app_path} {fmap_str}'
                    .format(self=self, hooks=hooks_str, deps=deps_str,
                            app_path=self.app_path, version=version,
                            fmap_str=fmap_str)
                )

                fpm_output = sudo(fpm_cmd)
                """
                deb_name = os.path.basename(fpm_output.split('"')[-2])
                if download:
                    get(deb_name, 'debian/%(basename)s')
                if push_to_repo:
                    run('scp {} mirror@mirror.local:/vrmd/mirror/incoming'
                        .format(deb_name))
                    with settings(host_string='mirror.local',
                                  user='mirror'), cd('/vrmd/mirror/incoming'):
                        run('reprepro -V -b /vrmd/mirror/debs includedeb {} {}'
                            .format(distro, deb_name))
                        run('/vrmd/mirror/sync-to-main-mirror.sh')
                """

if __name__ == "__main__":
    execute(main)
