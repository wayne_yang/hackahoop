import mysql.connector
import json
import sys
import time
import datetime
import db_utils
from decimal import *
getcontext().prec=5


encoding = "utf-8"


insertSql = ("INSERT INTO nba_stats.overall_careerstats (PLAYER_ID,SEASON_ID,"
            "GP,GS,MIN,FGM,FGA,FG_PCT,"
            "FG3M,FG3A,FG3_PCT,FTM,FTA,FT_PCT,OREB,DREB,REB,AST,STL,BLK,TOV,"
            "PF,PTS) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,"
            "%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)")


cnx = db_utils.connect(db_utils.dbConfig)
cursor = cnx.cursor()

for line in sys.stdin:
    data = json.loads(line)

    seasonAvgData = None
    for res in data["resultSets"]:
        if res["name"] == "SeasonTotalsRegularSeason":
            seasonAvgData = res
            break

    if seasonAvgData is None:
        sys.stderr.write("Failed to find SeasonTotalsRegularSeason\n")
        continue

    sAggregate = {}
    playerId = None
    for seasonData in seasonAvgData["rowSet"]:
        playerId = seasonData[0]
        seasonId = seasonData[1].split("-")[0]
        playerAge = int(seasonData[5])
        gp = int(seasonData[6])
        teamId = int(seasonData[3])

        if seasonData[7] is None:
            gs = 0
        else:
            gs = int(seasonData[7])
        minutes = seasonData[8]
        fgm = seasonData[9]
        fga = seasonData[10]
        fgPCT = seasonData[11]
        fg3m = seasonData[12]
        fg3a = seasonData[13]
        fg3PCT = seasonData[14]
        ftm = seasonData[15]
        fta = seasonData[16]
        ftPCT = seasonData[17]
        oreb = seasonData[18]
        dreb = seasonData[19]
        reb = seasonData[20]
        ast = seasonData[21]
        stl = seasonData[22]
        blk = seasonData[23]
        tov = seasonData[24]
        pf = seasonData[25]
        pts = seasonData[26]

        if minutes is None:
            minutes = 0
        if fgm is None:
            fgm = 0
        if fga is None:
            fga = 0
        if fgPCT is None:
            fgPCT = 0
        if fg3m is None:
            fg3m = 0
        if fg3a is None:
            fg3a = 0
        if fg3PCT is None:
            fg3PCT = 0
        if ftm is None:
            ftm = 0
        if fta is None:
            fta = 0
        if ftPCT is None:
            ftPCT = 0
        if oreb is None:
            oreb = 0
        if dreb is None:
            dreb = 0
        if reb is None:
            reb = 0
        if ast is None:
            ast = 0
        if stl is None:
            stl = 0
        if blk is None:
            blk = 0
        if tov is None:
            tov = 0
        if pf is None:
            pf = 0
        if pts is None:
            pts = 0


        formatStr = ("playerId={0},seasonId={1},gp={2},gs={3},minutes={4}lfgm={5},fga={6},fgPCT={7},fg3m={8},fg3a={9},fg3PCT={10},ftm={11},fta={12},ftPCT={13},oreb={14},dreb={15},reb={16},ast={17},stl={18},blk={19},tov={20},pf={21},pts={22}")
        #print formatStr.format(playerId, seasonId, playerAge, gp, gs, minutes, fgm, fga, fgPCT, fg3m, fg3a, fg3PCT, ftm, fta, ftPCT, oreb, dreb, reb, ast, stl, blk, tov, pf, pts) 

        record = [teamId, playerId, seasonId, gp, gs, minutes, fgm, fga, fgPCT, fg3m, fg3a, fg3PCT, ftm, fta, ftPCT, oreb, dreb, reb, ast, stl, blk, tov, pf, pts]

        if seasonId not in sAggregate:
            sAggregate[seasonId] = []
        sAggregate[seasonId].append(record)

    for seasonId in sAggregate:
        if len(sAggregate[seasonId]) == 1:
            cursor.execute(insertSql, sAggregate[seasonId][0][1:])
        else:
            result = [subSeason[1:] 
                for subSeason in sAggregate[seasonId] if subSeason[0] == 0]
            cursor.execute(insertSql, result[0])
            """ CALCULATE OVERALL SEASON AVG BY HAND
            result = {}
            runningTotal = []
            for subSeason in sAggregate[seasonId]:
                gp = int(subSeason[2])
                gs = int(subSeason[3])
                if seasonId not in result:
                    result[seasonId] = {"data": [], "totalGP": 0, "totalGS": 0 }
                result[seasonId]["data"].append([i * gp for i in subSeason[4:]])
                result[seasonId]["totalGP"] = result[seasonId]["totalGP"] + gp
                result[seasonId]["totalGS"] = result[seasonId]["totalGS"] + gs
                #print seasonId, gp, result[seasonId]
            
            for sid in result.keys():
                record = [playerId, int(sid), result[sid]["totalGP"], result[sid]["totalGS"]]
                record.extend([
                    Decimal(sum(i))/Decimal((result[sid]["totalGP"]))
                        for i in zip(*result[sid]["data"])
                ])
                #cursor.execute(insertSql, record)
                #print result[seasonId]
                #print [sum(i) for i in zip(*result[seasonId]["data"])]
            """

cnx.commit()
cursor.close()
cnx.close()
