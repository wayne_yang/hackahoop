from pyramid.view import view_config
from pyramid.response import Response
from hackahoop_models.similar_player import SimilarPlayer
from hackahoop_models.meta import DBSession
from hackahoop_models.careerstats import CombinedCareerStats, CareerStatsJsonEncoder
from hackahoop_models import careerstats
from hackahoop_models.playernews import PlayerTweet, Tweet, FollowRelationship, getFriendFollowers
from hackahoop_models.gamelog import GameLog, GameLogJsonEncoder
from hackahoop_models.depthchart import DepthChart
from hackahoop_models.playerinfo import PlayerInfo
from hackahoop_models.youtube_video import YoutubeVideo
from hackahoop_models.rate import Rating

import utils

import json
import decimal
from ..config import RotoStatCategory, FullStatCategory, DataSeriesColor

import logging
log = logging.getLogger(__name__)

@view_config(route_name='single_player_videos', renderer='../templates/single_player_videos.pt')
def single_player_videos_view(request):
    if "playerName" not in request.matchdict:
        # TODO: fixed this 
        return Response("playerName is missing", content_type='text/html', status_int=400)

    seoName = request.matchdict["playerName"]
    playerInfo = PlayerInfo.getBySeoName(seoName)
    playerId = playerInfo.playerId

    ratingCount = Rating.getRatingCount(playerId)
    videos = YoutubeVideo.getByPlayerId(playerId, limit=10)
    videoToTitleJson = json.dumps(dict([(i.videoId, i.title) for i in videos]))

    # If logged in, show follow/unfollow buttons
    followStatus = None
    friendFollowers = None
    if request.user:
        followStatus = FollowRelationship.followBy(playerId, request.user["userId"])
        # get friends who also follow this player
        friendFollowers = getFriendFollowers(request.user["userId"], playerId)
        log.debug("friendFollowers={0}".format(friendFollowers))
        friendFollowers = getPrettyPrintFollowers(request, friendFollowers)


    return {
        "player": playerInfo,
        "ratingCount": ratingCount,
        "activeTabs": getActivePlayerTab(request),
        'videos': videos,
        'videoToTitleJson': videoToTitleJson,
        "followStatus": followStatus,
        "friendFollowers": friendFollowers,
    }


@view_config(route_name='single_player_news', renderer='../templates/single_player_news.pt')
def single_player_news_view(request):
    if "playerName" not in request.matchdict:
        # TODO: fixed this 
        return Response("playerName is missing", content_type='text/html', status_int=400)

    seoName = request.matchdict["playerName"]
    playerInfo = PlayerInfo.getBySeoName(seoName)
    playerId = playerInfo.playerId

    ratingCount = Rating.getRatingCount(playerId)

    pTweets = PlayerTweet.getByPlayerId(playerId, limit=20)

    # If logged in, show follow/unfollow buttons
    followStatus = None
    friendFollowers = None
    if request.user:
        followStatus = FollowRelationship.followBy(playerId, request.user["userId"])
        # get friends who also follow this player
        friendFollowers = getFriendFollowers(request.user["userId"], playerId)
        log.debug("friendFollowers={0}".format(friendFollowers))
        friendFollowers = getPrettyPrintFollowers(request, friendFollowers)


    return {
        "player": playerInfo,
        "ratingCount": ratingCount,
        "activeTabs": getActivePlayerTab(request),
        'tweets': [pt.tweet for pt in pTweets],
        "followStatus": followStatus,
        "friendFollowers": friendFollowers,
    }

@view_config(route_name='discussions', renderer='../templates/discussions.pt')
def discussions_view(request):
    if "playerName" not in request.matchdict:
        # TODO: fixed this 
        return Response("playerName is missing", content_type='text/html', status_int=400)

    seoName = request.matchdict["playerName"]
    playerInfo = PlayerInfo.getBySeoName(seoName)
    playerId = playerInfo.playerId

    ratingResult = utils.getRatingsByPlayer(request, playerId, limit=20)
    ratingCount = Rating.getRatingCount(playerId)

    # If logged in, show follow/unfollow buttons
    followStatus = None
    friendFollowers = None
    if request.user:
        followStatus = FollowRelationship.followBy(playerId, request.user["userId"])
        # get friends who also follow this player
        friendFollowers = getFriendFollowers(request.user["userId"], playerId)
        log.debug("friendFollowers={0}".format(friendFollowers))
        friendFollowers = getPrettyPrintFollowers(request, friendFollowers)

    return {
        "player": playerInfo,
        "ratingResult": ratingResult,
        "ratingCount": ratingCount,
        'followStatus': followStatus,
        'followPath': request.route_path('follow_player'),
        'unfollowPath': request.route_path('unfollow_player'),
        'friendFollowers': friendFollowers,
        'editPerm': None,
        "activeTabs": getActivePlayerTab(request),
    }



@view_config(route_name='single_player', renderer='../templates/single_player.pt')
def single_player_view(request):
    if "playerName" not in request.matchdict:
        # TODO: fixed this 
        return Response("playerName is missing", content_type='text/html', status_int=400)

    seoName = request.matchdict["playerName"]
    playerInfo = PlayerInfo.getBySeoName(seoName)
    playerId = playerInfo.playerId

    stats = CombinedCareerStats.getByPlayerId(playerId)
    statsJSON = json.dumps(stats, cls=CareerStatsJsonEncoder)

    #log.debug("statsJSON={0}".format(statsJSON))

    # If logged in, show follow/unfollow buttons
    followStatus = None
    friendFollowers = None
    if request.user:
        followStatus = FollowRelationship.followBy(playerId, request.user["userId"])
        # get friends who also follow this player
        friendFollowers = getFriendFollowers(request.user["userId"], playerId)
        log.debug("friendFollowers={0}".format(friendFollowers))
        friendFollowers = getPrettyPrintFollowers(request, friendFollowers)

    radarData = []
    decimal.getcontext().prec = 3
    if stats is not None:
        radarData = [{ 
            "value": float(getattr(stats, i["name"])),
            "y": float(getattr(stats, i["name"])/decimal.Decimal(careerstats.MaxStat[i["name"]])),
            #"name": i["displayName"],
        } for i in RotoStatCategory]

    depthChart = DepthChart.get(playerInfo.teamId, playerInfo.position)

    similarPlayers = SimilarPlayer.getByPlayerId(playerId, limit=5, statsSeason=2012)
    for i, player in enumerate(similarPlayers):
        player.color = DataSeriesColor[i]

    log.debug(request.registry.settings["ga.liveTracking"])

    ratingCount = Rating.getRatingCount(playerId)

    pTweets = PlayerTweet.getByPlayerId(playerId, limit=1)

    return { 
        'tweets': [pt.tweet for pt in pTweets],
        "activeTabs": getActivePlayerTab(request),
        'similarPlayers': similarPlayers,
        'maxStat': json.dumps(careerstats.MaxStat),
        'player': playerInfo,
        'careerstats': stats,
        'careerstatsJSON': statsJSON,
        'radarData':  json.dumps(radarData),
        'rotoStatCategories': RotoStatCategory,
        'fullStatCategories': FullStatCategory,
        'depthChart': depthChart,
        'followStatus': followStatus,
        'friendFollowers': friendFollowers,
        'ratingCount': ratingCount,
    }


def getPrettyPrintFollowers(request, followers, maxDisplay=3):
    if not followers:
        return None
    if maxDisplay < 3:
        maxDisplay = 3

    followers = followers[:maxDisplay]

    count = len(followers)
    names = ["<a href='{0}'>{1}</a>".format(request.route_path("user", userId=i["authId"]), i["name"])
        for i in followers]
    if count < 2:
        return names[0]
    elif count == 2:
        return " and ".join(names)
    else:
        return ", ".join(names[:maxDisplay-1]) + ", and " + names[-1]


def getActivePlayerTab(request):
    tabs = {
        "discussions": "",
        "single_player": "",
        "single_player_news": "",
        "single_player_videos": "",
    }

    tabs[request.matched_route.name] = "active"
    return tabs
    
