from teams import Teams
import db_utils
import sys

conn = db_utils.connect(db_utils.dbConfig)
cursor = conn.cursor()

sql = ("INSERT INTO nba_stats.teams (team_name, team_id, team_abbreviation,"
    "team_code, team_city) VALUES(%s, %s, %s, %s, %s)")

for key, val in Teams.iteritems():
    cursor.execute(sql, val)

conn.commit()
cursor.close()
conn.close()

