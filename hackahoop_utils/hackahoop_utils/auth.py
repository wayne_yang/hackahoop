from rauth_session import OAuth1Session


def get_app_only_auth(consumerKey, consumerSecret):
    consumerKey = urllib.quote_plus(consumerKey)
    consumerSecert = urllib.quote_plus(consumerSecret)
    bearToken = base64.b64encode("{0}:{1}".format(consumerKey, consumerSecret))

    url = "https://api.twitter.com/oauth2/token"
    headers = {
        "Authorization" : "Basic {0}".format(bearToken),
        "Content-Type" : "application/x-www-form-urlencoded;charset=UTF-8"
    }

    body = "grant_type=client_credentials"

    # create your HTTP request
    req = urllib2.Request(url, body, headers)
    resp = None
    try:
        resp = urllib2.urlopen(req)
    except urllib2.HTTPError as e:
        sys.stderr.write("Got Error HTTPError: {0}\n".format(e))
        sys.exit()

    content = resp.read()
    data = json.loads(content)
    return {
        "Authorization": data["access_token"]
    }


def get_oauth1_session(consumer_key, consumer_secret, oauth_token, oauth_token_secret):
    """
    return session object
    """
    session = OAuth1Session(consumer_key, consumer_secret, oauth_token, oauth_token_secret)
    return session
    

