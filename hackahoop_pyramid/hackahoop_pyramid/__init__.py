from pyramid.config import Configurator
from pyramid.renderers import JSON
from pyramid.authentication import SessionAuthenticationPolicy
from pyramid.authorization import ACLAuthorizationPolicy
from pyramid.security import unauthenticated_userid

from decimal import Decimal
import datetime
import logging

from sqlalchemy import engine_from_config

from hackahoop_models.meta import DBSession, Base
from hackahoop_models.account import AuthId

log = logging.getLogger(__name__)

from pyramid.events import BeforeRender
import helpers

def add_renderer_globals(event):
   event['h'] = helpers

def decimal_adapter(obj, request):
    return float(obj)

def date_adapter(obj, request):
    return obj.isoformat()

def configure_velruse(config):
    settings = config.registry.settings
    providers = settings.get('login_providers', '')
    providers = filter(None, [p.strip()
                              for line in providers.splitlines()
                              for p in line.split(', ')])
    settings['login_providers'] = providers
    if not any(providers):
        log.warn('no login providers configured, double check your ini '
                 'file and add a few')

    # enable only facebook for now
    if 'facebook' in providers:
        config.include('velruse.providers.facebook')
        config.add_facebook_login_from_settings()


def configure_hackahoop(config):
    # add custom json render that handles Decimal type
    json_renderer = JSON()
    json_renderer.add_adapter(Decimal, decimal_adapter)
    json_renderer.add_adapter(datetime.date, date_adapter)
    config.add_renderer('json', json_renderer)

    config.add_static_view('static', 'static', cache_max_age=3600)

    ## ajax end points
    config.add_route('playersearch', '/v1/nba/playersearch')
    config.add_route('ajax_playernews', '/v1/nba/playernews')
    config.add_route('newsfeed', '/v1/nba/newsfeed')
    config.add_route('game_schedule', '/v1/nba/game_schedule')
    config.add_route('careerstats_api', '/v1/nba/careerstats')
    config.add_route('gamelog_api', '/v1/nba/gamelog')
    config.add_route('follow_player', '/player/follow')
    config.add_route('unfollow_player', '/player/unfollow')
    config.add_route('delete_rate', '/player/delete_rate')

    ## user-facing pages
    config.add_route('index', '/')
    config.add_route('rate_submit', '/rate_submit')
    config.add_route('user', '/user/{userId}')
    config.add_route('user_posts', '/user/{userId}/posts')
    config.add_route('logout', '/logout')
    config.add_route('register', '/register')
    config.add_route('search_result', '/search')
    config.add_route('players', '/players')
    config.add_route('discussions', '/player/{playerName}/discussions')
    config.add_route('rate_form', '/player/{playerName}/rate_form')
    config.add_route('global_rate_form', '/player/rate_form')
    config.add_route('single_player', '/player/{playerName}')
    config.add_route('single_player_news', '/player/{playerName}/news')
    config.add_route('single_player_videos', '/player/{playerName}/videos')
    config.add_route('test', '/test')

    ## hidden end points for labeling news
    config.add_route("corpus_list", '/corpus/list')
    config.add_route("update_corpus_doc", '/corpus/{docId}')

    # add facebook login
    config.include(configure_velruse)

    # set authentication policy to use session
    authnPolicy = SessionAuthenticationPolicy()
    config.set_authentication_policy(authnPolicy)

    authzPolicy = ACLAuthorizationPolicy()
    config.set_authorization_policy(authzPolicy)

    # add convenience method to request object
    config.add_request_method(get_user, 'user', reify=True)


def get_user(request):
    userId = unauthenticated_userid(request)
    if userId is not None:
        authIdObj = AuthId.getById(userId)
        if not authIdObj:
            return None

        return {
            "userId": userId,
            "userInfo": authIdObj.users[0]
        }
    return userId

def main(global_config, **settings):
    """ This function returns a Pyramid WSGI application.
    """
    engine = engine_from_config(settings, 'sqlalchemy.')
    DBSession.configure(bind=engine)
    Base.metadata.bind = engine

    from pyramid.settings import asbool
    import json
    settings['ga.liveTracking'] = asbool(settings.get('ga.liveTracking', 'false'))
    settings['ga.trackingId'] = settings.get('ga.trackingId', None)
    settings['ga.mp.version'] = settings.get('ga.mp.version', 1)

    config = Configurator(settings=settings)

    config.add_subscriber(add_renderer_globals, BeforeRender)

    # hackahoop configuration
    config.include(configure_hackahoop)

    config.scan()
    return config.make_wsgi_app()
