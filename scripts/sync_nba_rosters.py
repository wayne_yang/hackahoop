"""
Dependencies:
    - sudo apt-get install libxml2-dev libxslt-dev
    - pip install lxml
"""
from teams import CurTeamObjects, getTeamByFullName
import requests
from bs4 import BeautifulSoup
import re
import db_utils
from datetime import datetime
import re
import utils
import logging

def fetchTeamRosters():
    headers = {
        "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.95 Safari/537.36"
    }
    #url = "http://www.nba.com/{0}/roster".format(team.getCode())
    url = "http://espn.go.com/nba/teams"
    r = requests.get(url, headers=headers)
    soup = BeautifulSoup(r.content, "lxml")
    teamList = (soup.find("div", id="content")
        .select("div.span-6 > div.span-4")[0]
        .select("li"))

    rosters = []
    for teamLI in teamList:
        teamFullName = teamLI.select("a.bi")[0].text
        team = getTeamByFullName(teamFullName)

        # last <span> contains stats, schedule, roster, depth chart links
        rosterHref = teamLI.select("span")[-1].select("a")[-2].attrs["href"]
        rosterURL = "http://espn.go.com{0}".format(rosterHref)

        r = requests.get(rosterURL, headers=headers)
        soup = BeautifulSoup(r.content, "lxml")
        """ TODO:  uncomment
        #start = datetime.now()
        soup = BeautifulSoup(open("/tmp/{0}.html".format(team.getCode())).read(), "lxml")
        #delta = datetime.now() - start
        #print delta.total_seconds()
        """

        rows = soup.find_all("tr", class_=re.compile(r"player-\d+-\d+"))
        players = []
        for row in rows:
            # row:   NO., NAME, POS, AGE, HT, WT, COLLEGE, 2013-2014 SALARY
            cols = row.select("td")
            playerName = cols[1].find("a").text 
            position = cols[2].text
            school = cols[6].text
            suffix = None

            # if the name contains a suffix
            nameParts = utils.parseHumanName(playerName)
            suffix = nameParts["suffix"]
            playerName = nameParts["name"]

            players.append({
                "suffix": suffix,
                "name": playerName,
                "position": position,
                "school": school,
            })

        rosters.append({ "team": team, "players": players })
    return rosters

def parseRosterFromNBA(html):
    soup = BeautifulSoup(html)
    rows = (soup.find("div", class_="feeds-roster")
        .find("table")
        .find_all("tr", class_=re.compile(r"odd|even")))

    result = []
    for row in rows:
        cols = row.find_all("td")
        jersey = int(cols[0].text) if cols[0].text != "" else None
        link = cols[1].find("a")
        playerName = link.text
        playerLink = link.attrs["href"]
        pos = cols[2].text
        result.append({
            "jersey": jersey,
            "name": playerName,
            "pos" : pos,
        })
    return result    

def getLogger():
    #logging.basicConfig(filename="./sync_nba_rosters.log", level=logging.DEBUG)

    logger = logging.getLogger(__name__)
    logger.setLevel(logging.DEBUG)
    # create formatter
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
    ch = logging.StreamHandler()
    ch.setFormatter(formatter)
    logger.addHandler(ch)
    return logger

if __name__ == "__main__":
    #html = open("/tmp/b").read()
    from player import findPlayer, updatePlayer, getAllActivePlayers
    import sys
    import argparse

    parser = argparse.ArgumentParser(description="Sync NBA rosters")
    parser.add_argument("--dryrun", action="store_const", const=True, default=False)
    #parser.add_argument("--add-new-players", action="store_const", const=True, default=False)
    args = parser.parse_args()

    logger = getLogger()

    cnx = db_utils.connect(db_utils.dbConfig)
    curActivePlayerIds = set()
    rosters = fetchTeamRosters()

    for roster in rosters:
        players = roster["players"]
        team = roster["team"]
        for player in players:
            playerName = player["name"]
            suffix = player["suffix"]

            # get player info such as player_id
            # (203142, u'Chris', u'Copeland', u'Chris Copeland', u'C. Copeland', None, u'Colorado', u'USA', u'Colorado/USA', 14, u'PF', 1, 1610612754, u'Pacers', u'IND', u'pacers', u'Indiana')
            playerInfo = findPlayer( playerName, suffix=suffix, conn=cnx, onlyActiveStatus=False)

            if not playerInfo:
                # TODO: Add player to DB
                logger.debug("[NEW] {0} from {1} does not exist in the db".format(playerName.encode("utf8"), team.getCode()))
                continue
            elif len(playerInfo) > 1:
                # most likely we are looking for the player who is active match the given team
                activePlayerInfo = [i for i in playerInfo if i[13] == 1 and i[14] == team.id]
                if len(activePlayerInfo) == 1:
                    playerInfo = activePlayerInfo
                else:
                    logger.error("{0} from {1} match more than one entry in db: {2}".format(
                        playerName.encode("utf8"),
                        team.getAbbreviation(),
                        activePlayerInfo))
                    continue
            
            # check if current team_id is different from the live rosters
            dbTeamId = playerInfo[0][14]
            dbTeamName = playerInfo[0][15]
            playerId = playerInfo[0][0]
            curActivePlayerIds.add(playerId)

            newData = {}
            if dbTeamId != team.getId():
                logger.debug("[CHANGE] {0} ({1}) | {2} --> {3}".format(playerName.encode("utf8"), playerId, dbTeamName, team.getName()))

                newData["ROSTERSTATUS"] = 1
                newData["TEAM_ID"] = team.getId()

            if playerInfo[0][12] != player["position"]:
                logger.debug("[CHANGE] {0} position from {1} to {2}".format(
                    playerName.encode("utf8"),
                    playerInfo[0][12],
                    player["position"]))

                newData["ROSTERSTATUS"] = 1
                newData["POSITION"] = player["position"]

            if not args.dryrun and newData:
                updatePlayer(playerId, newData)

    dbActivePlayerIds = getAllActivePlayers(["player_id, full_name"])
    for playerId, playerName in dbActivePlayerIds:
        if playerId not in curActivePlayerIds:
            logger.debug("[INACTIVE] {0} ({1}) is no longer active".format(playerName.encode("utf8"), playerId))
            # update db record
            if not args.dryrun:
                updatePlayer(playerId, {"ROSTERSTATUS": 0})
