import requests

#'pageview', 'appview', 'event', 'transaction', 'item', 'social', 'exception', 'timing'
HIT_TYPE_PV = "pageview"
HIT_TYPE_ET = "event"


class MeasurementProtocol(object):
    """
    https://developers.google.com/analytics/devguides/collection/protocol/v1/devguide
    https://developers.google.com/analytics/devguides/collection/protocol/v1/parameters

    In the event of a network problem (e.g. DNS failure, refused connection, etc), Requests will raise a ConnectionError exception.
    In the event of the rare invalid HTTP response, Requests will raise an HTTPError exception.
    If a request times out, a Timeout exception is raised.
    If a request exceeds the configured number of maximum redirections, a TooManyRedirects exception is raised.
    All exceptions that Requests explicitly raises inherit from requests.exceptions.RequestException.
    """
    endpoint = "http://www.google-analytics.com/collect"

    def __init__(self, version, trackingId, clientId):
        self.version = version
        self.trackingId = trackingId
        self.clientId = clientId

    def sendEventTracking(self, category, action, label=None, value=None, customDimensions=None):
        """
        customDimensions = {"cd1": "{cd1 value}", ...}
        """
        payload = {
            "v": self.version,
            "tid": self.trackingId,
            "cid": self.clientId,
            "t": HIT_TYPE_ET,
            "ec": category,
            "ea": action
        }

        if customDimensions is not None:
            for k in customDimensions.keys():
                payload[k] = customDimensions[k]

        if label is not None:
            payload["el"] = label

        if value is not None:
            payload["ev"] = value

        r = requests.post(self.endpoint, data=payload)

        r.raise_for_status()

        
    def __repr__(self):
        a = ("MP<version={version}, trackingId={trackingId}, "
            "clientId={clientId}>")
        return a.format(
            version=self.version, 
            trackingId=self.trackingId,
            clientId=self.clientId)

if __name__=="__main__":
    mp = MeasurementProtocol(1, "UA-45004885-1", 123456)
    mp.sendEventTracking("video", "play", "holiday", 300)
