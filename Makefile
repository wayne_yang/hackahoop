.PHONY: hackahoop_cron hackahoop_celery clean

TARGET_PATH=/home/hackahoop
TARGET_CRON_PATH=/etc/cron.d

CRON_VERSION=1.0.2
hackahoop_cron:
	sudo virtualenv $(TARGET_PATH)/hackahoop_cron
	sudo $(TARGET_PATH)/hackahoop_cron/bin/pip install -r hackahoop_cron/requirements.txt
	sudo cp -R hackahoop_cron $(TARGET_PATH)/hackahoop_cron/
	sudo cp hackahoop_cron/etc/hackahoop_cron $(TARGET_CRON_PATH)/hackahoop_cron
	sudo chown root:root $(TARGET_CRON_PATH)/hackahoop_cron
	sudo chmod 644 $(TARGET_CRON_PATH)/hackahoop_cron
	fpm -s dir -t deb -n hackahoop_cron -a all --deb-user root --deb-group root -v $(CRON_VERSION) $(TARGET_PATH)/hackahoop_cron $(TARGET_CRON_PATH)/hackahoop_cron
	sudo rm -rf $(TARGET_CRON_PATH)/hackahoop_cron
	sudo rm -rf $(TARGET_PATH)/hackahoop_cron

CELERY_VERSION=1.0.0
hackahoop_celery:
	sudo virtualenv $(TARGET_PATH)/hackahoop_celery
	sudo $(TARGET_PATH)/hackahoop_celery/bin/pip install -r hackahoop_celery/requirements.txt
	#sudo $(TARGET_PATH)/hackahoop_celery/bin/pip install cython git+git://github.com/surfly/gevent.git@1.0rc3#egg=gevent
	sudo cp -R hackahoop_celery $(TARGET_PATH)/hackahoop_celery/
	fpm -s dir -t deb -n hackahoop_celery --deb-pre-depends libxslt1.1 --deb-pre-depends libevent-dev -a all --deb-user root --deb-group root -v $(CELERY_VERSION) $(TARGET_PATH)/hackahoop_celery
	sudo rm -rf $(TARGET_PATH)/hackahoop_celery
